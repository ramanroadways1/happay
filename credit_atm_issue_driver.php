<?php
include "header.php";
?>
<script type="text/javascript">
	$(function() {
		$("#tno").autocomplete({
		source: 'autofill/get_tno.php',
		select: function (event, ui) { 
              $('#tno').val(ui.item.value);   
              $('#company').val(ui.item.company);   
             $("#button1").attr('disabled',false);				   
             return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#tno").val('');
			$("#company").val('');
			$("#tno").focus();
			$("#button1").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
		});});
</script> 

<script type="text/javascript">
$(document).ready(function (e) {
	$("#Form1").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#button1").attr("disabled", true);
		$.ajax({
        	url: "./save_atm_issue_credit_to_driver.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#func_result2").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>

<div class="content-wrapper">
    <section class="content-header">
      <h4>
		Credit ATM related issue amount to driver :
		<br>
		<br>
		<font color="maroon"> ATM सम्बंधित प्रॉब्लम का पैसा ड्राइवर को क्रेडिट करे । </font>
      </h4>
	  
	  <style>
		.form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
		</section>
		
   <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<form id="Form1" autocomplete="off" method="POST">

<div class="row">
 
 <div class="form-group col-md-12">
    <br />
	<div class="form-group col-md-12">
		<div class="row">
		<div class="form-group col-md-3">
			<label>Vehicle Number </label>
			<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" class="form-control" required="required" id="tno" name="tno">
		</div>
		
		<input id="company" name="company" type="hidden" required="required">
		
		<div class="form-group col-md-3">
			<label>Amount <font color="red">*</font></label>
			<input type="number" name="amount" id="amount" class="form-control" required="required">
		</div>
		
		<div class="form-group col-md-3">
			<label>Enter OTP <a href="#" onclick="SendOTP()">Send otp</a></label>
			<input name="otp" oninput="this.value=this.value.replace(/[^0-9]/,'')" maxlength="6" type="text" class="form-control" required="required" id="otp">
		</div>
		
		<div class="form-group col-md-3" id="func_result">
			
		</div>
	</div>
		
	<div class="row">
		
		<div class="form-group col-md-3">
			<label>Narration <font color="red">*</font></label>
			<textarea name="narration" id="narration" oninput="this.value=this.value.replace(/[^a-z A-Z0-9,-/]/,'')" class="form-control" required="required"></textarea>
		</div>
		
		<div class="form-group col-md-12">
			<button id="button1" type="submit" class="btn btn-primary">Submit</button>
		</div>
	</div>
		
	</div>
		
    </div>
	
	</div>
  </div>
</div>

</form>

</body>
</html>

          </div>
          </div>
		  </div>
       </div>         
    </section>
	
<script>
function SendOTP()
{
	var tno = $('#tno').val();
	var amount = $('#amount').val();
	
	if(tno!='' && amount!='')
	{
		if(amount>0)
		{
			$('#tno').attr('readonly',true);
			$('#amount').attr('readonly',true);
			
			$("#loadicon").show();
				jQuery.ajax({
					url: "_send_otp_atm_issue_credit.php",
					data: 'tno=' + tno + '&amount=' + amount,
					type: "POST",
					success: function(data) {
						$("#func_result").html(data);
					},
				error: function() {}
			});
		}
		else
		{
			alert('invalid amount !');
		}
	}
	else
	{
		alert('Please enter vehicle number and amount !');
	}
}
</script>	
<?php
include "footer.php";
?>

<div id="func_result2"></div>