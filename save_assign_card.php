<?php
require_once("connect.php");

$requestId=date("Y:m:d_H:i:s");

$main_string = escapeString($conn,$_POST['kit_id']);
$veh_no_user = escapeString($conn,$_POST['veh_no_user']);
$main_string = explode("_", $main_string);

$kit_id = $main_string[0];
$card_no = $main_string[1];

// echo $main_string;
// echo $kit_id." ".$card_no;

$u_id = escapeString($conn,strtoupper($_POST['id']));
$userid = escapeString($conn,strtoupper($_POST['userid']));
$company = escapeString($conn,strtoupper($_POST['company']));

$chk_kyc = Qry($conn,"SELECT id FROM dairy.happay_users WHERE veh_no='$veh_no_user' AND kyc_status_happay='Approved'");

if(!$chk_kyc){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($chk_kyc)==0){
	echo "<script>
		alert('Error : Incomplete Kyc : $veh_no_user.');
		window.location.href='./assign_card.php';
	</script>";
	exit();
}

$chk_card = Qry($conn,"SELECT card_kit_id,company,veh_no,card_assigned FROM dairy.happay_card_inventory WHERE id='$kit_id'");
if(!$chk_card){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($chk_card)>0)
{
	$row_card=fetchArray($chk_card);
	
	if($row_card['card_assigned']==1)
	{
		echo "<script>
			alert('Error : Card already Assigned.');
			$('#loadicon').hide();
			$('#submit_button').attr('disabled',false);
		</script>";
		exit();
	}
	
	if($row_card['veh_no']!=$veh_no_user)
	{
		echo "<script>
			alert('Error : Card vehicle number not matching with user\'s vehicle number. $kit_id $card_no');
			$('#loadicon').hide();
			$('#submit_button').attr('disabled',false);
		</script>";
		exit();
	}
	
	// if($row_card['status']==0)
	// {
		// echo "<script>
			// alert('Error : Card is inactive.');
			// $('#loadicon').hide();
			// $('#submit_button').attr('disabled',false);
		// </script>";
		// exit();
	// }
	
	if($row_card['card_kit_id']!=$card_no)
	{
		echo "<script>
			alert('Error : Card not verified.');
			window.location.href='./assign_card.php';
		</script>";
		exit();
	}
	
	if($row_card['company']!=$company)
	{
		echo "<script>
			alert('Error : User company : $company not matching with card company : $row_card[company].');
			$('#loadicon').hide();
			$('#submit_button').attr('disabled',false);
		</script>";
		exit();
	}
}
else
{
	echo "<script>
		alert('Error : Card not found.');
		window.location.href='./assign_card.php';
	</script>";
	exit();
}

$check_pending_request = Qry($conn,"SELECT id FROM dairy.happay_card_transactions WHERE done!='1' AND card_no='$veh_no_user' AND 
card_using!='$veh_no_user'");

if(!$check_pending_request){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($check_pending_request)>0){
	echo "<script>
		alert('Error : Pending wallet load request found of vehicle no : $veh_no_user. Delete it first !');
		window.location.href='./';
	</script>";
	exit();
}

$ChkUser = Qry($conn,"SELECT id,veh_no FROM dairy.happay_users WHERE id='$u_id' AND card_id='0'");
if(!$ChkUser){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($ChkUser)==0){
	echo "<script>
		alert('Error : Card already mapped to user.');
		window.location.href='./';
	</script>";
	exit();
}

$row_user = fetchArray($ChkUser);

// UPDATE CARD KIT ID
	
$requestId2 = mt_rand().$requestId;

$data_kit_update = array(
"requestId"=>$requestId2,
"userId"=>$veh_no_user,
"metaFields"=>array(
// "Vehicle Number"=>$veh_no,
"Card Kit ID"=>$card_no,
),
);

$JsonString_Kit_Update = json_encode($data_kit_update);

$result_kit_update = HappayAPI("auth/v1/updateuser/",$JsonString_Kit_Update,$row_card['company']);
	
$result_update = json_decode($result_kit_update, true);

	if(strpos($result_kit_update,"Unauthorized") !== false)
	{ 
		closeConnection($conn);
		echo "<font color='red'><b>Error :</b> ".$result_kit_update."</font>
		<script>
			$('#loadicon').hide();
			$('#submit_button').attr('disabled',false);
		</script>";
		exit();
	} 
	
if(@$result_update['error'])
{
	closeConnection($conn);
	
	echo "<font color='red'><b>Error :</b> RequestId: $requestId2 , ".$result_update['error']['message']."</font><br><br>";	
	echo "<script>
		$('#submit_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

// UPDATE CARD KIT ID ENDSSS

// assign card to user

$data = array(
"requestId"=>$requestId,
"userId"=>$row_user['veh_no'],
"card_kit_id"=>$card_no
);

$JsonString = json_encode($data);

$result = HappayAPI("auth/v1/cards/assign_card/",$JsonString,$row_card['company']);

// echo $result."<br><br>";
// exit();

$result2 = json_decode($result, true);

	if(strpos($result,"Unauthorized") !== false)
	{ 
		closeConnection($conn);
		echo "<font color='red'><b>Error :</b> ".$result."</font>
		<script>
			$('#loadicon').hide();
			$('#submit_button').attr('disabled',false);
		</script>";
		exit();
	} 
	
	if(@$result2['error'])
	{
		closeConnection($conn);
		echo "<font color='red'><b>Error :</b> RequestId: $requestId ,".$result2['error']['message']."</font><br><br>";	
		echo "<script>
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		$data_kit_update1 = array("requestId"=>mt_rand(),"userId"=>$veh_no_user,"metaFields"=>array("Card Kit ID"=>""),);
		$JsonString_Kit_Update1 = json_encode($data_kit_update1);
		$result_kit_update1 = HappayAPI("auth/v1/updateuser/",$JsonString_Kit_Update1,$row_card['company']);
		exit();
	}
	
// assign card to user ends	

// get user card info //	

$get_user_card = array(
"requestId"=>mt_rand(),
"userId"=>$veh_no_user
);

$json_user_card = json_encode($get_user_card);

$result_user_card = HappayAPI("auth/v1/cards/get_user_cards/",$json_user_card,$row_card['company']);

// echo $result_user_card."<br>";
	
$result2_user_card = json_decode($result_user_card, true);

	if(strpos($result_user_card,"Unauthorized") !== false)
	{ 
		closeConnection($conn);
		echo "<font color='red'><b>Error :</b> ".$result_user_card."</font>
		<script>
			$('#loadicon').hide();
			$('#submit_button').attr('disabled',false);
		</script>";
		exit();
	} 
	
	if(@$result2_user_card['error'])
	{
		closeConnection($conn);
		
		echo "<font color='red'><b>Error :</b> ".$result2_user_card['error']['message']."</font><br><br>";	
		echo "<script>
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}

	foreach($result2_user_card['res_data']['card_list'] as $user_card_result)
	{
		$last_four_digits_card = $user_card_result['last_four_digits'];
	}
	
// get user card ends //	
	
StartCommit($conn);
$flag = true;
	
	$result_res=$result2['res_str'];	
	
	$result_res_update=$result_update['res_str'];			
	
	$update_user_tab = Qry($conn,"UPDATE dairy.happay_users SET card_id='$kit_id',card_kit_id='$card_no',time_of_assign='$timestamp' WHERE id='$u_id'");
	
	if(!$update_user_tab){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_card_tab = Qry($conn,"UPDATE dairy.happay_card_inventory SET card_no2='$last_four_digits_card',card_assigned='1',
	assigned_to='$row_user[veh_no]' WHERE id='$kit_id'");
	
	if(!$update_card_tab){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_card_using = Qry($conn,"UPDATE dairy.happay_card SET card_using='$veh_no_user' WHERE tno='$veh_no_user'");
	
	if(!$update_card_using){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$log_insert=Qry($conn,"INSERT INTO dairy.happay_log(req_id,tno,result,api_name,api_response,status,timestamp) VALUES 
		('$requestId','$card_no','$result_res','Card_Assign','$result','1','$timestamp')");
	
	if(!$log_insert){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$log_insert_update = Qry($conn,"INSERT INTO dairy.happay_log(req_id,tno,result,api_name,api_response,status,timestamp) VALUES 
		('$requestId2','$card_no','$result_res_update','Card_Kit_Update','$result_kit_update','1','$timestamp')");
	
	if(!$log_insert_update){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	// echo $result."<br>".$card_no."<br>".$result_kit_update;
	echo "<script>
		alert('Card Successfully Assigned.');
		document.getElementById('AssignForm').reset();
		document.getElementById('modal_hide').click();
		$('#Assign$u_id').attr('disabled',true);
		$('#Assign$u_id').html('Card Assigned');
		$('#submit_button').attr('disabled',false);
		$('#card_list_$card_no').attr('disabled',true);
		$('#loadicon').hide();
		// window.location.href='./assign_card.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./assign_card.php");
	exit();
}
		
?>