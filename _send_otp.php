<?php
require_once("./connect.php");

$tno = escapeString($conn,$_POST['tno']);
$trans_date = escapeString($conn,$_POST['trans_date']);
$branch_name = escapeString($conn,$_POST['branch_name']);
$amount = escapeString($conn,$_POST['amount']);

$trans_date2 = date("d/m/y",strtotime($trans_date));

if($tno=="" || $trans_date=="" || $branch_name=="" || $amount=="")
{
	?>
	<label>Select Trip <font color="red">*</font></label>
	<select name="trip_id" id="trip_id" class="form-control" required="required">
		<option value="">--select trip--</option> 
	</select>	
	<?php
	echo "<script>
		alert('Enter Vehicle No and Trans Date and Branch and Amount First !');
		$('#otp').attr('readonly',true);
		$('#button1').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

$otp = '1234567890';
$otp = substr(str_shuffle($otp),0,6);

$msg_template="Vehicle_no: $tno, Advance_Amount: $amount, Branch: $branch_name, Trans_Date: $trans_date2.\nOTP: $otp.\nRamanRoadways.";
// SendMsgCustom(9024281599,"Vehicle_no: $tno, Advance_Amount: $amount, Branch: $branch_name, Trans_Date: $trans_date.\nOTP: $otp.");
// MsgHappayCashAdv($tno,$amount,$branch_name,$trans_date2,$otp);

SendWAMsg($conn,9024281599,$msg_template);

$_SESSION['session_otp'] = $otp;

$get_trips = Qry($conn,"SELECT id,from_station,to_station,date(date) as start_date FROM dairy.trip WHERE tno='$tno'");

if(!$get_trips){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request.","./");
	exit();
}

if(numRows($get_trips)==0)
{
	?>
	<label>Select Trip <font color="red">*</font></label>
	<select name="trip_id" id="trip_id" class="form-control" required="required">
		<option value="">--select trip--</option> 
	</select>	
	<?php
	
	 echo "<script>
		alert('Trips not found !');
		$('#otp').attr('readonly',true);
		$('#button1').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

?>
	<label>Select Trip <font color="red">*</font></label>
	<select name="trip_id" id="trip_id" class="form-control" required="required">
		<option value="">--select trip--</option> 
<?php

while($row_trips = fetchArray($get_trips))
{
	echo "<option value='$row_trips[id]'>$row_trips[from_station] - $row_trips[to_station] : $row_trips[start_date]</option>";
} 

	echo "
	 </select>	
	 <script>
			alert('OTP Sent !');
			$('#otp').attr('readonly',false);
			$('#button1').attr('disabled',false);
			$('#loadicon').hide();
	</script>";
	exit();
?>