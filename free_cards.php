<?php
include "header.php";
exit();
?>
<div class="content-wrapper">
    <section class="content-header">
      <h4 style="font-family:Verdana">
		Free(unassigned) Cards :
      </h4>
	  
	  <style>
		label{font-family:Verdana;font-size:13px;}
		.form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
		</section>
		
   <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div class="row">
 
 <div class="form-group col-md-12">
    
	<div class="form-group col-md-12 table-responsive">
	<div id="result"></div>
	 <br />
        <table id="myTable" class="table table-bordered table-striped" style="font-family:Verdana;font-size:12px;">
        
		<tr>
			<th>#</th>
			<th>Card/Vehicle Number</th>
			<th>Company</th>
			<th>Balance</th>
			<th>Status</th>
		</tr>
          
            <?php
             $sql = Qry($conn,"SELECT id,tno,company,card_no2,balance,status FROM happay_card WHERE card_assigned=0");
              
			  if(!$sql){
				  echo getMySQLError($conn);exit();
			  }
			  
			  if(numRows($sql)==0)
			  {
				echo "<tr>
						<td colspan='12'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			  }
			 else
			 {
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
				 if($row['status']==1){
					$card_status="<b><font color='green'>Active</font></b>";
				 }
				 else{
					$card_status="<b><font color='red'>In-Active</font></b>"; 
				}
				 
				echo 
                "<tr>
					<td>$sn</td>
					<td>$row[card_no2]<br>($row[tno])</td>
					<td>$row[company]</td>
					<td>$row[balance]</td>
					<td>$card_status</td>
				</tr>
				";
				$sn++;		
              }
			}
            ?>
        </table>
      </div>
    </div>
  </div>
</div>

</body>
</html>

<script>
function AssignCard(id,company,card_no,status)
{
	$('#'+btn_name+id).attr('disabled',true);
	
	var driver_name = $('#'+driver_name+id).attr('disabled',true);
	var driver_id = $('#'+driver_id+id).attr('disabled',true);
	
	if(driver_id=='')
	{
		alert('Select Driver Name First !');
		$('#'+btn_name+id).attr('disabled',false);
	}
	else
	{
		$("#loadicon").show();
		jQuery.ajax({
			url: "free_card_assign.php",
			data: 'id=' + id + '&card_no=' + card_no + '&status=' + status + '&btn_name=' + btn_name + '&company=' + company + '&driver_id=' + driver_id,
			type: "POST",
			success: function(data) {
			$("#result").html(data);
			},
			error: function() {}
		});
	}	
}
</script>
            </div>
          </div>
		  </div>
       </div>         
    </section>
<?php
include "footer.php";
?>