<?php
include "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
      <h4 style="font-family:Verdana">
		Update KYC :
      </h4>
	  
	  <style>
	  label{font-family:Verdana;font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<script type="text/javascript">
$(document).ready(function (e) {
$("#KycForm").on('submit',(function(e) {
// $("#loadicon").show();
// $("#submit_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_kyc.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#tab_result").html(data);
	},
	error: function() 
	{} });}));});
</script>	
	
<div class="row">
 
 <div class="form-group col-md-12">
    
	<div class="form-group col-md-12 table-responsive">
	 <br />
        <table id="myTable" class="table table-bordered table-striped" style="font-family:Verdana;font-size:12px;">
          <tr>
				<th>#id</th>
				<th>First<br>Name</th>
				<th>Last<br>Name</th>
				<th>Mobile</th>
				<th>DOB</th>
				<th>Identity<br>Proof</th>
				<th>Address<br>Proof</th>
				<th>#</th>
			</tr>
          
            <?php
              $sql = Qry($conn,"SELECT id,f_name,l_name,kyc_status,mobile,dob FROM happay_users WHERE kyc_status!='2'");
              
			  if(!$sql){
				  echo getMySQLError($conn);exit();
			  }
			  
			  if(numRows($sql)==0)
			  {
				echo "<tr>
						<td colspan='12'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			  }
			 else
			 {
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
				 if($row['kyc_status']==0){	
					 $id_proof_s="<b><font color='red'>Pending</font></b>";
					 $addr_proof_s="<b><font color='red'>Pending</font></b>";
				 }else if($row['kyc_status']==1){
					 $id_proof_s="<b><font color='green'>Done</font></b>";
					 $addr_proof_s="<b><font color='red'>Pending</font></b>";
				 }else{
					$id_proof_s="<b><font color='green'>Done</font></b>";
					 $addr_proof_s="<b><font color='green'>Done</font></b>"; 
				 }
					 
				echo 
                "<tr>
					<td>$sn</td>
					<td>$row[f_name]</td>
					<td>$row[l_name]</td>
					<td>$row[mobile]</td>
					<td>".date("d/m/y",strtotime($row['dob']))."</td>	
					<td>$id_proof_s</td>
					<td>$addr_proof_s</td>
					<td>
						<input type='hidden' id='customer_name$row[id]' value='$row[f_name] $row[l_name]'>
						<button type='button' id='kyc_button$row[id]' onclick=Kyc('$row[id]','$row[mobile]','$row[kyc_status]') 
						class='btn btn-success btn-sm'>Do KYC</button>
					</td>
				</tr>
				";
				$sn++;		
              }
			}
            ?>
        </table>
      </div>
    </div>
  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>

<button type="button" id="ModalButton" style="display:none" class="btn btn-primary" data-toggle="modal" data-target="#myModal"></button>

<div class="modal" id="myModal" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title">Update KYC of : <span style="color:blue" id="customer_name"></span></h4>
      </div>
	<form id="KycForm" autocomplete="off">
      <div class="modal-body">
	  <div id="tab_result"></div>
		 <div class="row">
			
			<div class="form-group col-md-12" id="doc_type_div">
				<label>Document Type : <font color="red">*</font></label>
				<select name="doc_type" id="doc_type" class="form-control" required>
					<option value="">--Select Type--</option>
					<option value="pan_card">Pan Card</option>
					<option value="driving_license">Driving License</option>
					<option value="passport">Passport</option>
					<option value="voter_id">Voter Id</option>
				</select>
			</div>
			
			<div class="form-group col-md-12" id="doc_no_div">
				<label>Document Number : <font color="red">*</font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9-/]/,'')" class="form-control" id="doc_no" name="doc_no" required />
			</div>
			
			<div class="form-group col-md-6" id="poi_div" style="display:none">
				<label>Identity Proof : <font color="red">*</font></label>
				<input type="file" class="form-control" id="poi" name="poi" required />
			</div>
			
			<div class="form-group col-md-6" id="poa_div" style="display:none">
				<label>Address Proof : <font color="red">*</font></label>
				<input type="file" class="form-control" id="poa" name="poa" required />
			</div>
			
		</div>
			<input type="hidden" id="id_box" name="id">	
			<input type="hidden" id="userid_box" name="userid">	
			<input type="hidden" id="kyc_status" name="kyc_status">	
      </div>

      <div class="modal-footer">
        <button type="submit" id="submit_button" class="btn btn-primary">Update KYC</button>
        <button type="button" onclick="MyFunc1($('#id_box').val())" id="close_modal_button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="button" id="modal_hide" data-dismiss="modal" style="display:none">das</button>
      </div>
	</form>
    </div>
  </div>
</div>	

<script>	
function Kyc(id,userid,kyc_status)
{
	$('#kyc_button'+id).attr('disabled',true);
	var customer_name = $('#customer_name'+id).val();
	
	$('#customer_name').html(customer_name);
	$('#id_box').val(id);
	$('#userid_box').val(userid);
	$('#kyc_status').val(kyc_status);
	
	if(kyc_status==0)
	{
		$('#doc_type_div').show();
		$('#doc_no_div').show();
		$('#doc_type').attr('required',true);
		$('#doc_no').attr('required',true);
		
		$('#poi_div').show();
		$('#poa_div').hide();
		$('#poi').attr('required',true);
		$('#poa').attr('required',false);
		document.getElementById('ModalButton').click();
		$('#loadicon').hide();
	}
	else if(kyc_status==1)
	{
		$('#doc_type_div').hide();
		$('#doc_no_div').hide();
		$('#doc_type').attr('required',false);
		$('#doc_no').attr('required',false);
		
		$('#poi_div').hide();
		$('#poa_div').show();
		$('#poi').attr('required',false);
		$('#poa').attr('required',true);
		document.getElementById('ModalButton').click();
		$('#loadicon').hide();
	}
	else
	{
		alert('Kyc Already Updated.');
		window.location.href='./';
		$('#poi_div').hide();
		$('#poa_div').hide();
		$('#poi').attr('required',true);
		$('#poa').attr('required',true);
	}
}

function MyFunc1(id){
	$('#kyc_button'+id).attr('disabled',false);
}
</script>

<?php
include "footer.php";
?>