<?php
include "header.php";
?>
<script type="text/javascript">
	$(function() {
		$("#tno").autocomplete({
		source: 'autofill/get_tno.php',
		select: function (event, ui) { 
              $('#tno').val(ui.item.value);   
              $('#company').val(ui.item.company);   
             $("#button1").attr('disabled',false);				   
             return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#tno").val('');
			$("#company").val('');
			$("#tno").focus();
			$("#button1").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
		});});
</script> 

<script type="text/javascript">
$(document).ready(function (e) {
	$("#Form1").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#button1").attr("disabled", true);
		$.ajax({
        	url: "./move_to_cash_save.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#func_result2").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>

<div class="content-wrapper">
    <section class="content-header">
      <h4>
		Active cash & De-activate Happay :
      </h4>
	  
	  <style>
		.form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
		</section>
		
   <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<form id="Form1" autocomplete="off" method="POST">

<div class="row">
 
 <div class="form-group col-md-12">
    <br />
	<div class="form-group col-md-12">
		<div class="row">
		
		<div style="color:red" class="form-group col-md-12">
			Before switching to Cash, make sure Wallet amount must be zero.
		</div>

		<div class="form-group col-md-3">
			<label>Vehicle Number <font color="red">*</font></label>
			<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" class="form-control" required="required" id="tno" name="tno">
		</div>
		
		<input id="company" name="company" type="hidden" required="required">
		
		<div class="form-group col-md-3">
			<label>Enter OTP <a href="#" onclick="SendOTP()">Send otp</a></label>
			<input name="otp" oninput="this.value=this.value.replace(/[^0-9]/,'')" maxlength="6" type="text" class="form-control" required="required" id="otp">
		</div>
		
		<div class="form-group col-md-3">
			<label>&nbsp;</label>
			<br />
			<button id="button1" type="submit" class="btn btn-primary">Submit</button>
		</div>

	</div>
		
    </div>
	
	</div>
  </div>
</div>

</form>

</body>
</html>

          </div>
          </div>
		  </div>
       </div>         
   </section>
	
<script>
function SendOTP()
{
	var tno = $('#tno').val();
	
	if(tno!='')
	{
		$('#tno').attr('readonly',true);
		
		$("#loadicon").show();
				jQuery.ajax({
					url: "_send_otp_move_to_cash.php",
					data: 'tno=' + tno,
					type: "POST",
					success: function(data) {
						$("#func_result2").html(data);
					},
				error: function() {}
			});
	}
	else
	{
		alert('Please enter vehicle number !');
	}
}
</script>	
<?php
include "footer.php";
?>

<div id="func_result2"></div>