<?php
require_once("connect.php");

$requestId=date("Y:m:d_H:i:s");

$u_id = escapeString($conn,strtoupper($_POST['id']));
$userid = escapeString($conn,strtoupper($_POST['userid']));
$kyc_status = escapeString($conn,strtoupper($_POST['kyc_status']));
$document_number = escapeString($conn,strtoupper($_POST['document_number']));
$company = escapeString($conn,strtoupper($_POST['company']));
$veh_no = escapeString($conn,strtoupper($_POST['veh_no']));

$check_kyc_happay = Qry($conn,"SELECT id FROM dairy.happay_users WHERE id='$u_id' AND kyc_status_happay='Approved'");
if(!$check_kyc_happay){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error !","./update_kyc.php");
	exit();
}

if(numRows($check_kyc_happay)>0)
{
		echo "<script>
			alert('Kyc approved already.');
			window.location.href='./update_kyc.php';
		</script>";
		exit();
}

$verify_user = Qry($conn,"SELECT veh_no,kyc_status,document_no FROM dairy.happay_users WHERE id='$u_id'");
if(!$verify_user){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error !","./update_kyc.php");
	exit();
}

if(numRows($verify_user)==0)
{
		echo "<script>
			alert('Error : user not found.');
			window.location.href='./update_kyc.php';
		</script>";
		exit();
}

$row_verify = fetchArray($verify_user);

if($row_verify['veh_no']!=$veh_no)
{
	echo "<script>
		alert('Error : vehicle number not verified.');
		window.location.href='./update_kyc.php';
	</script>";
	exit();
}

if($row_verify['document_no']!=$document_number)
{
	echo "<script>
		alert('Error : Document number not verified.');
		window.location.href='./update_kyc.php';
	</script>";
	exit();
}

if($row_verify['kyc_status']!=$kyc_status)
{
	echo "<script>
		alert('Error : Kyc status not verified.');
		window.location.href='./update_kyc.php';
	</script>";
	exit();
}
	
if($document_number=='')
{
	// Update Min Kyc	
	
	$chk_user=Qry($conn,"SELECT id FROM dairy.happay_users WHERE id='$u_id' AND document_no!=''");
	
	if(!$chk_user){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error !","./update_kyc.php");
		exit();
	}

	if(numRows($chk_user)>0)
	{
		echo "<script>
			alert('Error : Minimun KYC already updated for this user.');
			window.location.href='./update_kyc.php';
		</script>";
		exit();
	}
	
	$doc_type = escapeString($conn,($_POST['doc_type']));
	$doc_no = escapeString($conn,($_POST['doc_no']));
	
	if($doc_no=="" || $doc_type=="")
	{
		echo "<script>
			alert('Error : Empty Values for Minimun KYC.');
			window.location.href='./update_kyc.php';
		</script>";
		exit();
	}
	
	$data_kyc = array(
	"requestId"=>$requestId,
	"userId"=>$veh_no,
	"kyc_type"=>$doc_type,
	"kyc_detail"=>$doc_no
	);

	$JsonString_KYC = json_encode($data_kyc);

	$result_kyc = HappayAPI("auth/v1/cards/upload_minimum_kyc/",$JsonString_KYC,$company);

	$result2_kyc = json_decode($result_kyc, true);

	if(strpos($result_kyc,"Unauthorized") !== false)
	{ 
		echo "<font color='red'><b>Error :</b> ".$result_kyc."</font>";
		echo "<script>
			document.getElementById('KycForm').reset();
			$('#loadicon').hide();
		</script>";
		exit();
	} 
	
	if(!empty($result2_kyc['error']['message']))
	{
		echo "<font color='red'><b>Error :</b> ".$result2_kyc['error']['message']."</font><br><br>";
		echo "<script>
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
			document.getElementById('KycForm').reset();
		</script>";
		exit();
	}
	
	echo "<font color='green'><b>SUCCESS :</b> ".$result2_kyc['res_str']."</font><br><br>";
		
	$update_min_kyc = Qry($conn,"UPDATE dairy.happay_users SET doc_type='".strtoupper($doc_type)."',document_no='".strtoupper($doc_no)."' 
	WHERE id='$u_id'");	
	
	if(!$update_min_kyc){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error !","./update_kyc.php");
		exit();
	}
	
	$log_insert=Qry($conn,"INSERT INTO dairy.happay_log(req_id,tno,result,api_response,status,timestamp) VALUES 
		('$requestId','$u_id','$result2_kyc[res_str]','$result_kyc','1','$timestamp')");
	
	if(!$log_insert){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error !","./update_kyc.php");
		exit();
	}
	
	$log_docs=Qry($conn,"INSERT INTO dairy.happay_kyc(veh_no,doc_type,doc_no,timestamp) VALUES 
		('$veh_no','$doc_type','$doc_no','$timestamp')");
	
	if(!$log_docs){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error !","./update_kyc.php");
		exit();
	}
	
		echo "<script>
			alert('SUCCESS : $result2_kyc[res_str] !');
			$('#doc_no_box$u_id').val('".strtoupper($doc_no)."');
			$('#doc_type_td$u_id').html('".strtoupper($doc_type)."');
			$('#doc_no_td$u_id').html('".strtoupper($doc_no)."');
			$('#kyc_status_td$u_id').html('<font color=red><b>Document Pending</b></font>');
			$('#submit_button').attr('disabled',false);
			$('#kyc_button$u_id').attr('disabled',false);
			document.getElementById('modal_hide').click(); 
			document.getElementById('KycForm').reset();
			$('#loadicon').hide();
		</script>";
		exit();
	
}
else
{
	// Update Full Kyc
	
// check api kyc start
	
$check_kyc_json = array(
	"requestId"=>"CHECK1234",
	"userId"=>$veh_no
);

$json_kyc_check = json_encode($check_kyc_json);

$result_kyc_check = HappayAPI("auth/v1/cards/get_user_info/",$json_kyc_check,$company);
$result2_kyc_check = json_decode($result_kyc_check, true);

	if(strpos($result_kyc_check,"Unauthorized") !== false)
	{ 
		echo "<font color='red'><b>Error :</b> ".$result_kyc_check."</font>";
		echo "<script>
			document.getElementById('KycForm').reset();
			$('#loadicon').hide();
		</script>";
		exit();
	} 
	
	if(!empty($result2_kyc_check['error']['message']))
	{
		echo "<font color='red'><b>Error :</b> ".$result2_kyc_check['error']['message']."</font><br><br>";
		echo "<script>
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
			document.getElementById('KycForm').reset();
		</script>";
		exit();
	}
	
if($result2_kyc_check['res_data']['kyc_status_details']=='Approved')
{
	echo "<script>
		alert('Kyc approved already.');
		window.location.href='./update_kyc.php';
		$('#loadicon').hide();
	</script>";
	exit();
}

// check api kyc ends	
	
$fix_name = mt_rand().date('dmYHis');
	
	if($kyc_status=="0"){
		$doc_type="POI";
		$update_var="1";
		$targetPath_1="kyc_docs/front/".$fix_name;
		$col_name="front_copy";
	}else if($kyc_status=="1"){
		$doc_type="POA";
		$update_var="2";
		$targetPath_1="kyc_docs/rear/".$fix_name;
		$col_name="rear_copy";
	}else if($kyc_status=="2"){
		$doc_type="PHOTO";
		$update_var="3";
		$targetPath_1="kyc_docs/photo/".$fix_name;
		$col_name="photo";
	}else{
		echo "<script>
			alert('Warning : Full KYC already updated !');
			window.location.href='./update_kyc.php';
		</script>";
		exit();
	}
	
	if($company=='RRPL')
	{
		$token="lmfIAJzy1RZPsSZ50hwWcLpEa";
	}
	else if($company=='RAMAN_ROADWAYS')
	{
		$token="icaxFOPnZGn9zxQHFslAQxEt2";
	}
	else
	{
		echo "<script>
			alert('Warning : Invalid Company Name !');
			window.location.href='./update_kyc.php';
		</script>";
		exit();
	}
	
$pathoffile=$_FILES[$doc_type]['tmp_name'];
$filename=$_FILES[$doc_type]['name'];
$filetype=$_FILES[$doc_type]['type'];

$valid_types = array("image/jpg", "image/jpeg", "image/png");
	
	if(!in_array($filetype, $valid_types))
	{
		echo "<script>
			alert('Warning : Only Image Upload Allowed. JPG or PNG');
			$('#submit_button').attr('disabled',false);
			document.getElementById('KycForm').reset();
			$('#loadicon').hide();
		</script>";
		exit();
	}
  
$url = 'https://api-v2.happay.in/auth/v1/cards/upload_kyc_documents/';

$headers = array();
$headers[] = 'Content-Type: multipart/form-data';
$headers[] = 'Authorization: Bearer '.$token.'';
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLINFO_HEADER_OUT, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_POST, true);

curl_setopt($ch, CURLOPT_POSTFIELDS, array(
'kyc_image' => curl_file_create($pathoffile),
'userId' => $veh_no,
'requestId' => $requestId,
'kyc_document_type' => $doc_type,
));

$result_kyc = curl_exec($ch);
$responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
curl_close($ch);

$result2_kyc = json_decode($result_kyc, true);

	if(strpos($result_kyc,"Unauthorized") !== false)
	{ 
		echo "<font color='red'><b>Error :</b> ".$result_kyc."</font>";
		echo "<script>
			document.getElementById('KycForm').reset();
			$('#loadicon').hide();
		</script>";
		exit();
	} 
	
	if(!empty($result2_kyc['error']['message']))
	{
		echo "<font color='red'><b>Error :</b> ".$result2_kyc['error']['message']."</font><br><br>";
		echo "<script>
			$('#submit_button').attr('disabled',false);
			document.getElementById('KycForm').reset();
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
		echo "<font color='green'><b>SUCCESS :</b> ".$result2_kyc['res_str']."</font><br><br>";
		
	$update_min_kyc = Qry($conn,"UPDATE dairy.happay_users SET kyc_status='$update_var' WHERE id='$u_id'");	
	
	if(!$update_min_kyc){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error !","./update_kyc.php");
		exit();
	}
	
	$log_insert=Qry($conn,"INSERT INTO dairy.happay_log(req_id,tno,result,api_response,status,timestamp) VALUES 
		('$requestId','$u_id','$result2_kyc[res_str]','$result_kyc','1','$timestamp')");
	
	if(!$log_insert){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error !","./update_kyc.php");
		exit();
	}
	
	$sourcePath = $_FILES[$doc_type]['tmp_name'];
	
	$targetPath = $targetPath_1.".".pathinfo($_FILES[$doc_type]['name'],PATHINFO_EXTENSION);
	
	if(($_FILES[$doc_type]['size']/1024/1024)>2)
	{
		ImageUpload(600,450,$sourcePath);
	}

	if(!move_uploaded_file($sourcePath,$targetPath))
	{
		Redirect("Unable to Upload PAN Card Copy.","./smemo.php");
		exit();
	}
	
	$log_docs=Qry($conn,"UPDATE dairy.happay_kyc SET `$col_name`='$targetPath' WHERE veh_no='$veh_no'");
	
	if(!$log_docs){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error !","./update_kyc.php");
		exit();
	}
	
	if($update_var==1)
	{
		$color="green";
		$msg="Addr. Proof<br>Pending";
	}
	else if($update_var==2)
	{
		$color="green";
		$msg="Photo<br>Pending";
	}
	else
	{
		$color="green";
		$msg="KYC Done";
	}
	
		echo "<script>
			alert('SUCCESS : $result2_kyc[res_str] !');
			document.getElementById('KycForm').reset();
			$('#kyc_status_box$u_id').val('$update_var');
			$('#kyc_status_td$u_id').html('<font color=$color><b>$msg</b></font>');
			$('#submit_button').attr('disabled',false);
			$('#kyc_button$u_id').attr('disabled',false);
			document.getElementById('modal_hide').click(); 
			$('#loadicon').hide();
		</script>";
	exit();
}
?>