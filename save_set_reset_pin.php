<?php
require_once("./connect.php");

include('vendor/autoload.php');

$kit_id=escapeString($conn,strtoupper($_REQUEST['kit_id']));
$veh_no=escapeString($conn,strtoupper($_REQUEST['veh_no']));
$company=escapeString($conn,strtoupper($_REQUEST['company']));
$pin_no=escapeString($conn,strtoupper($_REQUEST['pin_no']));

if(strlen($pin_no)!=4)
{
	echo "<script>
		alert('Check Pin number !');	
		$('#submit_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($kit_id=='')
{
	echo "<script>
		alert('Card not found !');	
		$('#submit_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($company=='')
{
	echo "<script>
		alert('Company not found !');	
		$('#submit_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

//get token

	$requestId = mt_rand();
	$requestId2 = mt_rand();

	$data1 = array(
	"requestId"=>$requestId,
	"card_kit_id"=>$kit_id
	);

	$JsonString_KYC = json_encode($data1);

	$result_kyc = HappayAPI("auth/v1/cards/initiate_pin_set/",$JsonString_KYC,$company);

	$result2_kyc = json_decode($result_kyc, true);

	if(strpos($result_kyc,"Unauthorized") !== false)
	{ 
		echo "<font color='red'><b>Error :</b> ".$result_kyc."</font>";
		echo "<script>
			alert('$result_kyc');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	} 
	
	if(!empty($result2_kyc['error']['message']))
	{
		$err_msg = $result2_kyc['error']['message'];
		echo "<font color='red'><b>Error :</b> ".$err_msg."</font>";
		echo "<script>
			alert('$err_msg');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$pin_token = $result2_kyc['res_data']['pin_set_token'];
	$publickey = $result2_kyc['res_data']['public_encryption_key'];

function EncryptPIN($pubic_key,$pin){
	$b64_public_encryption_key = $pubic_key;
	$public_encryption_key = base64_decode($b64_public_encryption_key);
	$encryptedPin = "";
	$rsa = new \phpseclib\Crypt\RSA();
	$rsa->loadKey($public_encryption_key);
	$rsa->setHash('sha256');
	$rsa->setMGFHash('sha256');
	$rsa->setEncryptionMode(\phpseclib\Crypt\RSA::ENCRYPTION_OAEP);
	return $encryptedPin = base64_encode($rsa->encrypt($pin));
}

$pin_no = EncryptPIN($publickey,$pin_no);

$data2_pin = array(
	"requestId"=>$requestId2,
	"pin_set_token"=>$pin_token,
	"pin"=>$pin_no
	);

	$JsonString_pin = json_encode($data2_pin);

	$result_pin = HappayAPI("auth/v1/cards/card_set_pin/",$JsonString_pin,$company);

	$result2_pin = json_decode($result_pin, true);

	if(strpos($result_pin,"Unauthorized") !== false)
	{ 
		echo "<font color='red'><b>Error :</b> ".$result_pin."</font>";
		echo "<script>
			alert('$result_pin');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	} 
	
	if(!empty($result2_pin['error']['message']))
	{
		$err_msg = $result2_pin['error']['message'];
		echo "<font color='red'><b>Error :</b> ".$err_msg."</font>";
		echo "<script>
			alert('$err_msg');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	echo "<font color='green'><b>SUCCESS :</b> ".$result2_pin['res_str']."</font>";
	
		echo "<script>
			alert('SUCCESS : $result2_pin[res_str] !');
			$('#submit_button').attr('disabled',true);
			$('#submit_button').html('PIN UPDATED');
			$('#loadicon').hide();
		</script>";
		exit();
?>