<?php
require_once("connect.php");

$requestId=date("Y:m:d_H:i:s");
$date=date("Y-m-d");
$timestamp=date("Y-m-d H:i:s");

$id = escapeString($conn,strtoupper($_POST['id']));
$amount = escapeString($conn,strtoupper($_POST['amount']));
$narration = escapeString($conn,strtoupper($_POST['narration']));
$otp = escapeString($conn,strtoupper($_POST['otp']));

if($amount=='' || $amount==0 || $amount<=0){
	echo "<script>
		alert('Invalid amount entered !!');
		$('#loadicon').hide();
		$('#button1$id').attr('disabled',false);
	</script>";
	exit();	
}

if($otp==''){
	echo "<script>
		alert('Otp not found !!');
		$('#loadicon').hide();
		$('#button1$id').attr('disabled',false);
	</script>";
	exit();	
}

if($narration==''){
	echo "<script>
		alert('Narration not found !!');
		$('#loadicon').hide();
		$('#button1$id').attr('disabled',false);
	</script>";
	exit();	
}

// $url="https://www.smsschool.in/api/verifyRequestOTP.php";
// $postData = array(
    // 'authkey' => "242484ARRP024r65bc082c9",
    // 'mobile' => 9024281599,
    // 'otp' => $otp
// );

// $ch = curl_init();
// curl_setopt_array($ch, array(
// CURLOPT_URL => $url,
// CURLOPT_RETURNTRANSFER => true,
// CURLOPT_POST => true,
// CURLOPT_POSTFIELDS => $postData
// ));

// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

// $output = curl_exec($ch);

// if(curl_errno($ch))
// {
	// echo "<script>
		// alert('SMS API Failure !');
		// $('#loadicon').hide();
		// $('#button1$id').attr('disabled',false);
	// </script>";
	// exit();	
// }

// $result = json_decode($output, true);
// curl_close($ch);

// if($result['type']=='error')
// {
	// echo "<script>
			// alert('Error : $result[message].');
			// $('#loadicon').hide();
			// $('#button1$id').attr('disabled',false);
		// </script>";
	// exit();	
// }
// else
// {
	// if($result['message']!='otp_verified')
	// {
		// echo "<script>
			// alert('Error : $result[message].');
			// $('#loadicon').hide();
			// $('#button1$id').attr('disabled',false);
		// </script>";
		// exit();	
	// }
// }

VerifyOTP($otp,"button1$id");

	// if($otp!=$_SESSION['session_otp'])
	// {
		// echo "<script>
				// alert('Error : Invalid OTP entered.');
				// $('#button1$id').attr('disabled',false);
				// $('#loadicon').hide();
		// </script>";
		// exit();	
	// }
	
$chk_running_file = Qry($conn,"SELECT id FROM dairy.running_scripts");
if(!$chk_running_file){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($chk_running_file)>0)
{
	echo "<script>
		alert('Please wait while api transaction are loading !');
		$('#loadicon').hide();
		$('#button1$id').attr('disabled',false);
	</script>";
	exit();
}

$getCardBal = Qry($conn,"SELECT veh_no,card_kit_id,company,card_status as status FROM dairy.happay_card_inventory WHERE id='$id'");
if(!$getCardBal){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing request.","withdraw_wallet_direct_cr.php");
	exit();
}

if(numRows($getCardBal)==0){
	echo "<script>
			alert('Error : card not found.');
			$('#loadicon').hide();
			$('#button1$id').attr('disabled',false);
		</script>";
	exit();
}

$row_card_info = fetchArray($getCardBal);

if($row_card_info['status']!=1)
{
	echo "<script>
			alert('Error : card is in-active.');
			$('#loadicon').hide();
			$('#button1$id').attr('disabled',false);
		</script>";
	exit();
}

$tno = $row_card_info['veh_no'];

$card_bal = Qry($conn,"SELECT balance FROM dairy.happay_live_balance WHERE veh_no='$row_card_info[veh_no]'");
if(!$card_bal){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing request.","withdraw_wallet_direct_cr.php");
	exit();
}

if(numRows($card_bal)==0){
	echo "<script>
			alert('Error : card balance not found.');
			$('#loadicon').hide();
			$('#button1$id').attr('disabled',false);
		</script>";
	exit();
}

$rowBal = fetchArray($card_bal);

if($amount>$rowBal['balance'])
{
	echo "<script>
			alert('Error : Requested Withdrawn amount is greater than balance amount.');
			$('#loadicon').hide();
			$('#button1$id').attr('disabled',false);
		</script>";
	exit();
}

if($amount==0 || $amount=='')
{
	echo "<script>
			alert('Error : Invalid amount entered.');
			$('#loadicon').hide();
			$('#button1$id').attr('disabled',false);
		</script>";
	exit();
}

$newBalance = $rowBal['balance']-$amount;
$company = $row_card_info['company'];

/// Check Current Bal of Card //////

	$ReqID_Verify = date("Y-m-d_H:i:s");
	$verify_Payload = array(
	"requestId"=>$ReqID_Verify,
	"userId"=>$tno
	);
	$verify_Payload = json_encode($verify_Payload);
	$result_Api_Verify = HappayAPI("auth/v1/cards/get_wallet_balance/",$verify_Payload,$company);
	$result_decoded_Verify = json_decode($result_Api_Verify, true);

	if(strpos($result_Api_Verify,"Unauthorized") !== false)
	{ 
		closeConnection($conn);
		echo "<font color='red'><b>Error :</b> ".$result."</font>
		<script>
			$('#loadicon').hide();
			$('#button1$id').attr('disabled',false);
		</script>";
		exit();
	} 
	
	if(@$result_decoded_Verify['error'])
	{
		$error_msg_Verify = $result_decoded_Verify['error']['message'];
		$insertApiLog_Verify = Qry($conn,"INSERT INTO dairy.happay_api_log(req_id,tno,result,api_name,api_response,status,timestamp) 
		VALUES ('$ReqID_Verify','$tno','$error_msg_Verify','GET_WALLET_BALANCE','$result_Api_Verify','0','$timestamp')");
		
		if(!$insertApiLog_Verify){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		echo "<script>
				alert('Error : API Error : $error_msg_Verify.');
				$('#loadicon').hide();
				$('#button1$id').attr('disabled',false);
		</script>";
		exit();
	}
	else
	{
		foreach($result_decoded_Verify['res_data'] as $data_Bal)
		{
			$verify_Bal = $data_Bal['current_balance'];			
		}			
	
		if($rowBal['balance']!=$verify_Bal)
		{
			// SendMsgCustom(9024281599,"$tno: Balance not matching. API and System Balance is : $verify_Bal and $rowBal[balance].");
			
			echo "<script>
				alert('Error : Card balance not verified. Please try agian after 5 minutes.');
				$('#loadicon').hide();
				$('#button1$id').attr('disabled',false);
			</script>";
			exit();
		}
	}
	
/// Check Current Bal of Card ends //////

	$trans_id_Qry=GetTransIdHappayDirectWdl("HPWD_DIR",$conn);
	
	if(!$trans_id_Qry || $trans_id_Qry=="" || $trans_id_Qry=="0")
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error While Fetching Transaction Id.","./");
		exit();
	}
	
	$trans_id = $trans_id_Qry;

$data = array(
"requestId"=>$requestId,
"userId"=>$row_card_info['veh_no'],
"withdraw_amount"=>$amount,
"wallet_name"=>"Imprest"
);

$JsonString = json_encode($data);

$result = HappayAPI("auth/v1/cards/withdraw_user_wallet/",$JsonString,$company);
$result_decoded = json_decode($result, true);

if(@$result_decoded['error'])
{
	$error_msg = $result_decoded['error']['message'];
	echo "<font color='red'><b>Error :</b> ".$error_msg."</font>";	
	
	$insertApiLog = Qry($conn,"INSERT INTO dairy.happay_api_log(req_id,tno,result,api_name,api_response,status,timestamp) VALUES 
	('$requestId','$row_card_info[veh_no]','$error_msg','WALLET_WITHDRAW_DIRECT','$result','0','$timestamp')");
	
	if(!$insertApiLog){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	echo "<script>
		alert('Error : $error_msg !');
		$('#button1$id').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}
else
{
	if(empty($result_decoded['res_data']['txn_id']))
	{
		echo "$result
		<script>
			alert('Happay server not responding. Please try after some time !');
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$Api_Response = $result_decoded['res_str'];	
	$txn_id = $result_decoded['res_data']['txn_id'];			
	
	$txn_type = $result_decoded['res_data']['txn_type']['name'];
	$txn_type_id = $result_decoded['res_data']['txn_type']['id'];
	
	$txn_amount = $result_decoded['res_data']['currency_amount'];
	$txn_date = $result_decoded['res_data']['txn_date'];
		
	$diff = (strtotime($timestamp)-strtotime($txn_date)/3600);
	
	if($diff>5)
	{
		$txn_date = date('Y-m-d H:i:s',strtotime('+5 hour +30 minutes',strtotime(substr($txn_date,0,19))));
	}
	else
	{
		$txn_date = $txn_date;
	}
	
	$merchant = $result_decoded['res_data']['payee_merchant'];
	
	$txn_user = $result_decoded['res_data']['user']['name'];
	
	$insertApiLog = Qry($conn,"INSERT INTO dairy.happay_api_log(req_id,tno,result,api_name,api_response,status,timestamp) VALUES 
	('$requestId','$row_card_info[veh_no]','$Api_Response','WALLET_WITHDRAW_DIRECT','$result','1','$timestamp')");
	
	if(!$insertApiLog){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing request.","withdraw_wallet_direct_cr.php");
		exit();
	}
}

StartCommit($conn);
$flag = true;

$insert_Trans = Qry($conn,"INSERT INTO dairy.happay_card_withdrawal (tno,trans_id,amount,narration,timestamp) VALUES 
('$tno','$trans_id','$amount','$narration','$timestamp')");

if(!$insert_Trans){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$updateMain_bal = Qry($conn,"UPDATE dairy.happay_main_balance SET api_bal=api_bal+'$amount' WHERE company='$company'");
if(!$updateMain_bal){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$updateTransDone = Qry($conn,"UPDATE dairy.happay_api_log SET completed='1' WHERE req_id='$requestId'");
if(!$updateTransDone){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<font color='green'><b>SUCCESS :</b> ".$Api_Response."</font>";	
	echo "<script>
		alert('Wallet Withdrawn Successfully.');
		$('#loadicon').hide();
	</script>"; 
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./withdraw_wallet_direct_cr.php");
	exit();
}	
	
?>