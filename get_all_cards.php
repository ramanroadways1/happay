<?php
include "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
      <h4 style="font-size:15px">
		Available / Active Cards :
      </h4>
	  
	  <style>
		.form-control{text-transform:uppercase;}
	  </style>
	
	
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div class="row">
 
 <div class="form-group col-md-12">
    
	<div class="form-group col-md-12 table-responsive">
	<div id="result"></div>
	 <br />
       <table id="example" class="table table-bordered table-striped" style="font-size:12px;">
		<thead>		
		 <tr>
				<th>#</th>
				<th>Vehicle Number</th>
				<th>Assigned_To</th>
				<th>Email-Id</th>
				<th>Mobile</th>
				<th>Password</th>
				<th>Company</th>
				<th>is Assigned</th>
				<th>Balance</th>
				<th>Card Status</th>
				<!--<th>Detach<br>Card</th>-->
				<th>Set PIN</th>
				<th>Toggle Status</th>
				<th>Detach Card</th>
			</tr>
          </thead>
		 <tbody> 
            <?php
              $sql = Qry($conn,"SELECT c.id,c.veh_no,c.card_kit_id,c.company,c.card_assigned,b.balance,c.card_status,
			  CONCAT(u.f_name,' ',u.l_name) as emboss_name,u.email,u.mobile,u.password 
			  FROM dairy.happay_card_inventory AS c 
			  LEFT OUTER JOIN dairy.happay_live_balance AS b ON b.veh_no=c.veh_no
			  LEFT OUTER JOIN dairy.happay_users AS u ON u.card_id=c.id
			  ");
              
			  if(!$sql){
				echo getMySQLError($conn);
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
				exit();
			}

			  
			  if(numRows($sql)==0)
			  {
				echo "<tr>
						<td colspan='12'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			  }
			 else
			 {
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
				 if($row['card_assigned']==1){
					$card_assigned="<b><font color='green'>YES</font></b>"; 
					$btn_assign="<button type='button' id='DetachButton$row[id]' onclick=ActionOnCard('$row[id]','$row[company]','$row[veh_no]','DETACH','DetachButton') 
						class='btn btn-primary btn-xs'><li class='fa fa-power-off'></li> Detach Card</button>";
				}
				 else{
					$card_assigned="<b><font color='red'>NO</font></b>"; 
					$btn_assign="<b><font color='red'>un-assigned</font></b>";
					// $email_id="";
				 }
				 
				 if($row['card_status']==1){
					$card_status="<b><font color='green'>Active</font></b>";
					
					$btn_toggle="<button type='button' id='ToggleBtn$row[id]' onclick=ActionOnCard('$row[id]','$row[company]','$row[veh_no]','BLOCK','ToggleBtn') 
					class='btn btn-danger btn-xs'><li class='fa fa-toggle-off'></li> Block Card</button>";
				 }
				 else{
					$card_status="<b><font color='red'>In-Active</font></b>"; 
					
					$btn_toggle="<button type='button' id='ToggleBtn$row[id]' onclick=ActionOnCard('$row[id]','$row[company]','$row[veh_no]','ACTIVE','ToggleBtn') 
					class='btn btn-success btn-xs'><li class='fa fa-toggle-off'></li> Active Card</button>";
				 }
				 
				echo 
                "<tr>
					<td>$sn</td>
					<td>$row[veh_no]<br>($row[card_kit_id])</td>
					<td>$row[emboss_name]</td>
					<td>$row[email]</td>
					<td>$row[mobile]</td>
					<td>$row[password]</td>
					<td>$row[company]</td>
					<td>$card_assigned</td>
					<td>$row[balance]</td>
					<td>$card_status</td>
					<td><button type='button' id='SetPinBtn$row[id]' onclick=SetPin('$row[id]','$row[company]','$row[card_kit_id]','$row[veh_no]') 
					class='btn btn-success btn-xs'><li class='fa fa-credit-card'></li> Set/Reset PIN</button></td>
					<td>$btn_toggle</td>
					<td><input type='hidden' value='$row[card_kit_id]' id='kit_id$row[id]'>
					$btn_assign</td>
				</tr>
				"; //<td>$btn_assign</td>
				$sn++;		
              }
			}
            ?>
		</tbody>	
        </table>
      </div>
	  <div id="pin_result"></div>
    </div>
  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>

<script>	
function ActionOnCard(id,company,card_no,action,btn_name)
{
	if(confirm("Are you sure ?")==true)
	{
		$('#'+btn_name+id).attr('disabled',true);
		
		$("#loadicon").show();
		jQuery.ajax({
			url: "action_on_card.php",
			data: 'id=' + id + '&card_no=' + card_no + '&action=' + action + '&btn_name=' + btn_name + '&company=' + company,
			type: "POST",
			success: function(data) {
			$("#result").html(data);
			},
			error: function() {}
		});
	}
}

function SetPin(id,company,kit_id,veh_no)
{
	$('#SetPinBtn'+id).attr('disabled',true);
	
	$("#loadicon").show();
	jQuery.ajax({
		url: "set_reset_pin.php",
		data: 'id=' + id + '&kit_id=' + kit_id + '&company=' + company + '&veh_no=' + veh_no,
		type: "POST",
		success: function(data) {
			$("#pin_result").html(data);
		},
		error: function() {}
	});
}

function UpdatePinForm()
{
	var kit_id = $('#kit_id_db').val();
	var veh_no = $('#veh_no_db').val();
	var company = $('#company_db').val();
	var pin_no_db = $('#pin_no_db').val();
	
	$("#submit_button").attr("disabled", true);
	$("#loadicon").show();
	jQuery.ajax({
		url: "save_set_reset_pin.php",
		data: 'kit_id=' + kit_id + '&company=' + company + '&veh_no=' + veh_no + '&pin_no=' + pin_no_db,
		type: "POST",
		success: function(data) {
			$("#tab_result").html(data);
		},
		error: function() {}
	});
}

$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

<?php
include "footer.php";
?>
<button type="button" id="ModalButton" style="display:none" class="btn btn-primary" data-toggle="modal" data-target="#myModal"></button>

<div class="modal" id="myModal" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="bg-primary modal-header">
        <h4 class="modal-title"  style="font-size:15px">Set/Reset Pin : <span style="color:" id="veh_no_form"></span></h4>
      </div>
	<div class="modal-body">
	  <div id="tab_result"></div>
		 <div class="row">
			
			<div class="form-group col-md-12" id="doc_no_div">
				<label>Enter PIN : <font color="red">*</font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^0-9-/]/,'')" maxlength="4" class="form-control" 
				id="pin_no_db" required />
			</div>
			
		</div>
			<input type="hidden" id="veh_no_db" name="veh_no">	
			<input type="hidden" id="kit_id_db" name="kit_id">	
			<input type="hidden" id="company_db" name="company">	
	</div>

      <div class="modal-footer">
        <button type="button" onclick="UpdatePinForm()" id="update_button" class="btn btn-sm btn-primary">Update PIN</button>
        <button type="button" onclick="$('#tab_result').html('')" class="btn btn-sm btn-danger" data-dismiss="modal">Close</button>
      </div>
	</div>
  </div>
</div>	