<?php
session_start();

include($_SERVER['DOCUMENT_ROOT']."/_connect.php"); // for localhost

date_default_timezone_set('Asia/Kolkata');
$timestamp=date("Y-m-d H:i:s");

$page_name=$_SERVER['REQUEST_URI'];
$page_url=$_SERVER['REQUEST_URI'];

if(!isset($_SESSION['happay_admin']))
{
	echo "<script>
		window.location.href='./logout.php';
	</script>";
	exit();
}

$conn = mysqli_connect($host,$username,$password,$db_name);
// $conn = mysqli_connect("localhost","root","","dairy");

if(!$conn)
{
	echo "<b><br /><br /><h3><center><font color='red'>DATABASE CONNECTION ERROR</font></center></b></h3>";
	exit();
}

function HappayAPI($url,$JsonString,$header_token){

if($header_token=='RRPL'){
	$token="lmfIAJzy1RZPsSZ50hwWcLpEa";
}else if($header_token=='RAMAN_ROADWAYS'){
	$token="icaxFOPnZGn9zxQHFslAQxEt2";
}else if($header_token=='TEST'){
	$token="SrXXJOkfAvgcmcSFRzNyhlpGI";
}else{
	return "TOKEN ERROR";
	exit();
}
$ch = curl_init('https://api-v2.happay.in/'.$url.'');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_TIMEOUT, 900);
curl_setopt($ch, CURLINFO_HEADER_OUT, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $JsonString);
 
// Set HTTP Header for POST request 
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	'Authorization: Bearer '.$token.'',
    'Content-Type: application/json',
    'Content-Length: ' . strlen($JsonString))
);
 
$result = curl_exec($ch);
curl_close($ch);
return $result;
}


function SendTransSms($mobile,$msg){
	$data = '1234567890';
	$refNo = substr(str_shuffle($data),0,6);
	
	$postData = array(
		'authkey' => "242484ARRP024r65bc082c9",
		'mobiles' => $mobile,
		// 'mobiles' => 9024281599,
		'message' => urlencode($msg." किसी प्रकार की सहायता के लिए  9016962404 पर संपर्क करे।"),
		'sender' => "RAMANR",
		'unicode' => 1,
		'otp' => "9016962404"
	);
	$ch = curl_init();
	curl_setopt_array($ch, array(
		CURLOPT_URL => "https://www.smsschool.in/api/otp.php",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_POST => true,
		CURLOPT_POSTFIELDS => $postData
	));
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	$output = curl_exec($ch);
	
	if(curl_errno($ch))
    {
        return "<script>alert('Error while sending Message !');</scrit>";
	}
	curl_close($ch);
}
?>