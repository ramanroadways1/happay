<?php
require_once("../_connect.php");

$get_first= Qry($conn,"SELECT tno FROM dairy.get_balance1");

if(!$get_first){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "Error";
	exit();
}

while($row = fetchArray($get_first))
{
	$get_bal= Qry($conn,"SELECT id,balance FROM dairy.happay_webhook_live WHERE date<'2021-07-19' AND card_no='$row[tno]' ORDER BY id DESC LIMIT 1");

	if(!$get_bal){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "Error ".getMySQLError($conn);
		exit();
	}
	
	$row_bal = fetchArray($get_bal);
	
	$update_new_bal= Qry($conn,"UPDATE dairy.happay_live_balance SET balance='$row_bal[balance]' WHERE veh_no='$row[tno]'");

	if(!$update_new_bal){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "Error ".getMySQLError($conn);
		exit();
	}
	
	$get_bal2= Qry($conn,"SELECT id,credit,debit FROM dairy.happay_webhook_live WHERE id>$row_bal[id] AND card_no='$row[tno]'");

	if(!$get_bal2){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "Error ".getMySQLError($conn);
		exit();
	}
	
	while($row2 = fetchArray($get_bal2))
	{
		$get_balance_live = Qry($conn,"SELECT balance FROM dairy.happay_live_balance WHERE veh_no='$row[tno]'");

		if(!$get_balance_live){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo "Error ".getMySQLError($conn);
			exit();
		}
		
		$row_live_bal = fetchArray($get_balance_live);
	
		$balance = $row_live_bal['balance'];
		
		if($row2['credit']>0)
		{
			$new_balance = $balance+$row2['credit'];
		}
		else
		{
			$new_balance = $balance-$row2['debit'];
		}
		
		$update_bal_record = Qry($conn,"UPDATE dairy.happay_webhook_live SET balance='$new_balance' WHERE id='$row2[id]'");

		if(!$update_bal_record){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo "Error ".getMySQLError($conn);
			exit();
		}
		
		$update_new_bal= Qry($conn,"UPDATE dairy.happay_live_balance SET balance='$new_balance' WHERE veh_no='$row[tno]'");

		if(!$update_new_bal){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo "Error ".getMySQLError($conn);
			exit();
		}
	}
}
	
echo "ALL OK";	
?>