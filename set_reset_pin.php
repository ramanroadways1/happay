<?php
require_once("./connect.php");

$id=escapeString($conn,strtoupper($_REQUEST['id']));
$kit_id=escapeString($conn,strtoupper($_REQUEST['kit_id']));
$veh_no=escapeString($conn,strtoupper($_REQUEST['veh_no']));
$company=escapeString($conn,strtoupper($_REQUEST['company']));

if($kit_id=='')
{
	echo "<script>
		alert('Card not found !');	
		$('#SetPinBtn$id').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($company=='')
{
	echo "<script>
		alert('Company not found !');	
		$('#SetPinBtn$id').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

echo "<script>
		$('#veh_no_form').html('$veh_no');
		$('#veh_no_db').val('$veh_no');
		$('#kit_id_db').val('$kit_id');
		$('#company_db').val('$company');
		$('#loadicon').hide();
		document.getElementById('ModalButton').click();
	</script>";
	exit();

?>