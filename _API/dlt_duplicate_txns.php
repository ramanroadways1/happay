<?php
require "../../_connect.php";

$timestamp = date("Y-m-d H:i:s");

// $from_date = date("Y-m-d");
$from_date = "2020-09-10";
$to_date = date("Y-m-d");

$sql = Qry($conn,"SELECT id,card_no,credit,debit,balance FROM dairy.happay_webhook_live WHERE date BETWEEN '$from_date' AND '$to_date' GROUP 
BY txn_id HAVING COUNT(id)>1");

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($sql)==0)
{
	echo "no duplicate txns found !!";
	exit();
}

StartCommit($conn);
$flag = true;

while($row_txns = fetchArray($sql))
{
	if($row_txns['credit']==0){
		$dr_cr = "D";
		$dr_cr_amount = $row_txns['debit'];
	}
	else{
		$dr_cr = "C";
		$dr_cr_amount = $row_txns['credit'];
	}
	
	$get_last_balance = Qry($conn,"SELECT balance FROM dairy.happay_webhook_live WHERE id<'".$row_txns['id']."' AND card_no='$row_txns[card_no]' 
	ORDER BY id DESC LIMIT 1");
	
	if(!$get_last_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	}
	
	if(numRows($get_last_balance)==0)
	{
		$last_balance = 0;
	}
	else
	{
		$row_balance = fetchArray($get_last_balance);
		$last_balance = $row_balance['balance'];
	}
	
	$dlt_this_txn = Qry($conn,"DELETE FROM dairy.happay_webhook_live WHERE id='$row_txns[id]'");
	
	if(!$get_last_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	}
		
	if($dr_cr=='C')
	{
		$update_new_balance = Qry($conn,"UPDATE dairy.happay_webhook_live SET balance=balance-'$dr_cr_amount' WHERE id>'$row_txns[id]' 
		AND card_no='$row_txns[card_no]'");
	
		if(!$update_new_balance){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
		}
		
		$update_main_balance = Qry($conn,"UPDATE dairy.happay_live_balance SET balance=balance-'$dr_cr_amount' WHERE veh_no='$row_txns[card_no]'");
	
		if(!$update_main_balance){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
		}
	}
	else
	{
		$update_new_balance = Qry($conn,"UPDATE dairy.happay_webhook_live SET balance=balance+'$dr_cr_amount' WHERE id>'$row_txns[id]' 
		AND card_no='$row_txns[card_no]'");
	
		if(!$update_new_balance){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
		}
		
		$update_main_balance = Qry($conn,"UPDATE dairy.happay_live_balance SET balance=balance-'$dr_cr_amount' WHERE veh_no='$row_txns[card_no]'");
	
		if(!$update_main_balance){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
		}
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "Txns update successs";
	exit();
}
else
{
	echo "Error..."; 
	MySQLRollBack($conn);
	closeConnection($conn);
	exit();
}
?>