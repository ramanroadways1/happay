<?php
require "../../_connect.php";
ob_start();

date_default_timezone_set('Asia/Kolkata');
$timestamp = date("Y-m-d H:i:s");

define('ACTION_HAPPAY_VERIFIED','HAPPAY_VERIFIED');
define('JSON_STATUS','status');
define('STATUS_OK','success');
define('STATUS_ERROR','error');
define('JSON_MSG','message');
// define('JSON_ERROR_MSG','errormsg');

$outputArr = array();

if(!empty($_SERVER['HTTP_CLIENT_IP']))   
{
   $ip_address = $_SERVER['HTTP_CLIENT_IP'];
}
else if(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))  
{
   $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
}
else
{
   $ip_address = $_SERVER['REMOTE_ADDR'];
}

$ip_array = array("111.93.129.174","106.51.226.49","52.76.191.122","13.235.192.245","15.206.76.169","122.182.250.253","159.89.161.219","157.32.167.9","122.179.52.134");

if(!in_array($ip_address, $ip_array)) 
{ 
	$outputArr[JSON_STATUS] = STATUS_ERROR;
    $outputArr[JSON_MSG]  = "IP Address $ip_address not whitelisted.";

    echo json_encode($outputArr);
    exit;
}

$data = json_decode(file_get_contents('php://input'), true);

$data_action='HAPPAY_VERIFIED';
$data_encoded = $data;

// echo $data_encoded."<br>";
// echo $data_encoded['action']."<br>";

if(empty($data)) {
    $outputArr[JSON_STATUS]     = STATUS_ERROR;
    $outputArr[JSON_MSG]  = "Invalid request";
	echo json_encode($outputArr);
    exit;
}

if(empty($data_action)) {
    $outputArr[JSON_STATUS]     = STATUS_ERROR;
    $outputArr[JSON_MSG]  = "Invalid request. Missing authorization Parameter.";
	echo json_encode($outputArr);
    exit;
}

// if(empty($data_encoded['data_fields'])) {
    // $outputArr[JSON_STATUS]     = STATUS_ERROR;
    // $outputArr[JSON_MSG]  = "Invalid request. Missing data fields.";
	// echo json_encode($outputArr);
    // exit;
// }

switch($data_action) {
    case ACTION_HAPPAY_VERIFIED: {
        $outputArr = InsertTransactions($conn,$data_encoded);
    }
    break;

    default: {
        $outputArr[JSON_STATUS]     = STATUS_ERROR;
        $outputArr[JSON_MSG]  = "Invalid request.";
    }
}

echo json_encode($outputArr);
exit;
 
function InsertTransactions($conn,$data_encoded1)
{
	date_default_timezone_set('Asia/Kolkata');
	
	$timestamp = date("Y-m-d H:i:s");
	$outputArr = array();
	
	$today_date =  date('Y-m-d');
	$prev_date = date('Y-m-d', strtotime($today_date .' -3 day'));
	$prev_date2 = date('Y-m-d', strtotime($today_date .' -3 day'));
	
	// $delete_old_txns = Qry($conn,"DELETE FROM dairy.happay_webhook WHERE date(timestamp)<='$prev_date2'");
	
	// if(!$delete_old_txns){
		// $flag = false;
		// errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		// exit();
	// }
	
	$json_encoded = json_encode($data_encoded1);

	$result = json_decode($json_encoded, true);
	$first_name = $result['user_data']['first_name'];
	$last_name = $result['user_data']['last_name'];
	$user_id = $result['user_data']['user_id'];
	$email_id = $result['user_data']['email_id'];
	$mobile_no = $result['user_data']['mobile_no'];
	$vehicle_no = $result['user_data']['user_extra_fields']['Vehicle Number'];

	foreach($result['transactions'] as $result1)
	{
		$kit_id = $result1['transaction_extra_fields']['Card Kit ID'];
		$trans_type = $result1['transaction_type'];
		$date_gmt = $result1['transaction_date'];
		$date = date('Y-m-d H:i:s',strtotime('+5 hour +30 minutes',strtotime($date_gmt)));
		$date_only = date('Y-m-d',strtotime('+5 hour +30 minutes',strtotime($date_gmt)));
		$merchant = $result1['payee_merchant'];
		$transaction_id = $result1['transaction_id'];
		$currency_amount = $result1['currency_amount'];
		$currency_alpha_code = $result1['currency_alpha_code'];
		$currency_name = $result1['currency_name'];
		$amount = $result1['Amount'];
		$transaction_description = $result1['transaction_description'];
		$transaction_wallet = $result1['transaction_wallet'];
		$city_name = $result1['city_name'];
		$approved_amount = $result1['approved_amount'];
	}
	
	$chk_trans = Qry($conn,"SELECT id FROM dairy.happay_webhook_live WHERE date(timestamp) BETWEEN '$prev_date' AND '$today_date' 
	AND txn_id='$transaction_id'");
	
	if(!$chk_trans){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
		
	if(numRows($chk_trans)==0)
	{
		/*			
		if($trans_type=='Wallet Load Credit')
		{
			$get_trans_id = Qry($conn,"SELECT id,trans_id FROM dairy.happay_card_transactions WHERE trans_id_updated=0 AND 
			card_using='$vehicle_no' AND credit='$currency_amount' ORDER BY id ASC LIMIT 1");
			
			if(!$get_trans_id){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
			
			$row_trans_id = fetchArray($get_trans_id);
			
			$trans_id_erp = $row_trans_id['trans_id'];
			$trans_id_erp_id = $row_trans_id['id'];
			
			$get_balance = Qry($conn,"SELECT balance FROM dairy.happay_live_balance WHERE veh_no='$vehicle_no'");
			
			if(!$get_balance){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
			
			$row_bal = fetchArray($get_balance);
			$balance_now = $row_bal['balance']+$currency_amount;
			
			$insert = Qry($conn,"INSERT INTO dairy.happay_webhook_live(company,card_no,card_kit_id,first_name,last_name,user_id,email_id,mobile_no,
			merchant,txn_id,trans_id,trans_date,trans_type,credit,debit,balance,date,narration,timestamp) VALUES ('RRPL','$vehicle_no','$kit_id',
			'$first_name','$last_name','$user_id','$email_id','$mobile_no','$merchant','$transaction_id','$trans_id_erp','$date','$trans_type',
			'$currency_amount','','$balance_now','$date_only','','$timestamp')");
			
			if(!$insert){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
			
			$update_balance = Qry($conn,"UPDATE dairy.happay_live_balance SET balance=balance+'$currency_amount' WHERE 
			veh_no='$vehicle_no'");
			
			if(!$update_balance){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
			
			$update_trans_id_set = Qry($conn,"UPDATE dairy.happay_card_transactions SET trans_id_updated='1' 
			WHERE id='$trans_id_erp_id'");
			
			if(!$update_trans_id_set){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
		}
		
		if($trans_type=='Wallet Withdraw Debit')
		{
			$get_trans_id = Qry($conn,"SELECT id,trans_id FROM dairy.happay_card_withdrawal WHERE trans_id_updated=0 AND 
			tno='$vehicle_no' AND trans_id LIKE 'HPWD%' ORDER BY id ASC LIMIT 1");
			
			if(!$get_trans_id){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
			
			$row_trans_id = fetchArray($get_trans_id);
			
			$trans_id_erp = $row_trans_id['trans_id'];
			$trans_id_erp_id = $row_trans_id['id'];
			
			$get_balance = Qry($conn,"SELECT balance FROM dairy.happay_live_balance WHERE veh_no='$vehicle_no'");
			
			if(!$get_balance){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
			
			$row_bal = fetchArray($get_balance);
			$balance_now = $row_bal['balance']-$currency_amount;
			
			$insert = Qry($conn,"INSERT INTO dairy.happay_webhook_live(company,card_no,card_kit_id,first_name,last_name,user_id,email_id,mobile_no,
			merchant,txn_id,trans_id,trans_date,trans_type,credit,debit,balance,date,narration,timestamp) VALUES ('RRPL','$vehicle_no','$kit_id',
			'$first_name','$last_name','$user_id','$email_id','$mobile_no','$merchant','$transaction_id','$trans_id_erp','$date','$trans_type',
			'','$currency_amount','$balance_now','$date_only','','$timestamp')");
			
			if(!$insert){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
			
			$update_balance = Qry($conn,"UPDATE dairy.happay_live_balance SET balance=balance-'$currency_amount' WHERE 
			veh_no='$vehicle_no'");
			
			if(!$update_balance){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
			
			if(!$update_balance){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
			
			$update_trans_id_set = Qry($conn,"UPDATE dairy.happay_card_withdrawal SET trans_id_updated='1' WHERE id='$trans_id_erp_id'");
			
			if(!$update_trans_id_set){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
		}
		
		if($trans_type=='ATM Expenses')
		{
			$get_balance = Qry($conn,"SELECT balance FROM dairy.happay_live_balance WHERE veh_no='$vehicle_no'");
			
			if(!$get_balance){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
			
			$row_bal = fetchArray($get_balance);
			$balance_now = $row_bal['balance']-$currency_amount;
			
			$insert = Qry($conn,"INSERT INTO dairy.happay_webhook_live(company,card_no,card_kit_id,first_name,last_name,user_id,email_id,mobile_no,
			merchant,txn_id,trans_id,trans_date,trans_type,credit,debit,balance,date,narration,timestamp) VALUES ('RRPL','$vehicle_no','$kit_id',
			'$first_name','$last_name','$user_id','$email_id','$mobile_no','$merchant','$transaction_id','','$date','$trans_type',
			'','$currency_amount','$balance_now','$date_only','','$timestamp')");
			
			if(!$insert){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
			
			$update_balance = Qry($conn,"UPDATE dairy.happay_live_balance SET balance=balance-'$currency_amount' WHERE 
			veh_no='$vehicle_no'");
			
			if(!$update_balance){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
		}
		
		if($trans_type=='Card Transaction')
		{
			$get_balance = Qry($conn,"SELECT balance FROM dairy.happay_live_balance WHERE veh_no='$vehicle_no'");
			
			if(!$get_balance){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
			
			$row_bal = fetchArray($get_balance);
			$balance_now = $row_bal['balance']-$currency_amount;
			
			$insert = Qry($conn,"INSERT INTO dairy.happay_webhook_live(company,card_no,card_kit_id,first_name,last_name,user_id,email_id,mobile_no,
			merchant,txn_id,trans_id,trans_date,trans_type,credit,debit,balance,date,narration,timestamp) VALUES ('RRPL','$vehicle_no','$kit_id',
			'$first_name','$last_name','$user_id','$email_id','$mobile_no','$merchant','$transaction_id','','$date','$trans_type',
			'','$currency_amount','$balance_now','$date_only','','$timestamp')");
			
			if(!$insert){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
			
			$update_balance = Qry($conn,"UPDATE dairy.happay_live_balance SET balance=balance-'$currency_amount' WHERE 
			veh_no='$vehicle_no'");
			
			if(!$update_balance){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
		}
		
		if($trans_type=='Fee Expenses')
		{
			$get_balance = Qry($conn,"SELECT balance FROM dairy.happay_live_balance WHERE veh_no='$vehicle_no'");
			
			if(!$get_balance){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
			
			$row_bal = fetchArray($get_balance);
			$balance_now = $row_bal['balance']-$currency_amount;
			
			$insert = Qry($conn,"INSERT INTO dairy.happay_webhook_live(company,card_no,card_kit_id,first_name,last_name,user_id,email_id,mobile_no,
			merchant,txn_id,trans_id,trans_date,trans_type,credit,debit,balance,date,narration,timestamp) VALUES ('RRPL','$vehicle_no','$kit_id',
			'$first_name','$last_name','$user_id','$email_id','$mobile_no','$merchant','$transaction_id','','$date','$trans_type',
			'','$currency_amount','$balance_now','$date_only','','$timestamp')");
			
			if(!$insert){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
			
			$update_balance = Qry($conn,"UPDATE dairy.happay_live_balance SET balance=balance-'$currency_amount' WHERE 
			veh_no='$vehicle_no'");
			
			if(!$update_balance){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
		}
		
		if($trans_type=='ATM Reversal')
		{
			$get_balance = Qry($conn,"SELECT balance FROM dairy.happay_live_balance WHERE veh_no='$vehicle_no'");
			
			if(!$get_balance){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
			
			$row_bal = fetchArray($get_balance);
			$balance_now = $row_bal['balance']+$currency_amount;
			
			$insert = Qry($conn,"INSERT INTO dairy.happay_webhook_live(company,card_no,card_kit_id,first_name,last_name,user_id,email_id,mobile_no,
			merchant,txn_id,trans_id,trans_date,trans_type,credit,debit,balance,date,narration,timestamp) VALUES ('RRPL','$vehicle_no','$kit_id',
			'$first_name','$last_name','$user_id','$email_id','$mobile_no','$merchant','$transaction_id','','$date','$trans_type',
			'$currency_amount','','$balance_now','$date_only','','$timestamp')");
			
			if(!$insert){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
			
			$update_balance = Qry($conn,"UPDATE dairy.happay_live_balance SET balance=balance+'$currency_amount' WHERE 
			veh_no='$vehicle_no'");
			
			if(!$update_balance){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
		}
		
		if($trans_type=='FEE Reversal')
		{
			$get_balance = Qry($conn,"SELECT balance FROM dairy.happay_live_balance WHERE veh_no='$vehicle_no'");
			
			if(!$get_balance){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
			
			$row_bal = fetchArray($get_balance);
			$balance_now = $row_bal['balance']+$currency_amount;
			
			$insert = Qry($conn,"INSERT INTO dairy.happay_webhook_live(company,card_no,card_kit_id,first_name,last_name,user_id,email_id,mobile_no,
			merchant,txn_id,trans_id,trans_date,trans_type,credit,debit,balance,date,narration,timestamp) VALUES ('RRPL','$vehicle_no','$kit_id',
			'$first_name','$last_name','$user_id','$email_id','$mobile_no','$merchant','$transaction_id','','$date','$trans_type',
			'$currency_amount','','$balance_now','$date_only','','$timestamp')");
			
			if(!$insert){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
			
			$update_balance = Qry($conn,"UPDATE dairy.happay_live_balance SET balance=balance+'$currency_amount' WHERE 
			veh_no='$vehicle_no'");
			
			if(!$update_balance){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			}
		}
		
		*/
		
		// else if($trans_type=='ATM Expenses' || $trans_type=='Atm Reversal' || $trans_type=='Fee Reversal' || $trans_type=='Fee Expenses')
		
		//Card Transaction // for online trans.
		 
		// if($trans_type!='Wallet Load Credit' AND $trans_type!='ATM Reversal' AND $trans_type!='FEE Reversal' AND $trans_type!='Wallet Withdraw Debit' AND $trans_type!='ATM Expenses' AND $trans_type!='Card Transaction' AND $trans_type!='Fee Expenses')
		// {
			// $insert_error_file = Qry($conn,"INSERT INTO dairy.running_scripts(file_name,desc1,timestamp) VALUES ('Happay_Auto_Txn_Error','$trans_type','$timestamp')");
		// }			
		
		// $insert = Qry($conn,"INSERT INTO dairy.happay_webhook (api_response,first_name,last_name,user_id,email_id,tno,dr_cr,mobile_no,
		// currency_alpha_code,transaction_type,currency_name,amount,transaction_date,transaction_description,transaction_wallet,city_name,
		// approved_amount,company,payee_merchant,transaction_id,currency_amount,timestamp) VALUES ('$json_encoded','$first_name','$last_name',
		// '$user_id','$email_id','$vehicle_no','','$mobile_no','$currency_alpha_code','$trans_type','$currency_name','$amount',
		// '$date','$transaction_description','$transaction_wallet','$city_name','$approved_amount','RRPL','$merchant','$transaction_id',
		// '$currency_amount','$timestamp')");
		
		// $insert2 = Qry($conn,"INSERT INTO dairy.happay_webhook_db (api_response,first_name,last_name,user_id,email_id,tno,dr_cr,mobile_no,
		// currency_alpha_code,transaction_type,currency_name,amount,transaction_date,transaction_description,transaction_wallet,city_name,
		// approved_amount,company,payee_merchant,transaction_id,currency_amount,timestamp) VALUES ('$json_encoded','$first_name','$last_name',
		// '$user_id','$email_id','$vehicle_no','','$mobile_no','$currency_alpha_code','$trans_type','$currency_name','$amount',
		// '$date','$transaction_description','$transaction_wallet','$city_name','$approved_amount','RRPL','$merchant','$transaction_id',
		// '$currency_amount','$timestamp')");
		
		if($trans_type=='Wallet Load Credit' || $trans_type=='Wallet Withdraw Debit' || $trans_type=='ATM Expenses' || $trans_type=='Card Transaction' || $trans_type=='Fee Expenses' || $trans_type=='ATM Reversal' || $trans_type=='FEE Reversal')
		{	
			if($trans_type=='Wallet Load Credit' || $trans_type=='FEE Reversal' || $trans_type=='ATM Reversal')
			{
				$cr_var = $currency_amount;
				$dr_var = "";
			}
			else
			{
				$cr_var="";
				$dr_var = $currency_amount;
			}
			
			$insert = Qry($conn,"INSERT INTO dairy.happay_webhook_live(company,card_no,card_kit_id,first_name,last_name,user_id,email_id,mobile_no,
			merchant,txn_id,trans_date,trans_type,credit,debit,date,timestamp) VALUES ('RRPL','$vehicle_no','$kit_id',
			'$first_name','$last_name','$user_id','$email_id','$mobile_no','$merchant','$transaction_id','$date','$trans_type',
			'$cr_var','$dr_var','$date_only','$timestamp')");
				
			if(!$insert)
			{ 
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
				$outputArr[JSON_STATUS] = STATUS_ERROR;
				$outputArr[JSON_MSG] = "Query Execution failed.";
			}
			else
			{
				$outputArr[JSON_STATUS] = STATUS_OK;
				$outputArr[JSON_MSG] = "Record inserted successfully.";
			}
			
			return $outputArr;
			exit();
		}	
	}
}
// echo json_encode($data);
?>