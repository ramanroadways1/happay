<?php
include_once "header.php";

$get_last_email = Qry($conn,"SELECT email2 FROM dairy.happay_users ORDER BY id DESC LIMIT 1");
if(!$get_last_email){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($get_last_email)==0){
	$lastUsrId = 1;
}
else{
	$row_last_user_id = fetchArray($get_last_email);
	$lastUsrId = ++$row_last_user_id['email2'];
}
$user_email = "raman.happay+".$lastUsrId."@gmail.com";
?>
<div class="content-wrapper">
    <section class="content-header">
       <h4>
		Create new User :
      </h4>
	  
	  <style>
		.form-control{text-transform:uppercase;}
	  </style>
	
	</section>

    <section class="content">
        <div class="row">
          <div class=""></div>
            <div class="col-md-12">
            <div class="box"> 
             <div class="box-body pad">
   
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>	   
   
<script type="text/javascript">
$(document).ready(function (e) {
$("#Form1").on('submit',(function(e) {
$("#loadicon").show();
$("#create_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_create_user.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_form").html(data);
		$("#loadicon").hide();
	},
	error: function() 
	{} });}));});
</script>			

	<form id="Form1" autocomplete="off">

         <div class="row">
			<div class="form-group col-md-12">
			 
				<div class="form-group col-md-12">
					<h5 id="result_form"></h5>
				 </div>
				 
				 <div class="form-group col-md-3">
                     <label>Title : <font color="red">*</font></label>
                     <select name="title" class="form-control" required>
						<option value="Mr">Mr</option>
						<option disabled value="Mrs">Mrs</option>
						<option disabled value="Miss">Miss</option>
					  </select>
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>Company : <font color="red">*</font></label>
                     <select name="company" class="form-control" required>
						<option value="">--SELECT--</option>
						<option value="RRPL">RRPL</option>
						<option value="RAMAN_ROADWAYS">RAMAN_ROADWAYS</option>
					  </select>
                 </div>
				 
				<div class="form-group col-md-3">
                     <label>First Name : <font color="red">* </font></label>
                     <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" class="form-control" name="f_name" required />
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>Last Name : <font color="red">* </font></label>
                     <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" class="form-control" name="l_name" required />
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>Gender : <font color="red">*</font></label>
                     <select name="gender" class="form-control" required>
						<option value="Male">Male</option>
						<option disabled value="Female">Female</option>
					  </select>
                 </div>
				 
				<div class="form-group col-md-3">
                     <label>Password : <font color="red">*</font></label>
                     <input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9-@+_.]/,'')" class="form-control" name="password" required />
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>Mobile No : <font color="red">*</font></label>
                     <input maxlength="10" oninput="this.value=this.value.replace(/[^0-9]/,'')" type="text" class="form-control" name="mobile" required />
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>Date of birth : <font color="red">*</font></label>
                     <input type="date" class="form-control" name="dob" required max="<?php echo date("Y-m-d"); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
                 </div>
				 
				  <div class="form-group col-md-3">
                     <label>Vehicle Number : <font color="red">*</font></label>
                     <select name="veh_no" data-live-search="true" data-live-search-style="" class="form-control selectpicker">
						<option value="">--Select Vehicle--</option>
						<?php 
						$get_vehicle = Qry($conn,"SELECT tno FROM dairy.own_truck WHERE tno not in(SELECT veh_no FROM 
						dairy.happay_card_inventory)");
						
						if(!$get_vehicle){
							echo getMySQLError($conn);
							errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
							exit();
						}

						if(numRows($get_vehicle)>0)
						{
							while($row_veh = fetchArray($get_vehicle))
							{
								echo "<option value='$row_veh[tno]'>$row_veh[tno]</option>";
							}
						}
						?>
					</select>
                 </div>
				 
				  <div class="form-group col-md-3">
                     <label>Email Id : <font color="red">*</font></label>
                     <input style="text-transform:lowercase" readonly value="<?php echo $user_email; ?>" oninput="this.value=this.value.replace(/[^a-zA-Z0-9-@+_.]/,'')" type="email" class="form-control" id="email" name="email" required />
                 </div>
				 
				 <!--
				<div class="form-group col-md-3">
					<label>KYC Document Type : <font color="red">*</font></label>
					<select name="doc_type" id="doc_type" class="form-control" required>
						<option value="">--Select Type--</option>
						<option value="pan_card">Pan Card</option>
						<option value="driving_license">Driving License</option>
						<option value="passport">Passport</option>
						<option value="voter_id">Voter Id</option>
					</select>
				</div>
				
				<div class="form-group col-md-3">
                     <label>Document No : <font color="red">*</font></label>
                     <input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" class="form-control" id="doc_no" name="doc_no" required />
                 </div>
				 -->
				 <input type="hidden" value="<?php echo $lastUsrId; ?>" name="email_unq_id">
				 
				<div class="form-group col-md-12">   
					<button type="submit" id="create_button" class="btn btn-success">Create User</button>
                </div> 
		</div>
      </div>
      </form>
	</div>
</div>
			</div> 
          </div>
        </div>       
    </section>
		
<?php
include "footer.php";
?>