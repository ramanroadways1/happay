<?php
require_once("./connect.php");

$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

$tno = escapeString($conn,$_POST['tno']);
$otp = escapeString($conn,$_POST['otp']);
$company = escapeString($conn,$_POST['company']);

// $url="https://www.smsschool.in/api/verifyRequestOTP.php";
// $postData = array(
    // 'authkey' => "242484ARRP024r65bc082c9",
    // 'mobile' => 8600997434,
    // 'otp' => $otp
// );

// $ch = curl_init();
// curl_setopt_array($ch, array(
// CURLOPT_URL => $url,
// CURLOPT_RETURNTRANSFER => true,
// CURLOPT_POST => true,
// CURLOPT_POSTFIELDS => $postData
// ));

// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

// $output = curl_exec($ch);

// if(curl_errno($ch))
// {
	// echo "<script>
		// alert('API Failure !');
		// $('#loadicon').hide();
		// $('#button1').attr('disabled',true);
	// </script>";
	// exit();	
// }

// $result = json_decode($output, true);
// curl_close($ch);

// if($result['type']=='error')
// {
	// echo "<script>
			// alert('Error : $result[message].');
			// $('#loadicon').hide();
			// $('#button1').attr('disabled',false);
		// </script>";
	// exit();	
// }
// else
// {
	// if($result['message']!='otp_verified')
	// {
		// echo "<script>
			// alert('Error : $result[message].');
			// $('#loadicon').hide();
			// $('#button1').attr('disabled',false);
		// </script>";
		// exit();	
	// }
// }

VerifyOTP($otp,"button1");

// if($otp!=$_SESSION['session_otp'])
	// {
		// echo "<script>
				// alert('Error : Invalid OTP entered.');
				// $('#button1').attr('disabled',false);
				// $('#loadicon').hide();
		// </script>";
		// exit();	
	// }
	
if($company=='')
{
	echo "<script>
		alert('Company not found !');
		$('#loadicon').hide();
		$('#button1').attr('disabled',true);
	</script>";
	exit();	
}

/// Check Current Bal of Card

	$ReqID_Verify = date("Y-m-d_H:i:s");
	$verify_Payload = array(
	"requestId"=>$ReqID_Verify,
	"userId"=>$tno
	);
	$verify_Payload = json_encode($verify_Payload);
	$result_Api_Verify = HappayAPI("auth/v1/cards/get_wallet_balance/",$verify_Payload,$company);
	$result_decoded_Verify = json_decode($result_Api_Verify, true);

	if(strpos($result_Api_Verify,"Unauthorized") !== false)
	{ 
		closeConnection($conn);
		echo "<font color='red'><b>Error :</b> ".$result."</font>
		<script>
			$('#loadicon').hide();
		</script>";
		exit();
	} 

	if(@$result_decoded_Verify['error'])
	{
		$error_msg_Verify = $result_decoded_Verify['error']['message'];
		$insertApiLog_Verify = Qry($conn,"INSERT INTO dairy.happay_api_log(req_id,tno,result,api_name,api_response,status,timestamp) 
		VALUES ('$ReqID_Verify','$tno','$error_msg_Verify','GET_WALLET_BALANCE','$result_Api_Verify','0','$timestamp')");
	
		if(!$error_msg_Verify){
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		}
		
		echo "<script>
				alert('Error : API Error : $error_msg_Verify.');
				$('#loadicon').hide();
		</script>";
		exit();
	}
	else
	{
		foreach($result_decoded_Verify['res_data'] as $data_Bal)
		{
			$verify_Bal = $data_Bal['current_balance'];			
		}
			
		if($verify_Bal>0)
		{
			echo "<script>
				alert('Error : Withdraw card balance first. Available balance is: $verify_Bal.');
				$('#loadicon').hide();
			</script>";
			exit();
		}
	}
	
// Check card balance ends

StartCommit($conn);
$flag = true;

$update1=Qry($conn,"UPDATE dairy.happay_card SET card_assigned='0',status='1' WHERE tno='$tno'");

if(!$update1){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}		

$update2 = Qry($conn,"UPDATE dairy.own_truck set happay_active='0' where tno='$tno'");

if(!$update2){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Card deactivated Successfully !');
		window.location.href='./move_to_cash.php';
		$('#loadicon').hide();
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	errorLog("COMMIT NOT COMPLETED. Timestamp : $timestamp. Truck No: $tno.",$conn,$page_name,__LINE__);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./");
	exit();
}		
?>