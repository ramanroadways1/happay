<?php
require_once 'connect.php';

$tno = escapeString($conn,strtoupper($_POST['tno']));

if($tno!="")
{	
	$qry_tno=" AND t.card_no='$tno'";
}
else
{
	$qry_tno="";
}

$from_date = escapeString($conn,strtoupper($_POST['from_date']));
$to_date = escapeString($conn,strtoupper($_POST['to_date']));

$sql = Qry($conn,"SELECT t.card_no,o.comp as company,t.card_using,t.trans_id,t.trans_type,t.credit,t.branch,t.date,t.superv_entry,t.done,t.done_time,
t.timestamp,d.name as driver_name
FROM dairy.happay_card_transactions AS t 
LEFT OUTER JOIN dairy.driver AS d ON d.code=t.driver_code 
LEFT OUTER JOIN dairy.own_truck AS o ON o.tno=t.card_using 
WHERE t.date BETWEEN '$from_date' AND '$to_date'$qry_tno ORDER BY t.id ASC");

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

?>
<a target="_blank" href="_download_excel_load_trans.php?tno=<?php echo $tno; ?>&from_date=<?php echo $from_date; ?>&to_date=<?php echo $to_date; ?>"><button type="button" class="pull-right btn btn-danger">Download Excel</button></a>
<br />
<br />
	<table id="example" class="table table-bordered table-striped" style="font-size:12.5px;">
        <thead>
		<tr>
			<th>#</th>
			<th>Vehicle_No</th>
			<th>Company</th>
			<th>Driver_Name</th>
			<th>Trans_id</th>
			<th>Date</th>
			<th>Amount</th>
			<th>Branch</th>
			<th>Entry_By</th>
			<th>Recharge_At</th>
		</tr>
		</thead>
       <tbody id=""> 
		
<?php
if(numRows($sql)==0)
{
	echo "<tr>
		<td colspan='12'>NO RESULT FOUND !!</td>
	</tr>";  
}
else
{
	$sn=1;
	while($row = fetchArray($sql))
	{
		if($row['superv_entry']=="1")
		{
			$entry_by = "<font color='red'>Supervisor</font>";
		}
		else
		{
			$entry_by = "<font color='green'>Branch</font>";
		}
		
		if($row['done_time']==0)
		{
			$done_time = "<font color='red'>Pending</font>";
		}
		else
		{
			$done_time = $row['done_time'];
		}
				echo "
					<td>$sn</td>
					<td>$row[card_no]</td>
					<td>$row[company]</td>
					<td>$row[driver_name]</td>
					<td>$row[trans_id]</td>
					<td>".date("d-m-y",strtotime($row["date"]))."</td>
					<td>$row[credit]</td>
					<td>$row[branch]</td>
					<td>$entry_by</td>
					<td>$done_time</td>
				</tr>
				";
		$sn++;		
     }
}
?>
	</tbody>
		</table>
<script> 
$("#loadicon").hide();
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script> 