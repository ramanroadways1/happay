<?php
include "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
      Update KYC :
      
	  &nbsp; <button type="button" onclick="LoadTrans();" class="btn-sm btn btn-primary">Reload Kyc Status</button>
	  <style>
	  label{font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div id="func_result"></div>

<script>
function LoadTrans()
{
$("#loadicon").show();
		jQuery.ajax({
			url: "_API/refresh_kyc_status.php",
			data: 'var=' + 'OK',
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
		},
		error: function() {}
	});
}
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#KycForm").on('submit',(function(e) {
$("#loadicon").show();
$("#submit_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_kyc.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#tab_result").html(data);
	},
	error: function() 
	{} });}));});
</script>	
	
<div class="row">
 
 <div class="form-group col-md-12">
    
	<div class="form-group col-md-12 table-responsive">
	 <br />
        <table id="example" class="table table-bordered table-striped" style="font-size:12px;">
		 <thead>	
          <tr>
				<th>#id</th>
				<th>Name</th>
				<th>Company</th>
				<th>Veh_No</th>
				<th>Mobile</th>
				<th>Document<br>Type</th>
				<th>Document<br>No.</th>
				<th>DOB</th>
				<th>Kyc Status</th>
				<th>KycStatus Happay</th>
				<th>Reason</th>
				<th>#</th>
			</tr>
          </thead>	 
		 <tbody> 		 
            <?php
              $sql = Qry($conn,"SELECT id,f_name,l_name,veh_no,company,kyc_status,kyc_status_happay,kyc_decline_reason,
			  doc_type,document_no,mobile,dob 
			  FROM dairy.happay_users WHERE kyc_status_happay!='Approved'");
              
			  if(!$sql){
				  echo getMySQLError($conn);
				  errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
				  exit();
			  }
			  
			  if(numRows($sql)==0)
			  {
				echo "<tr>
						<td colspan='12'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			  }
			 else
			 {
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
				 
				if($row['document_no']==''){
					$kyc_status="<b><font color='red'>Document No. Pending</font></b>";
				}else{
					if($row['kyc_status']==0){	
					 $kyc_status="<b><font color='red'>Document Pending</font></b>";
				 }else if($row['kyc_status']==1){
					 $kyc_status="<b><font color='blue'>Addr. Proof<br>Pending</font></b>";
				 }else if($row['kyc_status']==2){
					 $kyc_status="<b><font color='blue'>Photo<br>Pending</font></b>";
				 }else{
					$kyc_status="<b><font color='green'>KYC Done</font></b>";
				 }
				}	
				
				echo 
                "<tr>
					<td>$sn</td>
					<td>$row[f_name] $row[l_name]</td>
					<td>$row[company]</td>
					<td>$row[veh_no]</td>
					<td>$row[mobile]</td>
					<td id='doc_type_td$row[id]'>$row[doc_type]</td>
					<td id='doc_no_td$row[id]'>$row[document_no]</td>
					<td>";
					if($row['dob']==0){ echo ""; }else { echo date("d/m/y",strtotime($row['dob'])); }
					echo"</td>	
					<td id='kyc_status_td$row[id]'>$kyc_status</td>
					<td style='color:red'>$row[kyc_status_happay]</td>
					<td style='color:red'>$row[kyc_decline_reason]</td>
					<td>
						<input type='hidden' id='doc_no_box$row[id]' value='$row[document_no]'>
						<input type='hidden' id='kyc_status_box$row[id]' value='$row[kyc_status]'>
						<input type='hidden' id='customer_name$row[id]' value='$row[f_name] $row[l_name]'>
						<button type='button' id='kyc_button$row[id]' onclick=Kyc('$row[id]','$row[mobile]','$row[company]','$row[veh_no]') 
						class='btn btn-success btn-sm'>Do KYC</button>
					</td>
				</tr>
				";
				$sn++;		
              }
			}
            ?>
		</tbody> 			
        </table>
      </div>
    </div>
  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>

<button type="button" id="ModalButton" style="display:none" class="btn btn-primary" data-toggle="modal" data-target="#myModal"></button>

<div class="modal" id="myModal" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="bg-primary modal-header">
        <h4 class="modal-title">KYC : <span style="color:" id="customer_name"></span></h4>
      </div>
	<form id="KycForm" autocomplete="off">
      <div class="modal-body">
	  <div id="tab_result"></div>
		 <div class="row">
			
			<div class="form-group col-md-12" id="doc_type_div">
				<label>Document Type : <font color="red">*</font></label>
				<select name="doc_type" id="doc_type" class="form-control" required>
					<option value="">--Select Type--</option>
					<option value="pan_card">Pan Card</option>
					<option value="driving_license">Driving License</option>
					<option value="passport">Passport</option>
					<option value="voter_id">Voter Id</option>
				</select>
			</div>
			
			<div class="form-group col-md-12" id="doc_no_div">
				<label>Document Number : <font color="red">*</font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9-/]/,'')" class="form-control" id="doc_no" name="doc_no" required />
			</div>
			
			<div class="form-group col-md-6" id="poi_div" style="display:none">
				<label>Identity Proof : <font color="red">*</font></label>
				<input type="file" accept=".png, .jpg, .jpeg" class="form-control" id="poi" name="POI" required />
			</div>
			
			<div class="form-group col-md-6" id="poa_div" style="display:none">
				<label>Address Proof : <font color="red">*</font></label>
				<input type="file" accept=".png, .jpg, .jpeg" class="form-control" id="poa" name="POA" required />
			</div>
			
			<div class="form-group col-md-6" id="photo_div" style="display:none">
				<label>User Photo : <font color="red">*</font></label>
				<input type="file" accept=".png, .jpg, .jpeg" class="form-control" id="photo" name="PHOTO" required />
			</div>
			
		</div>
			<input type="hidden" id="id_box" name="id">	
			<input type="hidden" id="userid_box" name="userid">	 <!-- mobile no -->
			<input type="hidden" id="kyc_status" name="kyc_status">	
			<input type="hidden" id="document_number" name="document_number">	
			<input type="hidden" id="company" name="company">	
			<input type="hidden" id="veh_no_db" name="veh_no">	
      </div>

      <div class="modal-footer">
        <button type="submit" id="submit_button" class="btn btn-primary">Update KYC</button>
        <button type="button" onclick="MyFunc1($('#id_box').val())" id="close_modal_button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="button" id="modal_hide" data-dismiss="modal" style="display:none">das</button>
      </div>
	</form>
    </div>
  </div>
</div>	

<script>	
function Kyc(id,userid,company,veh_no)
{
	$('#kyc_button'+id).attr('disabled',true);
	var customer_name = $('#customer_name'+id).val();
	var doc_no_ip = $('#doc_no_box'+id).val();
	var kyc_status = $('#kyc_status_box'+id).val();
	
	$('#customer_name').html(customer_name+" ("+veh_no+")");
	$('#id_box').val(id);
	$('#userid_box').val(userid);
	$('#veh_no_db').val(veh_no);
	$('#kyc_status').val(kyc_status);
	$('#company').val(company);
	$('#document_number').val(doc_no_ip);
	
	if(doc_no_ip=='')
	{
		$('#doc_type_div').show();
		$('#doc_no_div').show();
		
		$('#doc_type').attr('required',true);
		$('#doc_no').attr('required',true);
		
			$('#poi_div').hide();
			$('#poa_div').hide();
			$('#photo_div').hide();
			$('#poi').attr('required',false);
			$('#poa').attr('required',false);
			$('#photo').attr('required',false);
			
		document.getElementById('ModalButton').click();
		$('#loadicon').hide();
	}
	else
	{
		$('#doc_type_div').hide();
		$('#doc_no_div').hide();
		
		$('#doc_type').attr('required',false);
		$('#doc_no').attr('required',false);
		
		if(kyc_status==0)
		{
			$('#poi_div').show();
			$('#poa_div').hide();
			$('#photo_div').hide();
			$('#poi').attr('required',true);
			$('#poa').attr('required',false);
			$('#photo').attr('required',false);
			document.getElementById('ModalButton').click();
			$('#loadicon').hide();
		}
		else if(kyc_status==1)
		{
			$('#poi_div').hide();
			$('#poa_div').show();
			$('#photo_div').hide();
			$('#poi').attr('required',false);
			$('#poa').attr('required',true);
			$('#photo').attr('required',false);
			document.getElementById('ModalButton').click();
			$('#loadicon').hide();
		}
		else if(kyc_status==2)
		{
			$('#poi_div').hide();
			$('#poa_div').hide();
			$('#photo_div').show();
			$('#poi').attr('required',false);
			$('#poa').attr('required',false);
			$('#photo').attr('required',true);
			document.getElementById('ModalButton').click();
			$('#loadicon').hide();
		}
		else
		{
			alert('Kyc Already Updated.');
			window.location.href='./update_kyc.php';
			$('#poi_div').hide();
			$('#poa_div').hide();
			$('#photo_div').hide();
			$('#poi').attr('required',true);
			$('#poa').attr('required',true);
			$('#photo').attr('required',true);
		}
	}
}

function MyFunc1(id){
	$('#kyc_button'+id).attr('disabled',false);
}


$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

<?php
include "footer.php";
?>