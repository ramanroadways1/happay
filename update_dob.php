<?php
require_once("connect.php");

$requestId=date("Y:m:d_H:i:s");
$timestamp = date("Y:m:d H:i:s");

$get_users = Qry($conn,"SELECT id,company,veh_no FROM dairy.happay_users");
if(!$get_users){
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	echo "<script>
		alert('Error while processing request !');
	</script>";
	exit();
}

if(numRows($get_users)==0)
{
	
	echo "<script>
		alert('Error : no user found.');
	</script>";
	exit();
}

$updated_records = 0;

while($row_users = fetchArray($get_users))
{

	$ReqID_Verify = date("Y-m-d_H:i:s");
	$verify_Payload = array(
	"requestId"=>$ReqID_Verify,
	"userId"=>$row_users['veh_no']
	);
	
	$verify_Payload = json_encode($verify_Payload);
	$result_Api_Verify = HappayAPI("auth/v1/cards/get_user_info/",$verify_Payload,$row_users['company']);
	$result_decoded_Verify = json_decode($result_Api_Verify, true);

	if(strpos($result_Api_Verify,"Unauthorized") !== false)
	{ 
		closeConnection($conn);
		echo "<font color='red'><b>Error :</b> ".$result."</font>";
		exit();
	} 
	
	if(@$result_decoded_Verify['error'])
	{
		$error_msg_Verify = $result_decoded_Verify['error']['message'];
		
		if(!$insertApiLog_Verify){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo "<script>
				alert('Error : API Error : $error_msg_Verify.');
			</script>";
			exit();
		}
	}
	else
	{
		$dob = $result_decoded_Verify['res_data']['dateOfBirth'];	

		$update_dob = Qry($conn,"UPDATE dairy.happay_users SET dob='$dob' WHERE id='$row_users[id]'");
		
		if(AffectedRows($conn)!=0)
		{
			$updated_records = $updated_records+1;
		}
		// updated_records
	}
}
	echo "$updated_records Records updated";
	closeConnection($conn);
	exit();

?>