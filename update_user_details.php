<?php
include "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
      Update User details :
      <style>
	  label{font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div id="func_result"></div>

<script>
function LoadTrans()
{
$("#loadicon").show();
		jQuery.ajax({
			url: "_API/refresh_kyc_status.php",
			data: 'var=' + 'OK',
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
		},
		error: function() {}
	});
}
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#UserForm").on('submit',(function(e) {
$("#loadicon").show();
$("#submit_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_update_user_mobile.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#tab_result").html(data);
	},
	error: function() 
	{} });}));});
</script>	
	
<div class="row">
 
 <div class="form-group col-md-12">
    
	<div class="form-group col-md-12 table-responsive">
	 <br />
        <table id="example" class="table table-bordered table-striped" style="font-size:12px;">
		 <thead>	
          <tr>
				<th>#id</th>
				<th>Name</th>
				<th>Company</th>
				<th>Veh_No</th>
				<th>Mobile</th>
			</tr>
          </thead>	 
		 <tbody> 		 
            <?php
              $sql = Qry($conn,"SELECT id,f_name,l_name,veh_no,company,mobile FROM dairy.happay_users WHERE kyc_status_happay IN('Approved','Not-initiated')");
              
			  if(!$sql){
				  echo getMySQLError($conn);
				  errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
				  exit();
			  }
			  
			  if(numRows($sql)==0)
			  {
				echo "<tr>
						<td colspan='12'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			  }
			 else
			 {
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
				 
				echo 
                "<tr>
					<td>$sn</td>
					<td>$row[f_name] $row[l_name]</td>
					<td>$row[company]</td>
					<td>
						$row[veh_no]
						<br>
						<button type='button' id='update_button_$row[id]' onclick=UpdateUserDetails('$row[id]','$row[mobile]','$row[company]','$row[veh_no]') 
						class='btn btn-default btn-xs'><li class='fa fa-pencil-square-o'></li> Update</button>
					</td>
					<td>
						$row[mobile]
						<br>
						<button type='button' id='user_button$row[id]' onclick=UpdateUser('$row[id]','$row[mobile]','$row[company]','$row[veh_no]') 
						class='btn btn-default btn-xs'><li class='fa fa-pencil-square-o'></li> Update</button>
					</td>
					<input type='hidden' id='customer_name$row[id]' value='$row[f_name] $row[l_name]'>
					<input type='hidden' id='mobile_no$row[id]' value='$row[mobile]'>
				</tr>
				";
				$sn++;		
              }
			}
            ?>
		</tbody> 			
        </table>
      </div>
    </div>
  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>

<button type="button" id="ModalButton" style="display:none" class="btn btn-primary" data-toggle="modal" data-target="#myModal"></button>

<div class="modal" id="myModal" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="bg-primary modal-header">
        <h4 style="font-size:13px" class="modal-title">Update user : <span style="color:" id="customer_name"></span></h4>
      </div>
	<form id="UserForm" autocomplete="off">
      <div class="modal-body">
	  <div id="tab_result"></div>
		 <div class="row">
			
			<div class="form-group col-md-12" id="doc_no_div">
				<label>Mobile Number : <font color="red">*</font></label>
				<input type="text" maxlength="10" oninput="this.value=this.value.replace(/[^0-9/]/,'')" class="form-control" id="mobile_update" 
				name="mobile_update" required />
			</div>
			
		</div>
			<input type="hidden" id="id_box" name="id">	
			<input type="hidden" id="company" name="company">	
			<input type="hidden" id="veh_no_db" name="veh_no">	
			<input type="hidden" id="mobile_no_db" name="mobile_ext">	
      </div>

      <div class="modal-footer">
        <button type="submit" id="submit_button" class="btn btn-sm btn-primary">Update</button>
        <button type="button" onclick="MyFunc1($('#id_box').val())" id="close_modal_button" class="btn btn-sm btn-danger" data-dismiss="modal">Close</button>
        <button type="button" id="modal_hide" data-dismiss="modal" style="display:none">das</button>
      </div>
	</form>
    </div>
  </div>
</div>	

<script>	
function UpdateUser(id,mobile,company,veh_no)
{
	$('#user_button'+id).attr('disabled',true);
	var customer_name = $('#customer_name'+id).val();
	var mobile_no = $('#mobile_no'+id).val();
	
	$('#customer_name').html(customer_name+" ("+veh_no+")");
	$('#id_box').val(id);
	$('#veh_no_db').val(veh_no);
	$('#company').val(company);
	$('#mobile_no_db').val(mobile);
	$('#mobile_update').val(mobile);
	
	document.getElementById('ModalButton').click();
	$('#loadicon').fadeOut('slow');
}

function UpdateUserDetails(id,mobile,company,veh_no)
{
	$('#update_button_'+id).attr('disabled',true);
	var customer_name = $('#customer_name'+id).val();
	var mobile_no = $('#mobile_no'+id).val();
	
	$('#customer_name_2').html(customer_name+" ("+veh_no+")");
	$('#id_box_2').val(id);
	$('#vehicle_update_ip').val(veh_no);
	$('#company_2').val(company);
	$('#vehicle_update_db').val(veh_no);
	
	document.getElementById('ModalButtonDetails').click();
	$('#loadicon').fadeOut('slow');
}

function MyFunc1(id){
	$('#user_button'+id).attr('disabled',false);
}

function MyFunc2(id){
	$('#update_button_'+id).attr('disabled',false);
}

$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#UserForm2").on('submit',(function(e) {
$("#loadicon").show();
$("#submit_button_2").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_update_user_details.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#tab_result2").html(data);
	},
	error: function() 
	{} });}));});
</script>

<button type="button" id="ModalButtonDetails" style="display:none" class="btn btn-primary" data-toggle="modal" data-target="#myModalUpdate"></button>

<form id="UserForm2" autocomplete="off">
<div class="modal" id="myModalUpdate" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="bg-primary modal-header">
        <h4 style="font-size:13px" class="modal-title">Update user : <span style="color:" id="customer_name_2"></span></h4>
      </div>
	
      <div class="modal-body">
	  <div id="tab_result2"></div>
		 <div class="row">
			
			<div class="form-group col-md-12" id="doc_no_div">
				<label>Vehicle Number : <font color="red">*</font></label>
				<input type="text" maxlength="10" oninput="this.value=this.value.replace(/[^A-Za-z0-9/]/,'')" class="form-control" id="vehicle_update_ip" 
				name="veh_no" required />
			</div>
			
		</div>
			<input type="hidden" id="id_box_2" name="id">	
			<input type="hidden" id="company_2" name="company">	
			<input type="hidden" id="vehicle_update_db" name="vehicle_db">	
      </div>

      <div class="modal-footer">
        <button type="submit" id="submit_button_2" class="btn btn-sm btn-primary">Update</button>
        <button type="button" onclick="MyFunc2($('#id_box_2').val())" id="close_modal_button2" class="btn btn-sm btn-danger" data-dismiss="modal">Close</button>
        <button type="button" id="modal_hide2" data-dismiss="modal" style="display:none"></button>
      </div>
	</form>
    </div>
  </div>
</div>	

<?php
include "footer.php";
?>