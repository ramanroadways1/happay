<?php
date_default_timezone_set('Asia/Kolkata');

$days_prev=date('Y-m-d',strtotime("yesterday"));

include_once('../mail_lib/config.php');
include('../mail_lib/Classes/PHPExcel.php');

$from_date = mysqli_real_escape_string($conn,$_REQUEST['from_date']);
$to_date = mysqli_real_escape_string($conn,$_REQUEST['to_date']);
$tno = mysqli_real_escape_string($conn,$_REQUEST['tno']);

if($tno!="")
{
	$qry_tno=" AND card_no='$tno'";
}
else
{
	$qry_tno="";
}

$objPHPExcel = new PHPExcel();

$query = "SELECT * FROM happay_webhook_live WHERE date(trans_date) BETWEEN '$from_date' AND '$to_date'$qry_tno 
ORDER BY trans_date ASC";

$result = mysqli_query($conn,$query);
// while($row=mysqli_fetch_array($result))
// {
	$sn=1;
		
// }
// exit();

$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Vehicle_No');
$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Merchant');
$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Txn_Id');
$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Trans_id');
$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Trans_Date');
$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Trans_Type');
$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Credit');
$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Debit');
$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Balance');

$objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setBold(true);

$rowCount	=	2;

if(mysqli_num_rows($result) > 0)
{
while($row = mysqli_fetch_array($result))
{
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, mb_strtoupper($row['card_no'],'UTF-8'));
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, mb_strtoupper($row['merchant'],'UTF-8'));
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, mb_strtoupper($row['txn_id'],'UTF-8'));
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, mb_strtoupper($row['trans_id'],'UTF-8'));
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, mb_strtoupper($row['trans_date'],'UTF-8'));
	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, mb_strtoupper($row['trans_type'],'UTF-8'));
	$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, mb_strtoupper($row['credit'],'UTF-8'));
	$objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, mb_strtoupper($row['debit'],'UTF-8'));
	$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, mb_strtoupper($row['balance'],'UTF-8'));
	$rowCount++;
}

// START - Auto size columns for each worksheet
foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {

    $objPHPExcel->setActiveSheetIndex($objPHPExcel->getIndex($worksheet));

    $sheet = $objPHPExcel->getActiveSheet();
    $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
    $cellIterator->setIterateOnlyExistingCells(true);

    foreach ($cellIterator as $cell) {
        $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
    }
}
// END - Auto size columns for each worksheet

$objWriter	=	new PHPExcel_Writer_Excel2007($objPHPExcel);

	$filename = 'Happay_Transactions.xlsx';
	 
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="'. $filename);
	header('Cache-Control: max-age=0');
	$objWriter->save('php://output');
	 // $objWriter->save('./'.$filename);
}
?>