<?php
require_once("./connect.php");

$id = escapeString($conn,$_POST['id']);
$amount = escapeString($conn,$_POST['amount']);
$timestamp = date("Y-m-d H:i:s");

if($id=='' || $amount=="")
{
	echo "<script>
		alert('Error : Empty Fields !');
		$('#loadicon').hide();
		$('#BtnSubmit').attr('disabled', false);
	</script>";
	exit();
}

if($amount<=0)
{
	echo "<script>
		alert('Error : Invalid Amount !');
		$('#loadicon').hide();
		$('#BtnSubmit').attr('disabled', false);
	</script>";
	exit();
}

$chkStatus = Qry($conn,"SELECT trans_id,credit,done FROM dairy.happay_card_transactions WHERE id='$id'");
if(!$chkStatus){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($chkStatus)==0)
{
	echo "<script>
		alert('Transaction not found !');
		$('#loadicon').hide();
		$('#BtnSubmit').attr('disabled', false);
	</script>";
	exit();
}

$rowTrans = fetchArray($chkStatus);

if($rowTrans['done']!=0)
{
	echo "<script>
		alert('Recharge already done !');
		$('#loadicon').hide();
		$('#BtnSubmit').attr('disabled', false);
	</script>";
	exit();
}

if($rowTrans['credit']==$amount)
{
	echo "<script>
		alert('Nothing to update !');
		$('#loadicon').hide();
		$('#BtnSubmit').attr('disabled', false);
	</script>";
	exit();
}

$amount_diff = $amount-$rowTrans['credit'];

$trans_id = $rowTrans['trans_id'];

$GetTripId = Qry($conn,"SELECT id,trip_id,vou_no FROM dairy.cash WHERE trans_id='$trans_id'");
if(!$GetTripId){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($GetTripId)==0)
{
	echo "<script>
		alert('Happay Transaction not found !');
		$('#loadicon').hide();
		$('#BtnSubmit').attr('disabled', false);
	</script>";
	exit();
}

$rowCash = fetchArray($GetTripId);

$cash_id = $rowCash['id'];
$trip_id = $rowCash['trip_id'];
$vou_no = $rowCash['vou_no'];

$Get_trip = Qry($conn,"SELECT tno,driver_code FROM dairy.trip WHERE id='$trip_id'");
if(!$Get_trip){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($Get_trip)==0){
	echo "<script>
		alert('Running Trip not found !');
		$('#loadicon').hide();
		$('#BtnSubmit').attr('disabled', false);
	</script>";
	exit();
}

$rowTrip = fetchArray($Get_trip);

$driver_code = $rowTrip['driver_code'];
$tno = $rowTrip['tno'];

if($driver_code=="" || $driver_code==0){
	echo "<script>
		alert('Driver not found !');
		$('#loadicon').hide();
		$('#BtnSubmit').attr('disabled', false);
	</script>";
	exit();
}

$getCompany = Qry($conn,"SELECT company FROM dairy.happay_card WHERE tno='$tno'");
if(!$getCompany){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($getCompany)==0){
	echo "<script>
		alert('Card not found !');
		$('#loadicon').hide();
		$('#BtnSubmit').attr('disabled', false);
	</script>";
	exit();
}

$rowCompany = fetchArray($getCompany);
$company = $rowCompany['company'];

$driverBook = Qry($conn,"SELECT id,tno,driver_code FROM dairy.driver_book WHERE trans_id='$trans_id'");
if(!$driverBook){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($driverBook)==0){
	echo "<script>
		alert('Transaction not found in driver-book !');
		$('#loadicon').hide();
		$('#BtnSubmit').attr('disabled', false);
	</script>";
	exit();
}

$rowDriverBook = fetchArray($driverBook);

if($rowDriverBook['tno']!=$tno || $rowDriverBook['driver_code']!=$driver_code)
{
	echo "<script>
		alert('Driver or Vehicle Number not verified with Transaction !');
		$('#loadicon').hide();
		$('#BtnSubmit').attr('disabled', false);
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

// $updateTransaction = Qry($conn,"UPDATE happay_card_transactions SET credit=(credit+('$amount_diff')),balance=(balance+('$amount_diff')) 
// WHERE id='$id'");
$updateTransaction = Qry($conn,"UPDATE dairy.happay_card_transactions SET credit=(credit+('$amount_diff')) WHERE id='$id'");

if(!$updateTransaction){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

// $updateTransactionAll = Qry($conn,"UPDATE happay_card_transactions SET balance=(balance+('$amount_diff')) WHERE id>'$id' AND 
// card_no='$tno'");

// if(!$updateTransactionAll){
	// $flag = false;
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
// }

$updateHappayMainBal = Qry($conn,"UPDATE dairy.happay_main_balance SET balance=(balance-('$amount_diff')) WHERE company='$company'");

if(!$updateHappayMainBal){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$updateVouAmount = Qry($conn,"UPDATE dairy.cash SET amount=(amount+('$amount_diff')) WHERE id='$cash_id'");

if(!$updateVouAmount){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$updateVouAmount2 = Qry($conn,"UPDATE mk_tdv SET amt=(amt+('$amount_diff')),colset='',colset_d='' WHERE tdvid='$vou_no'");

if(!$updateVouAmount2){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$updateTripAmount = Qry($conn,"UPDATE dairy.trip SET cash=(cash+('$amount_diff')) WHERE id='$trip_id'");

if(!$updateTripAmount){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$updateDriverBook = Qry($conn,"UPDATE dairy.driver_book SET credit=(credit+('$amount_diff')),balance=(balance+('$amount_diff')) WHERE 
id='$rowDriverBook[id]'");

if(!$updateDriverBook){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$updateDriverBookTrans = Qry($conn,"UPDATE dairy.driver_book SET balance=(balance+('$amount_diff')) WHERE id>'$rowDriverBook[id]' AND 
tno='$tno' AND driver_code='$driver_code'");

if(!$updateDriverBookTrans){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$updateDriverBalance = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=(amount_hold+('$amount_diff')) WHERE code='$driver_code' ORDER 
by id DESC LIMIT 1");

if(!$updateDriverBalance){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$log_response = "Trip_Id: $trip_id, Vou_No: $vou_no. Amount updated : $rowTrans[credit] to $amount.";

$insertLog = Qry($conn,"INSERT INTO dairy.happay_edit_log(card_no,log_type,trans_id,response,timestamp) VALUES ('$tno','REQ_UPDATE',
'$trans_id','$log_response','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Updated Successfully.');
		window.location.href='./recharge_req.php';
		$('#loadicon').hide();
	</script>"; 
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./recharge_req.php");
	exit();
}
?>