<?php
require_once("connect.php");

$requestId=date("Y:m:d_H:i:s");
$date=date("Y-m-d");
$timestamp=date("Y-m-d H:i:s");

$id = escapeString($conn,strtoupper($_POST['id']));
$amount = escapeString($conn,strtoupper($_POST['amount']));
$tno = escapeString($conn,strtoupper($_POST['tno']));

$ChkTrip = Qry($conn,"SELECT trip_no,driver_code FROM trip WHERE tno='$tno'");
if(!$ChkTrip){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Trip not Found.","./");
	exit();
}

if(numRows($ChkTrip)==0)
{
	echo "<script>
		alert('Error : Running Trip not found. Vehicle Number : $tno.');
		$('#loadicon').hide();
	</script>"; 
	exit();
}

$rowTrip = fetchArray($ChkTrip);

if($rowTrip['driver_code']==0 || $rowTrip['driver_code']=="")
{
	echo "<script>
		alert('Error : Driver not found. Vehicle Number : $tno($rowTrip[trip_no]).');
		$('#loadicon').hide();
	</script>"; 
	exit();
}

$trip_no = $rowTrip['trip_no'];
$driver_code = $rowTrip['driver_code'];

$trans_id_Qry=GetTransIdHappayWdl("HPWD",$conn);
	
	if(!$trans_id_Qry || $trans_id_Qry=="" || $trans_id_Qry=="0")
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error While Fetching Transaction Id.","./");
		exit();
	}
	
	$trans_id = $trans_id_Qry;
	
StartCommit($conn);
$flag = true;

$insert_Trans = Qry($conn,"INSERT INTO happay_card_withdrawal (tno,driver_code,trip_no,trans_id,amount,timestamp) VALUES 
('$tno','$driver_code','$trip_no','$trans_id','$amount','$timestamp')");

if(!$insert_Trans){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$select_amount=Qry($conn,"SELECT id,tno,amount_hold FROM driver_up WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");

if(!$select_amount){	
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($select_amount)==0){
	$flag = false;
	errorLog("Error While Fetching Driver Balance. Code: $driver_code.",$conn,$page_name,__LINE__);
}

$row_amount=fetchArray($select_amount);
$hold_amt=$row_amount['amount_hold']-$amount;
$driver_id=$row_amount['id'];

if($row_amount['tno']!=$tno)
{
	$flag = false;
	errorLog("Vehicle not verified. DbVeh: $row_amount[tno]. System : $tno.",$conn,$page_name,__LINE__);
}

$insert_book = Qry($conn,"INSERT INTO driver_book (driver_code,tno,trip_no,trans_id,desct,debit,balance,date,branch,
timestamp) VALUES ('$driver_code','$tno','$trip_no','$trans_id','HAPPAY_WITHDRAWAL','$amount','$hold_amt','$date','HAPPAY_ADMIN',
'$timestamp')");

if(!$insert_book){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_hold_amount=Qry($conn,"UPDATE driver_up SET amount_hold=amount_hold-$amount WHERE id='$driver_id'");

if(!$update_hold_amount){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Wallet Withdrawn Successfully.');
		window.location.href='./withdraw_wallet2.php';
	</script>"; 
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./withdraw_wallet2.php");
	exit();
}	
	
?>