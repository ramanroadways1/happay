<?php
require_once("./connect.php");

$id = escapeString($conn,$_POST['id']);
$timestamp = date("Y-m-d H:i:s");

if($id=='')
{
	echo "<script>
		alert('Error : Empty Fields !');
		$('#loadicon').hide();
		// $('#ButtonDelete$id').attr('disabled', false);
	</script>";
	exit();
}

$chkStatus = Qry($conn,"SELECT card_using,trans_id,credit,done FROM dairy.happay_card_transactions WHERE id='$id'");
if(!$chkStatus){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($chkStatus)==0)
{
	echo "<script>
		alert('Transaction not found !');
		$('#loadicon').hide();
		// $('#ButtonDelete$id').attr('disabled', false);
	</script>";
	exit();
}

$rowTrans = fetchArray($chkStatus);

if($rowTrans['done']!=0)
{
	echo "<script>
		alert('Recharge already done !');
		$('#loadicon').hide();
		// $('#ButtonDelete$id').attr('disabled', false);
	</script>";
	exit();
}

$amount = $rowTrans['credit'];
$trans_id = $rowTrans['trans_id'];
$card_using = $rowTrans['card_using'];

$GetTripId = Qry($conn,"SELECT id,trip_id,vou_no FROM dairy.cash WHERE trans_id='$trans_id'");
if(!$GetTripId){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($GetTripId)==0)
{
	echo "<script>
		alert('Happay Transaction not found !');
		$('#loadicon').hide();
		// $('#ButtonDelete$id').attr('disabled', false);
	</script>";
	exit();
}

$rowCash = fetchArray($GetTripId);

$cash_id = $rowCash['id'];
$trip_id = $rowCash['trip_id'];
$vou_no = $rowCash['vou_no'];

$Get_trip = Qry($conn,"SELECT tno,driver_code FROM dairy.trip WHERE id='$trip_id'");
if(!$Get_trip){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($Get_trip)==0){
	echo "<script>
		alert('Running Trip not found !');
		$('#loadicon').hide();
		$('#ButtonDelete$id').attr('disabled', false);
	</script>";
	exit();
}

$rowTrip = fetchArray($Get_trip);

$driver_code = $rowTrip['driver_code'];
$tno = $rowTrip['tno'];

$getCompany = Qry($conn,"SELECT company FROM dairy.happay_card_inventory WHERE veh_no='$card_using'");
if(!$getCompany){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($getCompany)==0){
	echo "<script>
		alert('Card not found !');
		$('#loadicon').hide();
		$('#ButtonDelete$id').attr('disabled', false);
	</script>";
	exit();
}

$rowCompany = fetchArray($getCompany);
$company = $rowCompany['company'];

$driverBook = Qry($conn,"SELECT id,tno,driver_code FROM dairy.driver_book WHERE trans_id='$trans_id'");
if(!$driverBook){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($driverBook)==0){
	echo "<script>
		alert('Transaction not found in driver-book !');
		$('#loadicon').hide();
		$('#ButtonDelete$id').attr('disabled', false);
	</script>";
	exit();
}

$rowDriverBook = fetchArray($driverBook);
$driver_book_id = $rowDriverBook['id'];

if($rowDriverBook['driver_code']!=$driver_code)
{
	echo "<script>
		alert('Driver or Vehicle Number not verified with Transaction !');
		$('#loadicon').hide();
		$('#ButtonDelete$id').attr('disabled', false);
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$Delete_Transaction = Qry($conn,"DELETE FROM dairy.happay_card_transactions WHERE id='$id'");
if(!$Delete_Transaction){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0){
	$flag = false;
	errorLog("Happay Voucher not found.",$conn,$page_name,__LINE__);
}

// $updateTransactionAll = Qry($conn,"UPDATE happay_card_transactions SET balance=(balance+('$amount_diff')) WHERE id>'$id' AND 
// card_no='$tno'");

// if(!$updateTransactionAll){
	// $flag = false;
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
// }

$updateHappayMainBal = Qry($conn,"UPDATE dairy.happay_main_balance SET balance=balance+'$amount' WHERE company='$company'");
if(!$updateHappayMainBal){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0){
	$flag = false;
	errorLog("Balance not updated.",$conn,$page_name,__LINE__);
}

$delete_VouAmount = Qry($conn,"DELETE FROM dairy.cash WHERE id='$cash_id'");
if(!$delete_VouAmount){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0){
	$flag = false;
	errorLog("Happay voucher entry not found.",$conn,$page_name,__LINE__);
}

$delet_VouAmount2 = Qry($conn,"DELETE FROM mk_tdv WHERE tdvid='$vou_no'");
if(!$delet_VouAmount2){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0){
	$flag = false;
	errorLog("Truck Voucher: $vou_no not found.",$conn,$page_name,__LINE__);
}

$updateTripAmount = Qry($conn,"UPDATE dairy.trip SET cash=cash-'$amount' WHERE id='$trip_id'");
if(!$updateTripAmount){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0){
	$flag = false;
	errorLog("Trip Id: $trip_id not found.",$conn,$page_name,__LINE__);
}

$delete_DriverBook = Qry($conn,"DELETE FROM dairy.driver_book WHERE id='$driver_book_id'");
if(!$delete_DriverBook){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0){
	$flag = false;
	errorLog("Driver book entry not found.",$conn,$page_name,__LINE__);
}

$updateDriverBookTrans = Qry($conn,"UPDATE dairy.driver_book SET balance=balance-'$amount' WHERE id>$driver_book_id AND tno='$tno'");
if(!$updateDriverBookTrans){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

// if(AffectedRows($conn)==0){
	// $flag = false;
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
// }

$updateDriverBalance = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold-'$amount' WHERE code='$driver_code' ORDER by id 
DESC LIMIT 1");
if(!$updateDriverBalance){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$log_response = "Driver: $driver_code, Trans_Id: $trans_id, Trip_Id: $trip_id, Amount: $amount, Vou_No: $vou_no";

$insertLog = Qry($conn,"INSERT INTO dairy.happay_edit_log(card_no,log_type,trans_id,response,timestamp) VALUES ('$tno','REQ_DELETE',
'$trans_id','$log_response','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Request Deleted Successfully.');
		window.location.href='./recharge_req.php';
		$('#loadicon').hide();
	</script>"; 
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./recharge_req.php");
	exit();
}
?>