<?php
require_once("connect.php");

$requestId=date("Y:m:d_H:i:s");
$timestamp = date("Y:m:d H:i:s");

$id = escapeString($conn,strtoupper($_POST['id']));
$card_no = escapeString($conn,strtoupper($_POST['card_no']));
$action = escapeString($conn,($_POST['action']));
$btn_name = escapeString($conn,($_POST['btn_name']));
$company = escapeString($conn,($_POST['company']));

$GetCardNo=Qry($conn,"SELECT veh_no,card_kit_id FROM dairy.happay_card_inventory WHERE id='$id'");
if(!$GetCardNo){
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	echo "<script>
		alert('Error while processing request !');
		$('#loadicon').hide();
	</script>";
	exit();
}

if(numRows($GetCardNo)==0){
	
	echo "<script>
		alert('Error : Card not found.');
		$('#loadicon').hide();
		$('#$btn_name$id').attr('disabled',false);
	</script>";
	exit();
}
else
{
	$rowCard = fetchArray($GetCardNo);
	$card_kit_id = $rowCard['card_kit_id'];
	$veh_no = $rowCard['veh_no'];
}

if($card_no!=$veh_no)
{
	echo "<script>
		alert('Error : Card not verified.');
		$('#loadicon').hide();
		$('#$btn_name$id').attr('disabled',false);
	</script>";
	exit();
}

$getCardBal = Qry($conn,"SELECT balance FROM dairy.happay_live_balance WHERE veh_no='$card_no'");
if(!$getCardBal){
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	echo "<script>
		alert('Error while processing request !');
		$('#loadicon').hide();
	</script>";
	exit();
}

if(numRows($getCardBal)==0)
{
	echo "<script>
		alert('Error : Card Balance not found !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$row_bal = fetchArray($getCardBal);
$card_balance = $row_bal['balance'];

if($card_balance>0)
{
	echo "<script>
		alert('Warning : Please withdraw wallet amount first. Card Balance is : Rs. $card_balance');
		$('#loadicon').hide();
		$('#$btn_name$id').attr('disabled',false);
	</script>";
	exit();
}

// Get card balance from api ---

	$ReqID_Verify = date("Y-m-d_H:i:s");
	$verify_Payload = array(
	"requestId"=>$ReqID_Verify,
	"userId"=>$card_no
	);
	
	$verify_Payload = json_encode($verify_Payload);
	$result_Api_Verify = HappayAPI("auth/v1/cards/get_wallet_balance/",$verify_Payload,$company);
	$result_decoded_Verify = json_decode($result_Api_Verify, true);

	if(strpos($result_Api_Verify,"Unauthorized") !== false)
	{ 
		closeConnection($conn);
		echo "<font color='red'><b>Error :</b> ".$result."</font>
		<script>
			$('#loadicon').hide();
		</script>";
		exit();
	} 
	
	if(@$result_decoded_Verify['error'])
	{
		$error_msg_Verify = $result_decoded_Verify['error']['message'];
		
		$insertApiLog_Verify = Qry($conn,"INSERT INTO dairy.happay_api_log(req_id,tno,result,api_name,api_response,status,
		timestamp) VALUES ('$ReqID_Verify','$card_no','$error_msg_Verify','GET_WALLET_BALANCE','$result_Api_Verify','0',
		'$timestamp')");
		
		if(!$insertApiLog_Verify){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo "<script>
					alert('Error : API Error : $error_msg_Verify.');
					$('#loadicon').hide();
			</script>";
			exit();
		}
	}
	else
	{
		foreach($result_decoded_Verify['res_data'] as $data_Bal)
		{
			$verify_Bal = $data_Bal['current_balance'];			
		}			
	
		if($card_balance!=$verify_Bal)
		{
			echo "<script>
				alert('Error : Card balance not verified. Please try agian after 5 minutes.');
				$('#loadicon').hide();
			</script>";
			exit();
		}
	}
	
// Get card balance from api ends //	

if($action=="BLOCK")
{
	$set_status="Inactive";
}
else if($action=="ACTIVE")
{
	$set_status="Active";
}
else if($action=='DETACH')
{
	
}
else
{
	echo "<script>
		alert('Error : Invalid Action.');
		$('#loadicon').hide();
		$('#$btn_name$id').attr('disabled',false);
	</script>";
	exit();
}
	
if($action!='DETACH')	
{
	$log_status = "change_card_status";
		
	$data = array(
	"requestId"=>$requestId,
	"card_status"=>$set_status,
	"card_kit_id"=>$card_kit_id
	);

	$JsonString = json_encode($data);
	$result = HappayAPI("auth/v1/cards/change_card_status/",$JsonString,$company);

	$result2 = json_decode($result, true);

		if(strpos($result,"Unauthorized") !== false)
		{ 
			closeConnection($conn);
			echo "<font color='red'><b>Error :</b> ".$result."</font>
			<script>
				$('#$btn_name$id').attr('disabled',false);
				$('#loadicon').hide();
			</script>";
			exit();
		} 

	if(@$result2['error'])
	{
		$error_msg = $result2['error']['message'];
		
		echo "<font color='red'><b>Error :</b> ".$error_msg."</font><br><br>";	
		
		$insertApiLogCheck = Qry($conn,"INSERT INTO dairy.happay_api_log(req_id,tno,result,api_name,api_response,status,timestamp) VALUES 
		('$requestId','$card_no','$error_msg','change_card_status','$result','0','$timestamp')");
		
		if(!$insertApiLogCheck){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		echo "<script>
			alert('API Error : $error_msg.')
			$('#$btn_name$id').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	else
	{
		$result_res = $result2['res_str'];			
		
		$current_status = $result2['res_data']['current_status'];
		
		// echo $result."<br>".$current_status;
		
		if(empty($result_res))
		{
			$log_insert=Qry($conn,"INSERT INTO dairy.happay_api_log(req_id,tno,result,api_name,api_response,status,timestamp) VALUES 
			('$requestId','$card_no','Empty Response','change_card_status','$result','0','$timestamp')");
			
			if(!$log_insert){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		
			echo "
			$result
			<script>
				alert('Error : API Empty Response. Contact System Admin.');
				$('#$btn_name$id').attr('disabled',false);
				$('#loadicon').hide();
			</script>";
			exit();
		}
	}
}
else
{
	$log_status = "detach_card";
	$current_status='DETACH';
	
	$data = array(
	"requestId"=>$requestId,
	// "userId"=>$set_status,
	"card_kit_id"=>$card_kit_id
	);

	$JsonString = json_encode($data);
	$result = HappayAPI("auth/v1/cards/detach_card/",$JsonString,$company);

	$result2 = json_decode($result, true);

		if(strpos($result,"Unauthorized") !== false)
		{ 
			closeConnection($conn);
			echo "<font color='red'><b>Error :</b> ".$result."</font>
			<script>
				$('#$btn_name$id').attr('disabled',false);
				$('#loadicon').hide();
			</script>";
			exit();
		} 

	if(@$result2['error'])
	{
		$error_msg = $result2['error']['message'];
		
		echo "<font color='red'><b>Error :</b> ".$error_msg."</font><br><br>";	
		
		$insertApiLogCheck = Qry($conn,"INSERT INTO dairy.happay_api_log(req_id,tno,result,api_name,api_response,status,timestamp) VALUES 
		('$requestId','$card_no','$error_msg','detach_card','$result','0','$timestamp')");
		
		if(!$insertApiLogCheck){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		echo "<script>
			alert('API Error : $error_msg.')
			$('#$btn_name$id').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	else
	{
		$result_res = $result2['res_str'];			
		
		if(empty($result_res))
		{
			$log_insert=Qry($conn,"INSERT INTO dairy.happay_api_log(req_id,tno,result,api_name,api_response,status,timestamp) VALUES 
			('$requestId','$card_no','Empty Response','detach_card','$result','0','$timestamp')");
			
			if(!$log_insert){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		
			echo "
			$result
			<script>
				alert('Error : API Empty Response. Contact System Admin.');
				$('#$btn_name$id').attr('disabled',false);
				$('#loadicon').hide();
			</script>";
			exit();
		}
	}
	
	
	$requestId2 = mt_rand().$requestId;

	$data_kit_update = array(
	"requestId"=>$requestId2,
	"userId"=>$card_no,
	"metaFields"=>array(
	// "Vehicle Number"=>$veh_no,
	"Card Kit ID"=>"",
	),
	);

	$JsonString_Kit_Update = json_encode($data_kit_update);

	$result_kit_update = HappayAPI("auth/v1/updateuser/",$JsonString_Kit_Update,$company);
		
	$result_update = json_decode($result_kit_update, true);

		if(strpos($result_kit_update,"Unauthorized") !== false)
		{ 
			closeConnection($conn);
			echo "<font color='red'><b>Error :</b> ".$result_kit_update."</font>
			<script>
				$('#loadicon').hide();
				$('#$btn_name$id').attr('disabled',false);
			</script>";
			exit();
		} 
		
	if(@$result_update['error'])
	{
		closeConnection($conn);
		
		echo "<font color='red'><b>Error :</b> ".$result_update['error']['message']."</font><br><br>";	
		echo "<script>
			$('#$btn_name$id').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
}
	
StartCommit($conn);
$flag = true;	
	
	if($current_status=='Active')
	{
		$update_card_tab = Qry($conn,"UPDATE dairy.happay_card_inventory SET card_status='1' WHERE id='$id'");
		if(!$update_card_tab){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
	
		$update_card2 = Qry($conn,"UPDATE dairy.happay_card SET status='1' WHERE card_using='$card_no'");
		if(!$update_card2){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
	}
	else if($current_status=='Inactive')
	{
		$update_card_tab = Qry($conn,"UPDATE dairy.happay_card_inventory SET card_status='0' WHERE id='$id'");
		if(!$update_card_tab){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		
		$update_card2 = Qry($conn,"UPDATE dairy.happay_card SET status='0' WHERE card_using='$card_no'");
		if(!$update_card2){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
	}
	else if($current_status=='DETACH')
	{
		$update_card3 = Qry($conn,"UPDATE dairy.own_truck SET happay_active='0' WHERE tno='$card_no'");
		if(!$update_card3){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		
		$update_user = Qry($conn,"UPDATE dairy.happay_users SET card_id='0',card_kit_id='' WHERE veh_no='$card_no'");
		if(!$update_user){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		
		$insert_detach_log = Qry($conn,"INSERT INTO dairy.happay_detached_cards(card_no,kit_id,timestamp) VALUES 
		('$card_no','$card_kit_id','$timestamp')");
		
		if(!$insert_detach_log){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		
		$delete_card = Qry($conn,"DELETE FROM dairy.happay_card_inventory WHERE id='$id'");
		if(!$delete_card){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		
		$update_main_card = Qry($conn,"UPDATE dairy.happay_card SET card_using=tno WHERE card_using='$card_no'");
		if(!$update_main_card){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		
		$delete_record = Qry($conn,"DELETE FROM dairy.happay_card WHERE card_using='$card_no'");
		if(!$delete_record){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		
	} 
	 
	$log_insert=Qry($conn,"INSERT INTO dairy.happay_api_log(req_id,tno,result,api_name,api_response,status,timestamp) VALUES 
		('$requestId','$veh_no','$result_res','$log_status','$result','1','$timestamp')");
	
	if(!$log_insert){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_status = Qry($conn,"UPDATE dairy.happay_api_log SET completed='1' WHERE req_id='$requestId'");	
	if(!$update_status){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('SUCCESS : Card marked as $current_status.');
		$('#$btn_name$id').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./get_all_cards.php");
	exit();
}

?>