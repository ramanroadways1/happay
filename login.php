<?php
session_start();

if(isset($_SESSION['happay_admin']))
{
	echo "<script>
		window.location.href='./index.php';
	</script>";
	exit();
}

?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta name="robots" content="noindex,nofollow"/>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Happay-Card : Control Panel | Log in</title>
  <link rel="shortcut icon" type="image/png" href="happay_small.jpg"/>
	<link rel="shortcut icon" type="image/png" href="happay_small.png"/>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  
<script type="text/javascript">
$(document).ready(function (e) {
$("#LoginForm").on('submit',(function(e) {
$("#login_button").attr("disabled",true);
$("#login_button_name").html("Please wait..");
e.preventDefault();
	$.ajax({
	url: "./validate_login.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
	$("#result").html(data);
	},
	error: function() 
	{} });}));});
</script> 
  
 <style type="text/css">
@import url(https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700);

body {
  background: #999;
  padding: 40px;
  font-family: "Open Sans Condensed", sans-serif;
}

#bg {
  position: fixed;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background: url(bg1.jpg) no-repeat center center fixed;
  background-size: cover;
  -webkit-filter: blur(4px);    
}

form {
  position: relative;
  width: 350px;
  margin: 0 auto;
  background: rgba(130,130,130,.3);
  padding: 20px 22px;
  border: 1px solid;
  border-top-color: rgba(255,255,255,.4);
  border-left-color: rgba(255,255,255,.4);
  border-bottom-color: rgba(60,60,60,.4);
  border-right-color: rgba(60,60,60,.4);
}

form input, form button {
  width: 300px;
  border: 1px solid;
  border-bottom-color: rgba(255,255,255,.5);
  border-right-color: rgba(60,60,60,.35);
  border-top-color: rgba(60,60,60,.35);
  border-left-color: rgba(80,80,80,.45);
  background-color: rgba(0,0,0,.2);
  background-repeat: no-repeat;
  padding: 8px 24px 8px 10px;
  font: bold .875em/1.25em "Open Sans Condensed", sans-serif;
  letter-spacing: .075em;
  color: #fff;
  text-shadow: 0 1px 0 rgba(0,0,0,.1);
  margin-bottom: 19px;
}

form input:focus { background-color: rgba(0,0,0,.4); }

::-webkit-input-placeholder { color: #ccc; text-transform: uppercase; }
::-moz-placeholder { color: #ccc; text-transform: uppercase; }
:-ms-input-placeholder { color: #ccc; text-transform: uppercase; }

form button[type=submit] {
  width: 300px;
  margin-bottom: 0;
  color: #FFF;
  letter-spacing: .05em;
  text-shadow: 0 1px 0 #133d3e;
  text-transform: uppercase;
  background: #225556;
  border-top-color: #9fb5b5;
  border-left-color: #608586;
  border-bottom-color: #1b4849;
  border-right-color: #1e4d4e;
  cursor: pointer;
}

  </style>
</head>
<body>
  <br />
  <br />
  <br />
<div id="bg"></div>

<form id="LoginForm" method="POST" autocomplete="off">
    
	<center>
		<i style="color:#FFF" class="fa fa-sign-in"></i> &nbsp;<span><font color="#FFF">Happay Control Panel : Log In</font></span>
	</center>
    <br />
  
  <input type="text" style="text-transform:uppercase" name="username" id="" placeholder="Username" class="username" required>
  
  <input type="password" name="password" id="" placeholder="Password" class="password" required>
  
	<center>
		<button type="submit" id="login_button" name="login"><span id="login_button_name">Log in</span></button>
	</center>
<br />
	<center><div id="result" style="color:maroon"></div></center>
</form>

<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>