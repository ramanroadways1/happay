<?php
require_once("connect.php");

$requestId=date("Y:m:d_H:i:s");

$timestamp = date("Y-m-d H:i:s");
$u_id = escapeString($conn,strtoupper($_POST['id']));
$company = escapeString($conn,strtoupper($_POST['company']));
$veh_no = escapeString($conn,strtoupper($_POST['vehicle_db']));
$veh_no_new = escapeString($conn,strtoupper($_POST['veh_no']));

if($veh_no_new==$veh_no)
{
	echo "<script>
		alert('Nothing to update !');
		$('#submit_button_2').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$chk_new_veh_no = Qry($conn,"SELECT f_name FROM dairy.happay_users WHERE veh_no='$veh_no_new'");

if(!$chk_new_veh_no){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error !","./update_user_details.php");
	exit();
}

if(numRows($chk_new_veh_no) > 0)
{
	$row_user = fetchArray($chk_new_veh_no);
	
	echo "<script>
		alert('Error : Vehicle number already assigned to $row_user[f_name] !');
		$('#submit_button_2').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$chk_vehicle = Qry($conn,"SELECT id,comp FROM dairy.own_truck WHERE tno='$veh_no_new' AND is_sold=0");

if(!$chk_vehicle){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error !","./update_user_details.php");
	exit();
}

if(numRows($chk_vehicle)==0)
{
	echo "<script>
		alert('Error : Inavalid vehicle number !');
		$('#submit_button_2').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$verify_user = Qry($conn,"SELECT veh_no,company FROM dairy.happay_users WHERE id='$u_id'");

if(!$verify_user){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error !","./update_user_details.php");
	exit();
}

if(numRows($verify_user)==0)
{
	echo "<script>
		alert('Error : user not found.');
		$('#submit_button_2').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$row_verify = fetchArray($verify_user);

if($row_verify['veh_no']!=$veh_no)
{
	echo "<script>
		alert('Error : vehicle number not verified.');
		$('#submit_button_2').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$requestId = mt_rand().date("dmyhis");

$json_update = array(
"requestId"=>$requestId,
"userId"=>$veh_no,
"metaFields"=>array(
	"Vehicle Number"=>$veh_no_new,
)
);

$json_update = json_encode($json_update);

$result_update = HappayAPI("auth/v1/updateuser/",$json_update,$row_verify['company']);

$result_decode = json_decode($result_update, true);

	if(strpos($result_update,"Unauthorized") !== false)
	{ 
		echo "<font color='red'><b>Error :</b> ".$result_update."</font>";
		echo "<script>
			$('#loadicon').fadeOut('slow');
			$('#submit_button_2').attr('disabled',false);
		</script>";
		exit();
	} 
	
	if(!empty($result_decode['error']['message']))
	{
		echo "<font color='red'><b>Error :</b> ".$result_decode['error']['message']."</font><br><br>";
		echo "<script>
			$('#loadicon').fadeOut('slow');
			$('#submit_button_2').attr('disabled',false);
		</script>";
		exit();
	}
	
	echo "<font color='green'><b>SUCCESS :</b> ".$result_decode['res_str']."</font><br><br>";
		
$update_user = Qry($conn,"UPDATE dairy.happay_users SET veh_no='$veh_no_new' WHERE id='$u_id'");	
	
if(!$update_user){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error while processing request !');
		$('#loadicon').fadeOut('slow');
		$('#submit_button_2').attr('disabled',false);
	</script>";
	exit();
}

$update_user2 = Qry($conn,"UPDATE dairy.happay_card SET tno='$veh_no_new' WHERE tno='$veh_no'");	
	
if(!$update_user2){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error while processing request !');
		$('#loadicon').fadeOut('slow');
		$('#submit_button_2').attr('disabled',false);
	</script>";
	exit();
}

$update_user3 = Qry($conn,"UPDATE dairy.happay_card SET card_using='$veh_no_new' WHERE card_using='$veh_no'");	
	
if(!$update_user3){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error while processing request !');
		$('#loadicon').fadeOut('slow');
		$('#submit_button_2').attr('disabled',false);
	</script>";
	exit();
}

$update_user4 = Qry($conn,"UPDATE dairy.happay_card_transactions SET card_no='$veh_no_new' WHERE card_no='$veh_no'");	
	
if(!$update_user4){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error while processing request !');
		$('#loadicon').fadeOut('slow');
		$('#submit_button_2').attr('disabled',false);
	</script>";
	exit();
}

$update_user5 = Qry($conn,"UPDATE dairy.happay_card_transactions SET card_using='$veh_no_new' WHERE card_using='$veh_no'");	
	
if(!$update_user5){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error while processing request !');
		$('#loadicon').fadeOut('slow');
		$('#submit_button_2').attr('disabled',false);
	</script>";
	exit();
}

$update_user6 = Qry($conn,"UPDATE dairy.happay_card_withdrawal SET tno='$veh_no_new' WHERE tno='$veh_no'");	
	
if(!$update_user6){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error while processing request !');
		$('#loadicon').fadeOut('slow');
		$('#submit_button_2').attr('disabled',false);
	</script>";
	exit();
}

$update_user7 = Qry($conn,"UPDATE dairy.happay_live_balance SET veh_no='$veh_no_new' WHERE veh_no='$veh_no'");	
	
if(!$update_user7){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error while processing request !');
		$('#loadicon').fadeOut('slow');
		$('#submit_button_2').attr('disabled',false);
	</script>";
	exit();
}

$update_user8 = Qry($conn,"UPDATE dairy.happay_webhook_live SET card_no='$veh_no_new' WHERE card_no='$veh_no'");	
	
if(!$update_user8){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error while processing request !');
		$('#loadicon').fadeOut('slow');
		$('#submit_button_2').attr('disabled',false);
	</script>";
	exit();
}
	
$log_insert = Qry($conn,"INSERT INTO dairy.happay_log(req_id,tno,result,api_response,status,timestamp) VALUES 
('$requestId','$u_id','$result_decode[res_str]','$result_update','1','$timestamp')");
	
if(!$log_insert){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error while processing request !');
		$('#loadicon').fadeOut('slow');
		$('#submit_button_2').attr('disabled',false);
	</script>";
	exit();
}
	
$update_log = Qry($conn,"INSERT INTO dairy.happay_edit_log(card_no,log_type,response,timestamp) VALUES 
('$veh_no','UPDATE_USER','Vehicle No Updated. $veh_no to $veh_no_new.','$timestamp')");
	
if(!$update_log){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error while processing request !');
		$('#loadicon').fadeOut('slow');
		$('#submit_button_2').attr('disabled',false);
	</script>";
	exit();
}
	
echo "<script>
	alert('SUCCESS : $result_decode[res_str] !');
	document.getElementById('UserForm2').reset();
	document.getElementById('modal_hide2').click(); 
	$('#loadicon').fadeOut('slow');
</script>";
exit();

?>