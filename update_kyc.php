<?php
include "header.php";
?>

<style>
::-webkit-scrollbar{
    width: 5px;
    height: 5px;
}
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
    border-radius: 5px;
}
::-webkit-scrollbar-thumb {
    border-radius: 5px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9); 
}

@media (min-width: 768px) {
  .modal-xl {
    width: 95%;
   max-width:100%;
  }
}
</style>
<div class="content-wrapper">
    <section class="content-header">
      Update KYC :
      
	  &nbsp; <button type="button" onclick="LoadTrans();" class="btn-xs btn btn-primary">Reload Kyc Status</button>
	  <style>
	  label{font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div id="func_result"></div>

<script>
function LoadTrans()
{
	$("#loadicon").show();
		jQuery.ajax({
			url: "_API/refresh_kyc_status.php",
			data: 'var=' + 'OK',
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
		},
		error: function() {}
	});
}
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#KycForm").on('submit',(function(e) {
$("#loadicon").show();
$("#submit_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_kyc.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#tab_result").html(data);
	},
	error: function() 
	{} });}));});
</script>	
	
<div class="row">
 
 <div class="form-group col-md-12">
    
	<div class="form-group col-md-12 table-responsive">
	 <br />
        <table id="example" class="table table-bordered table-striped" style="font-size:12px;">
		 <thead>	
          <tr>
				<th>#id</th>
				<th>Name</th>
				<th>Veh_No</th>
				<th>Mobile</th>
				<th>Document Type/No.</th>
				<th>DOB</th>
				<th>Kyc Status</th>
				<th>KycStatus Happay</th>
				<th>Reason</th>
				<th>#GenURL</th>
				<th>#KYC_Link</th>
			</tr>
          </thead>	 
		 <tbody> 		 
            <?php
              $sql = Qry($conn,"SELECT id,f_name,l_name,veh_no,company,kyc_status,kyc_url,kyc_status_happay,kyc_decline_reason,
			  doc_type,document_no,mobile,dob 
			  FROM dairy.happay_users WHERE kyc_status_happay!='Approved'");
              
			  if(!$sql){
				  echo getMySQLError($conn);
				  errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
				  exit();
			  }
			  
			  if(numRows($sql)==0)
			  {
				echo "<tr>
						<td colspan='12'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			  }
			 else
			 {
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
				 
				if($row['document_no']==''){
					$kyc_status="<b><font color='red'>Document No. Pending</font></b>";
				}else{
					if($row['kyc_status']==0){	
					 $kyc_status="<b><font color='red'>Document Pending</font></b>";
				 }else if($row['kyc_status']==1){
					 $kyc_status="<b><font color='blue'>Addr. Proof<br>Pending</font></b>";
				 }else if($row['kyc_status']==2){
					 $kyc_status="<b><font color='blue'>Photo<br>Pending</font></b>";
				 }else{
					$kyc_status="<b><font color='green'>KYC Done</font></b>";
				 }
				}	
				
				if($row['kyc_url']=='')
				{
					$link_kyc_btn="disabled";
					$btn_gen="";
				}
				else
				{
					$link_kyc_btn="";
					$btn_gen="disabled";
				}
				
				echo 
                "<tr>
					<td>$sn</td>
					<td>$row[f_name] $row[l_name]</td>
					<td>$row[veh_no]<br>$row[company]</td>
					<td>$row[mobile]</td>
					<td id='doc_td$row[id]'>$row[document_no]<br>$row[doc_type]</td>
					<td>";
					if($row['dob']==0){ echo ""; }else { echo date("d/m/y",strtotime($row['dob'])); }
					echo"</td>	
					<td id='kyc_status_td$row[id]'>$kyc_status</td>
					<td style='color:red'>$row[kyc_status_happay]</td>
					<td style='color:red'>$row[kyc_decline_reason]</td>
					<td>
						<input type='hidden' id='doc_no_box$row[id]' value='$row[document_no]'>
						<input type='hidden' id='kyc_status_box$row[id]' value='$row[kyc_status]'>
						<input type='hidden' id='customer_name$row[id]' value='$row[f_name] $row[l_name]'>
						<button type='button' $btn_gen id='kyc_button$row[id]' onclick=Kyc('$row[id]','$row[mobile]','$row[company]','$row[veh_no]') 
						class='btn btn-success btn-xs'>Gen. Kyc Url</button>
					</td> 
					<input type='hidden' value='$row[kyc_url]' id='link_kyc_2_$row[id]'>
					<td>
						<button onclick='KycLinkLoad($row[id])' type='button' id='kyc_link_btn_$row[id]' $link_kyc_btn target='_blank' class='btn btn-xs btn-primary'>Do KYC</button>
					</td>
				</tr>
				";
				$sn++;		
				
				// <td>
						// <a target='_blank' id='link_kyc_$row[id]' href='$row[kyc_url]'><button onclick='$(this).prop(\"disabled\", true);KycLinkLoad($row[id])' type='button' id='kyc_link_btn_$row[id]' $link_kyc_btn target='_blank' class='btn btn-sm btn-primary'>Do KYC</button></a>
					// </td>
              }
			}
            ?>
		</tbody> 			
        </table>
      </div>
    </div>
  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>

<script>
// function KycUrlWindow() {
  // window.open("cash_allow_records.php", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=150,left=500,width=400,height=400");
// }
</script>

<style>
.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}

.modal-body {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}
</style>

<button type="button" id="ModalButton" style="display:none" class="btn btn-primary" data-toggle="modal" data-target="#myModal"></button>

<div class="modal fade" id="myModal" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-xl modal-dialog-centered">
    <div class="modal-content" style="max-height: calc(100vh - 70px);overflow: auto;">

      <div class="bg-primary modal-header">
        <h4 class="modal-title">KYC : <span style="color:" id="customer_name"></span></h4>
      </div>
	<div class="modal-body">
		<iframe class="responsive-iframe" src=""></iframe>
    </div>

      <div class="modal-footer">
        <button type="button" id="close_modal_button" class="btn btn-sm btn-danger" data-dismiss="modal">Close</button>
       </div>
	</div>
  </div>
</div>	

<script>	
// function ifrhgh(){
    // var iframehght =  $("#print_frame").contents().height();
    // $("#print_frame").height(iframehght);
// }

function KycLinkLoad(id)
{
	var link1 = $('#link_kyc_2_'+id).val();
	$("#myModal iframe").attr("src", link1);
    $("#ModalButton")[0].click();
}

function Kyc(id,userid,company,veh_no)
{
	$('#kyc_button'+id).attr('disabled',true);
	var customer_name = $('#customer_name'+id).val();
	var doc_no_ip = $('#doc_no_box'+id).val();
	var kyc_status = $('#kyc_status_box'+id).val();
	
	if(id=='')
	{
		alert('User-Id not found !');
	}
	else if(veh_no=='')
	{
		alert('Vehicle number not found !');
	}
	else
	{
		$("#loadicon").show();
			jQuery.ajax({
				url: "gen_video_kyc_url.php",
				data: 'id=' + id + '&veh_no=' + veh_no,
				type: "POST",
				success: function(data) {
				$("#func_result").html(data);
			},
			error: function() {}
		});
	}
}

function MyFunc1(id){
	$('#kyc_button'+id).attr('disabled',false);
}


$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

<?php
include "footer.php";
?>