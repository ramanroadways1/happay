<?php
require_once("./connect.php");

$tno = escapeString($conn,$_POST['tno']);
$amount = escapeString($conn,$_POST['amount']);

if($tno=="" || $amount=="")
{
	?>
	<label>Select Trip <font color="red">*</font></label>
	<select name="trip_id" id="trip_id" class="form-control" required="required">
		<option value="">--select trip--</option> 
	</select>	
	<?php
	echo "<script>
		alert('Enter Vehicle No and Amount First !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$otp = '1234567890';
$otp = substr(str_shuffle($otp),0,6);

$msg_template="Credit amount of ATM related issue.\nVehicle_no: $tno, Amount: $amount.\nOTP: $otp.\nRamanRoadways.";
// SendMsgCustom(9024281599,"Credit amount of ATM related issue.\nVehicle_no: $tno, Amount: $amount.\nOTP: $otp.");
// MsgHappayCreditAmountAtmIssue($tno,$amount,$otp);

SendWAMsg($conn,9024281599,$msg_template);

$_SESSION['session_otp'] = $otp;

$get_trips = Qry($conn,"SELECT id,from_station,to_station,date(date) as start_date FROM dairy.trip WHERE tno='$tno'");

if(!$get_trips){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request.","./");
	exit();
}

if(numRows($get_trips)==0)
{
	?>
	<label>Select Trip <font color="red">*</font></label>
	<select name="trip_id" id="trip_id" class="form-control" required="required">
		<option value="">--select trip--</option> 
	</select>	
	<?php
	
	 echo "<script>
		alert('Vehicle Trips not found !');
		$('#loadicon').hide();
	</script>";
	exit();
}

?>
	<label>Select Trip <font color="red">*</font></label>
	<select name="trip_id" id="trip_id" class="form-control" required="required">
		<option value="">--select trip--</option> 
<?php

while($row_trips = fetchArray($get_trips))
{
	echo "<option value='$row_trips[id]'>$row_trips[from_station] - $row_trips[to_station] : $row_trips[start_date]</option>";
} 

	echo "
	 </select>	
	 <script>
			alert('OTP Sent !');
			$('#loadicon').hide();
	</script>";
	exit();
?>