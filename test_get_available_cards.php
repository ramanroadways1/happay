<?php
require_once("connect.php");

$JsonString_RRPL = array("requestId"=>mt_rand(),"start"=>"0","count"=>"500");
$JsonString_RR = array("requestId"=>mt_rand(),"start"=>"0","count"=>"500");

$JsonString_RRPL = json_encode($JsonString_RRPL);
$JsonString_RR = json_encode($JsonString_RR);

$result_RRPL = HappayAPI("auth/v1/cards/get_available_cards/",$JsonString_RRPL,"RRPL");
$result_RR = HappayAPI("auth/v1/cards/get_available_cards/",$JsonString_RR,"RAMAN_ROADWAYS");
	
$result_RRPL = json_decode($result_RRPL, true);
$result_RR = json_decode($result_RR, true);

echo "<table border='1'>";

$i=1;
foreach($result_RRPL['res_data']['card_list'] as $rrpl_kits)
{
	$veh_no1 = str_replace(" RRPL","",$rrpl_kits["embossing_name"]);
	echo "<tr><td>$i</td><td>".$rrpl_kits["card_kit_id"]."</td><td>".$veh_no1."</td></tr>";
$i++;
}
echo "</table>";
exit();

// API URL
$url = 'https://rrpl.online/happay/_API/';

// Create a new cURL resource
$ch = curl_init($url);

// Setup request to send json via POST
$data = array(
       "action" => "HAPPAY_VERIFIED",
		"data_fields" => array(
           "id" => "ID",
           "firstname" => "YES",
           "lastname" =>"LBAE",
           "email" => "email"
       )
);
 
 $payload = json_encode($data);

// Attach encoded JSON string to the POST fields
curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

// Set the content type to application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

// Return response instead of outputting
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// Execute the POST request
$result = curl_exec($ch);

// Close cURL resource
curl_close($ch);

echo $result;