<?php
require_once("../_connect.php");

$timestamp = date("Y-m-d H:i:s");
$today_date = date("Y-m-d");

$update_Org_Wallet = Qry($conn,"SELECT id,txn_date as txn_timestamp,date(txn_date) as txn_date1 
FROM dairy.happay_webhook_load_wallet ORDER BY id DESC LIMIT 1");
if(!$update_Org_Wallet){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

$row_OrgLoad = fetchArray($update_Org_Wallet);

$last_entry_date = $row_OrgLoad['txn_date1'];

$dataOrgLoad_RRPL = array(
"requestId"=>"REQ12345679",
"Wallet"=>"Imprest",
"start_date"=>date("d-m-Y"),
"end_date"=>date("d-m-Y")
);

$dataOrgLoad_RR = array(
"requestId"=>"REQ12345600",
"Wallet"=>"Imprest",
"start_date"=>date("d-m-Y"),
"end_date"=>date("d-m-Y")
);

$JsonString_RRPL = json_encode($dataOrgLoad_RRPL);
$JsonString_RR = json_encode($dataOrgLoad_RR);

$token_RRPL = $happay_token_of_rrpl;
$token_RR = $happay_token_of_rr;

$ch_RRPL = curl_init('https://api-v2.happay.in/auth/v1/cards/get_org_txns/');
curl_setopt($ch_RRPL, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch_RRPL, CURLOPT_TIMEOUT, 900);
curl_setopt($ch_RRPL, CURLINFO_HEADER_OUT, true);
curl_setopt($ch_RRPL, CURLOPT_POST, true);
curl_setopt($ch_RRPL, CURLOPT_POSTFIELDS, $JsonString_RRPL);
curl_setopt($ch_RRPL, CURLOPT_HTTPHEADER, array(
	'Authorization: Bearer '.$token_RRPL.'',
    'Content-Type: application/json',
    'Content-Length: ' . strlen($JsonString_RRPL))
);
$result_RRPL = curl_exec($ch_RRPL);
curl_close($ch_RRPL);
// echo $result_RRPL;
$result_RRPL = json_decode($result_RRPL, true);

if(@$result_RRPL['error']=="")
{
	if($result_RRPL['res_data']['total_count']>0)
	{
		$total_load_amount = 0;
		$total_withdraw_amount = 0;
		
		foreach($result_RRPL['res_data']['txn_list'] as $data_RRPL)
		{
			$chk_txn_id = Qry($conn,"SELECT id FROM dairy.happay_webhook_load_wallet WHERE date(txn_date) BETWEEN 
			'$last_entry_date' AND '$today_date' AND txn_id='$data_RRPL[txn_id]'");
			
			if(!$chk_txn_id){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			if(numRows($chk_txn_id)==0)
			{
				if($data_RRPL['txn_type_id']=="17" || $data_RRPL['txn_type_id']=="19" || $data_RRPL['txn_type_id']=="18")
				{
					$txn_date_Zone11 = substr($data_RRPL['txn_date'],-5);
					if($txn_date_Zone11=="00:00")
					{
						$txn_date1Org = date('Y-m-d H:i:s',strtotime('+5 hour +30 minutes',strtotime(substr($data_RRPL['txn_date'],0,19))));
						$txn_date_org_load = substr($txn_date1Org,0,19);
					}
					else
					{
						$txn_date_org_load = substr($data_RRPL['txn_date'],0,19);		
					}
					
					if(strtotime($txn_date_org_load)>strtotime($row_OrgLoad['txn_timestamp']))
					{
						// echo $txn_date_org_load;
						if($data_RRPL['txn_type_id']=="17" || $data_RRPL['txn_type_id']=="18")
						{
							$total_load_amount = $total_load_amount+$data_RRPL['currency_amount'];
						}
						else
						{
							$total_withdraw_amount = $total_withdraw_amount+$data_RRPL['currency_amount'];
						}
						
						$insertOrgTxns = Qry($conn,"INSERT INTO dairy.happay_webhook_load_wallet(company,merchant,dr_cr,txn_id,txn_type,currency_amount,
						txn_type_id,txn_date,timestamp) VALUES ('RRPL','$data_RRPL[merchant]','$data_RRPL[debit_or_credit_indicator]',
						'$data_RRPL[txn_id]','$data_RRPL[txn_type]','$data_RRPL[currency_amount]','$data_RRPL[txn_type_id]',
						'$txn_date_org_load','$timestamp')");
						
						if(!$insertOrgTxns){
							errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						}
					}
				}
			}
		}
		
		if($total_load_amount>0){
			$update_main_bal = Qry($conn,"UPDATE dairy.happay_main_balance SET balance=balance+'$total_load_amount',
			api_bal=api_bal+'$total_load_amount' WHERE company='RRPL'");
			
			if(!$update_main_bal){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}		
		}	

		if($total_withdraw_amount>0)
		{
			$update_main_bal1 = Qry($conn,"UPDATE dairy.happay_main_balance SET balance=balance-'$total_load_amount',
			api_bal=api_bal-'$total_withdraw_amount' WHERE company='RRPL'");
			
			if(!$update_main_bal1){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}		
		}			
	}
}

// For raman roadways

$ch_RR = curl_init('https://api-v2.happay.in/auth/v1/cards/get_org_txns/');
curl_setopt($ch_RR, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch_RR, CURLOPT_TIMEOUT, 900);
curl_setopt($ch_RR, CURLINFO_HEADER_OUT, true);
curl_setopt($ch_RR, CURLOPT_POST, true);
curl_setopt($ch_RR, CURLOPT_POSTFIELDS, $JsonString_RR);
curl_setopt($ch_RR, CURLOPT_HTTPHEADER, array(
	'Authorization: Bearer '.$token_RR.'',
    'Content-Type: application/json',
    'Content-Length: ' . strlen($JsonString_RR))
);
$result_RR = curl_exec($ch_RR);
curl_close($ch_RR);
$result_RR = json_decode($result_RR, true);

if(@$result_RR['error']=="")
{
	if($result_RR['res_data']['total_count']>0)
	{
		$total_load_amount_rr = 0;
		$total_withdraw_amount_rr = 0;
		foreach($result_RR['res_data']['txn_list'] as $data_RR)
		{
			if($data_RR['txn_type_id']=="17" || $data_RR['txn_type_id']=="19" || $data_RR['txn_type_id']=="18")
			{
				$chk_txn_id_rr = Qry($conn,"SELECT id FROM dairy.happay_webhook_load_wallet WHERE date(txn_date) BETWEEN 
				'$last_entry_date' AND '$today_date' AND txn_id='$data_RR[txn_id]'");
				
				if(!$chk_txn_id_rr){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				if(numRows($chk_txn_id_rr)==0)
				{
					$txn_date_Zone11 = substr($data_RR['txn_date'],-5);
					if($txn_date_Zone11=="00:00")
					{
						$txn_date1Org = date('Y-m-d H:i:s',strtotime('+5 hour +30 minutes',strtotime(substr($data_RR['txn_date'],0,19))));
						$txn_date_org_load = substr($txn_date1Org,0,19);
					}
					else
					{
						$txn_date_org_load = substr($data_RR['txn_date'],0,19);		
					}
					
					if(strtotime($txn_date_org_load)>strtotime($row_OrgLoad['txn_timestamp']))
					{
						if($data_RR['txn_type_id']=="17" || $data_RR['txn_type_id']=="18")
						{
							$total_load_amount_rr = $total_load_amount_rr+$data_RR['currency_amount'];
						}
						else
						{
							$total_withdraw_amount_rr = $total_withdraw_amount_rr+$data_RR['currency_amount'];
						}
						
						$insertOrgTxns = Qry($conn,"INSERT INTO dairy.happay_webhook_load_wallet(company,merchant,dr_cr,txn_id,txn_type,currency_amount,
						txn_type_id,txn_date,timestamp) VALUES ('RAMAN_ROADWAYS','$data_RR[merchant]','$data_RR[debit_or_credit_indicator]',
						'$data_RR[txn_id]','$data_RR[txn_type]','$data_RR[currency_amount]','$data_RR[txn_type_id]',
						'$txn_date_org_load','$timestamp')");
						
						if(!$insertOrgTxns){
							errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						}
					}
				}
			}
		}
		
		if($total_load_amount_rr>0){
			$update_main_bal = Qry($conn,"UPDATE dairy.happay_main_balance SET balance=balance+'$total_load_amount_rr',
			api_bal=api_bal+'$total_load_amount_rr' WHERE company='RAMAN_ROADWAYS'");
			
			if(!$update_main_bal){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}		
		}	

		if($total_withdraw_amount_rr>0)
		{
			$update_main_bal1 = Qry($conn,"UPDATE dairy.happay_main_balance SET balance=balance-'$total_withdraw_amount_rr',
			api_bal=api_bal-'$total_withdraw_amount_rr' WHERE company='RAMAN_ROADWAYS'");
			
			if(!$update_main_bal1){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}		
		}			
	}
}

// if($flag)
// {
	// MySQLCommit($conn);
	// closeConnection($conn);
	
	// echo "<script>
		// alert('All Transactions Fetched Successfully !');
		// $('#loadicon').hide();
	// </script>"; 
	// exit();
// }
// else
// {
	// echo "<script>
		// alert('Error while Fetching Transactions !');
		// $('#loadicon').hide();
	// </script>"; 
	// MySQLRollBack($conn);
	// closeConnection($conn);
	// errorLog("Error While Processing Request.",$conn,$page_name,__LINE__);
	// exit();
// }	
echo "ALL DONE";	
?>	
