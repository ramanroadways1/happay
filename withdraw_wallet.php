<?php
include "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
      <h4>
		Withdraw wallet and debit driver :
		<br>
		<br>
		<font color="maroon">कार्ड में से कंपनी वॉलेट में क्रेडिट करे और ड्राइवर को भी डेबिट करे  । </font>
      </h4>
	  <?php
		// echo date('Y-m-d H:i:s',strtotime('+5 hour +30 minutes',strtotime('2020-08-09 13:44:25')));
	  ?>
	  <style>
		.form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div class="row">
 
 <div class="form-group col-md-12">
    
	<div class="form-group col-md-12 table-responsive">
	<div id="result"></div>
	 <br />
       <table id="example" class="table table-bordered table-striped" style="font-size:12.5px;">
		<thead>		
		 <tr>
				<th>#</th>
				<th>Vehicle Number</th>
				<th>Company</th>
				<th>Balance</th>
				<th>Withdraw Amount</th>
			</tr>
          </thead>
		 <tbody> 
            <?php
              $sql = Qry($conn,"SELECT c.id,c.veh_no,c.card_kit_id,c.company,b.balance 
			  FROM dairy.happay_card_inventory AS c 
			  LEFT OUTER JOIN dairy.happay_live_balance AS b ON b.veh_no=c.veh_no 
			  WHERE c.card_assigned='1' AND c.card_status='1'");
              
			 if(!$sql){
				echo getMySQLError($conn);
				errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
				exit();
			}
			  
			  if(numRows($sql)==0)
			  {
				echo "<tr>
						<td colspan='12'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			  }
			 else
			 {
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
				echo 
                "<tr>
					<td>$sn</td>
					<td>$row[veh_no]<br>($row[card_kit_id])</td>
					<td>$row[company]</td>
					<td>$row[balance]</td>
					<td>
			<input type='hidden' value='$row[balance]' id='BalAmount$row[id]'>		
		<div class='form-inline'>
			<input type='number' class='form-control' style='font-size:12px;height:30px;' id='amount$row[id]'>
<button type='button' id='button1$row[id]' onclick='WithdrawAmount($row[id])' 
class='btn btn-primary btn-sm'><span style='font-size:px;' class='glyphicon glyphicon-share-alt'></span></a></div>
					</td>
				</tr>
				";
				$sn++;		
              }
			}
            ?>
		</tbody>	
        </table>
      </div>
    </div>
  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>

<script>	
function WithdrawAmount(id,amount)
{
	// alert('Temporary unavailable !');
	
	var amount = $('#amount'+id).val();
	var balance_amount = $('#BalAmount'+id).val();
	
	if(amount!=0 && amount!='' && amount>0)
	{
		if(amount>balance_amount)
		{
			alert('Please check withdraw amount is grater than balance amount !');
		}
		else
		{
			$('#button1'+id).attr('disabled',true);
			$("#loadicon").show();
			jQuery.ajax({
				url: "withdraw_wallet_action.php",
				data: 'id=' + id + '&amount=' + amount,
				type: "POST",
				success: function(data) {
				$("#result").html(data);
				},
				error: function() {}
			});
		}
	}
	else
	{
		alert('Please enter valid amount value !');
	}
}

$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

<?php
include "footer.php";
?>