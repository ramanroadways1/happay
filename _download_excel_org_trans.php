<?php
date_default_timezone_set('Asia/Kolkata');

$days_prev=date('Y-m-d',strtotime("yesterday"));

include_once('../mail_lib/config.php');
include('../mail_lib/Classes/PHPExcel.php');

$from_date = mysqli_real_escape_string($conn,$_REQUEST['from_date']);
$to_date = mysqli_real_escape_string($conn,$_REQUEST['to_date']);

$objPHPExcel = new PHPExcel();

$query = "SELECT * FROM happay_webhook_load_wallet WHERE date(txn_date) BETWEEN '$from_date' AND '$to_date' ORDER BY txn_date ASC";

$result = mysqli_query($conn,$query);
// while($row=mysqli_fetch_array($result))
// {
	$sn=1;
		
// }
// exit();

$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Company');
$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Merchant');
$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Txn_Id');
$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Trans_Type');
$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Credit');
$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Debit');
$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Txn_Date');

$objPHPExcel->getActiveSheet()->getStyle("A1:G1")->getFont()->setBold(true);

$rowCount	=	2;

if(mysqli_num_rows($result) > 0)
{
while($row = mysqli_fetch_array($result))
{
	if($row['dr_cr']=="C"){
			$cr_amt = $row['currency_amount'];
			$dr_amt = 0;
		}
		else{
			$dr_amt = $row['currency_amount'];
			$cr_amt = 0;
		}
		
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, mb_strtoupper($row['company'],'UTF-8'));
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, mb_strtoupper($row['merchant'],'UTF-8'));
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, mb_strtoupper($row['txn_id'],'UTF-8'));
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, mb_strtoupper($row['txn_type'],'UTF-8'));
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, mb_strtoupper($cr_amt,'UTF-8'));
	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, mb_strtoupper($dr_amt,'UTF-8'));
	$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, mb_strtoupper($row['txn_date'],'UTF-8'));
	$rowCount++;
}

// START - Auto size columns for each worksheet
foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {

    $objPHPExcel->setActiveSheetIndex($objPHPExcel->getIndex($worksheet));

    $sheet = $objPHPExcel->getActiveSheet();
    $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
    $cellIterator->setIterateOnlyExistingCells(true);

    foreach ($cellIterator as $cell) {
        $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
    }
}
// END - Auto size columns for each worksheet

$objWriter	=	new PHPExcel_Writer_Excel2007($objPHPExcel);

	$filename = 'Happay_Org_Txn.xlsx';
	 
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="'. $filename);
	header('Cache-Control: max-age=0');
	$objWriter->save('php://output');
	 // $objWriter->save('./'.$filename);
}
?>