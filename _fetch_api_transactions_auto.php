<?php
require_once '../_connect.php';

$timestamp = date("Y-m-d H:i:s");

$chk_file_error = Qry($conn,"SELECT id FROM dairy.running_scripts WHERE file_name='Happay_Auto_Txn_Error'");

if(!$chk_file_error){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($chk_file_error)>0)
{
	echo "File running error";
	exit();
}

$last_txn_date = Qry($conn,"SELECT date(trans_date) as txn_date,trans_date FROM dairy.happay_webhook_live ORDER BY id DESC LIMIT 1");

if(!$last_txn_date){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

$row_last_txn = fetchArray($last_txn_date);

$last_txn_timestamp = $row_last_txn['trans_date'];

$to_date = $row_last_txn['txn_date'];
$from_date = date('Y-m-d', strtotime($to_date.' -3 day'));

$get_new_txns = Qry($conn,"SELECT tno,company,first_name,last_name,user_id,email_id,mobile_no,payee_merchant,transaction_id,transaction_date,
transaction_type,amount,date(transaction_date),timestamp FROM dairy.happay_webhook WHERE transaction_type 
IN('Wallet Load Credit','Wallet Withdraw Debit','ATM Expenses','Card Transaction','Fee Expenses','ATM Reversal','FEE Reversal') AND 
transaction_id NOT IN(SELECT txn_id FROM dairy.happay_webhook_live WHERE date(trans_date) BETWEEN '$from_date' AND '$to_date') 
ORDER BY transaction_date ASC");

if(!$get_new_txns){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_new_txns)>0)
{
	StartCommit($conn);
	$flag = true;	
	
	$insert_file_log = Qry($conn,"INSERT INTO dairy.running_scripts_log(file_name,response,start_timestamp) VALUES ('Happay_get_txn','Start_File',
	'$timestamp')");

	if(!$insert_file_log){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$inserted_log_id = mysqli_insert_id($conn);
 
	while($row = fetchArray($get_new_txns))
	{
		$get_balance = Qry($conn,"SELECT balance FROM dairy.happay_live_balance WHERE veh_no='$row[tno]'");
		
		if(!$get_balance){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$row_balance = fetchArray($get_balance);
		$balance = $row_balance['balance'];
		
		if($row['transaction_type']=='Wallet Load Credit' || $row['transaction_type']=='ATM Reversal'  || $row['transaction_type']=='FEE Reversal')
		{
			$new_balance = $balance + $row['amount'];
			$col_name="credit";
		}			
		else if($row['transaction_type']=='Wallet Withdraw Debit' || $row['transaction_type']=='ATM Expenses' || $row['transaction_type']=='Card Transaction' || $row['transaction_type']=='Fee Expenses')
		{
			$new_balance = $balance - $row['amount'];
			$col_name="debit";
		}
		else
		{
			$flag = false;
			errorLog("Invalid Txn Type: $row[transaction_type]",$conn,$page_name,__LINE__);
		}	

		$insert_txn = Qry($conn,"INSERT INTO dairy.happay_webhook_live(card_no,company,first_name,last_name,user_id,email_id,
		mobile_no,merchant,txn_id,trans_date,trans_type,`$col_name`,balance,date,timestamp) VALUES ('$row[tno]','$row[company]',
		'$row[first_name]','$row[last_name]','$row[user_id]','$row[email_id]','$row[mobile_no]','$row[payee_merchant]','$row[transaction_id]',
		'$row[transaction_date]','$row[transaction_type]','$row[amount]','$new_balance','$row[transaction_date]','$timestamp')");	

		if(!$insert_txn){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$update_balance = Qry($conn,"UPDATE dairy.happay_live_balance SET balance = '$new_balance' WHERE veh_no='$row[tno]'");	

		if(!$update_balance){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	
	$update_log_timestamp = Qry($conn,"UPDATE dairy.running_scripts_log SET end_timestamp='$timestamp' WHERE id='$inserted_log_id'");

	if(!$update_log_timestamp){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($flag)
	{
		MySQLCommit($conn);
		closeConnection($conn);
		echo "ALL OK";
	}
	else
	{
		MySQLRollBack($conn);
		closeConnection($conn);
		echo "ERROR";
	}
}
else
{
	echo "<center><font color='red'>No new txns found..</font></center>";
}
?>