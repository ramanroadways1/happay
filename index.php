<?php
include "header.php";

$today_date = date("Y-m-d");

$get_card_details = Qry($conn,"SELECT 
(SELECT COUNT(id) FROM dairy.happay_card_inventory) as 'total_cards',
(SELECT COUNT(id) FROM dairy.happay_card_inventory WHERE card_status='1') as 'active_cards',
(SELECT COUNT(id) FROM dairy.happay_card_inventory WHERE card_assigned='1') as 'assigned_cards',
(SELECT COUNT(id) FROM dairy.happay_card_inventory WHERE company='RRPL') as 'rrpl_cards',
(SELECT COUNT(id) FROM dairy.happay_card_inventory WHERE company='RAMAN_ROADWAYS') as 'rr_cards'");

if(!$get_card_details){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$row_details = fetchArray($get_card_details);

$get_wallet_details = Qry($conn,"SELECT 
(SELECT COUNT(id) FROM dairy.happay_webhook_live WHERE date='$today_date') as 'today_transactions',
(SELECT COALESCE(SUM(credit+debit),0) FROM dairy.happay_webhook_live WHERE date='$today_date') as 'today_transactions_amt',
(SELECT COUNT(id) FROM dairy.happay_webhook_live WHERE date='$today_date' AND trans_type='Wallet Load Credit') as 'today_load',
(SELECT COALESCE(SUM(credit),0) FROM dairy.happay_webhook_live WHERE date='$today_date' AND trans_type='Wallet Load Credit') as 'today_load_amt',
(SELECT COUNT(id) FROM dairy.happay_webhook_live WHERE date='$today_date' AND trans_type='Wallet Withdraw Debit') as 'today_withdraw',
(SELECT COALESCE(SUM(debit),0) FROM dairy.happay_webhook_live WHERE date='$today_date' AND trans_type='Wallet Withdraw Debit') as 'today_withdraw_amt',
(SELECT COUNT(id) FROM dairy.happay_webhook_live WHERE date='$today_date' AND trans_type='ATM Expenses') as 'atm_exp',
(SELECT COALESCE(SUM(debit),0) FROM dairy.happay_webhook_live WHERE date='$today_date' AND trans_type='ATM Expenses') as 'atm_exp_amt',
(SELECT COUNT(id) FROM dairy.happay_webhook_live WHERE date='$today_date' AND trans_type='Fee Expenses') as 'atm_fee',
(SELECT COALESCE(SUM(debit),0) FROM dairy.happay_webhook_live WHERE date='$today_date' AND trans_type='Fee Expenses') as 'atm_fee_amt',
(SELECT COUNT(id) FROM dairy.happay_webhook_live WHERE date='$today_date' AND trans_type='Card Transaction') as 'card_txn',
(SELECT COALESCE(SUM(debit),0) FROM dairy.happay_webhook_live WHERE date='$today_date' AND trans_type='Card Transaction') as 'card_txn_amt',
(SELECT COUNT(id) FROM dairy.happay_webhook_live WHERE date='$today_date' AND trans_type IN('ATM Reversal','FEE Reversal')) as 'reversal',
(SELECT COALESCE(SUM(credit),0) FROM dairy.happay_webhook_live WHERE date='$today_date' AND trans_type IN('ATM Reversal','FEE Reversal')) as 'reversal_amt',
(SELECT COUNT(id) FROM dairy.happay_webhook_load_wallet WHERE date(txn_date)='$today_date') as 'company_load',
(SELECT COALESCE(SUM(currency_amount),0) FROM dairy.happay_webhook_load_wallet WHERE date(txn_date)='$today_date') as 'company_load_amt'
");

if(!$get_wallet_details){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$row_details2 = fetchArray($get_wallet_details);
?>
<div class="content-wrapper">
    <section class="content-header">
      <h1 style="font-size:15px">
		Dashboard :
        <small></small>
      </h1>
    </section>

<section class="content">
		
	<div class="row" style="font-size:13px;">
		  
		 <a href="#"> 
		<div class="col-lg-3 col-xs-6">
			  <div class="small-box bg-green">
				<div class="inner">
				  <h4 style="font-size:14px;"><?php echo "RRPL Bal : ".$rrpl_balance; ?></h4>
				  <p style="font-size:14px;"><?php echo"RR Bal : ".$rr_balance; ?></p>
				</div>
			  </div>
		 </div> 
		 </a>
	 
		<a href="report/index.php?type=TOTAL_CARD" target="_blank">
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h4 style="font-size:14px;"><?php echo $row_details['total_cards']; ?></h4>
			<p style="font-size:14px;">Total Cards</p>
            </div>
          </div>
        </div>
        </a>
		
		<a href="#">
		<div class="col-lg-3 col-xs-6">
          <div class="small-box bg-yellow">
            <div class="inner">
              <h4 style="font-size:14px;"><?php echo $row_details['active_cards']; ?></h4>

              <p style="font-size:14px;">Active Card</p>
            </div>
           </div>
        </div>
		</a>
        
		 <a href="#">
		<div class="col-lg-3 col-xs-6">
          <div class="small-box bg-red">
            <div class="inner">
              <h4 style="font-size:14px;"><?php echo $row_details['assigned_cards']; ?></h4>

              <p style="font-size:14px;">Assgined Cards</p>
            </div>
          </div>
        </div>
		</a>
		
	</div>
	
	<div class="row" style="font-size:13px;">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<h4 style="font-size:15px">Today's Statistics :</h4>
		</div>
	</div>
	
	<div class="row" style="font-size:13px;">
		  
		<a href="report/index.php?type=TODAY_TRANS" target="_blank">  
		<div class="col-lg-3 col-xs-6">
			  <div class="small-box bg-green">
				<div class="inner">
				  <h4 style="font-size:14px;"><?php echo $row_details2['today_transactions_amt']; ?></h4>
				  <p style="font-size:14px;">Transactions: <?php echo $row_details2['today_transactions']; ?></p>
				</div>
			 </div>
		 </div> 
		 </a>
	 
		<a href="report/index.php?type=WALLET_LOAD" target="_blank">
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h4 style="font-size:14px;"><?php echo $row_details2['today_load_amt']; ?></h4>
			<p style="font-size:14px;">Wallet Load: <?php echo $row_details2['today_load']; ?></p>
            </div>
          </div>
        </div>
        </a>
		
		 <a href="report/index.php?type=WALLET_WDL" target="_blank">
		<div class="col-lg-3 col-xs-6">
          <div class="small-box bg-yellow">
            <div class="inner">
              <h4 style="font-size:14px;"><?php echo $row_details2['today_withdraw_amt']; ?></h4>

              <p style="font-size:14px;">Wallet Withdraw: <?php echo $row_details2['today_withdraw']; ?></p>
            </div>
          </div>
        </div>
		</a>
        
		<a href="report/index.php?type=COMPANY_WALLET" target="_blank">
		<div class="col-lg-3 col-xs-6">
          <div class="small-box bg-red">
            <div class="inner">
              <h4 style="font-size:14px;"><?php echo $row_details2['company_load_amt']; ?></h4>

              <p style="font-size:14px;">Company Wallet Load: <?php echo $row_details2['company_load']; ?></p>
            </div>
          </div>
        </div>
		</a>
		
	</div>
	
	
	<div class="row" style="font-size:13px;">
		  
		 <a href="report/index.php?type=ATM_EXP" target="_blank"> 
		<div class="col-lg-3 col-xs-6">
			  <div class="small-box bg-green">
				<div class="inner">
				  <h4 style="font-size:14px;"><?php echo $row_details2['atm_exp_amt']; ?></h4>
				  <p style="font-size:14px;">ATM Expenses: <?php echo $row_details2['atm_exp']; ?></p>
				</div>
			  </div>
		 </div> 
		</a>
	 
	 <a href="report/index.php?type=FEE_EXP" target="_blank">
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h4 style="font-size:14px;"><?php echo $row_details2['atm_fee_amt']; ?></h4>
			<p style="font-size:14px;">ATM Fee Expenses: <?php echo $row_details2['atm_fee']; ?></p>
            </div>
          </div>
        </div>
        </a>
		
		 <a href="report/index.php?type=CARD_TRANS" target="_blank">
		<div class="col-lg-3 col-xs-6">
          <div class="small-box bg-yellow">
            <div class="inner">
              <h4 style="font-size:14px;"><?php echo $row_details2['card_txn_amt']; ?></h4>

              <p style="font-size:14px;">Card Transaction: <?php echo $row_details2['card_txn']; ?></p>
            </div>
          </div>
        </div>
        </a>
		
		<a href="report/index.php?type=REVERSAL" target="_blank">
		<div class="col-lg-3 col-xs-6">
          <div class="small-box bg-red">
            <div class="inner">
              <h4 style="font-size:14px;"><?php echo $row_details2['reversal_amt']; ?></h4>

              <p style="font-size:14px;">Reversal: <?php echo $row_details2['reversal']; ?></p>
            </div>
          </div>
        </div>
		</a>
		
	</div>
	
</section>
 </div>

<?php
include "footer.php";
?>