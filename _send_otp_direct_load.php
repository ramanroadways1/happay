<?php
require_once("./connect.php");

$tno = escapeString($conn,$_POST['tno']);
$amount = escapeString($conn,$_POST['amount']);
$id = escapeString($conn,$_POST['id']);

if($tno=='')
{
	echo "<script>
		alert('vehicle number not found !');
		$('#otp_btn_$id').attr('disabled',false);
		$('#button1$id').attr('disabled',true);
		$('#amount$id').attr('readonly',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($amount=='' || $amount<=0)
{
	echo "<script>
		alert('amount is not valid !');
		$('#otp_btn_$id').attr('disabled',false);
		$('#button1$id').attr('disabled',true);
		$('#amount$id').attr('readonly',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$otp = '1234567890';
$otp = substr(str_shuffle($otp),0,6);

// SendMsgCustom(9024281599,"Vehicle_no: $tno, Load card direct from pool balance: $amount.\nOTP : $otp");
// MsgHappayDirectLoadFromPoolBalance($tno,$amount,$otp);
$msg_template="Vehicle_no: $tno, Load card direct from pool balance: $amount.\nOTP : $otp.\nRamanRoadways.";

SendWAMsg($conn,9024281599,$msg_template);

$_SESSION['session_otp'] = $otp;

	echo "
	 <script>
		alert('OTP Sent !');
		$('#loadicon').hide();
		$('#otp_btn_$id').attr('disabled',true);
		$('#button1$id').attr('disabled',false);
		$('#amount$id').attr('readonly',true);
	</script>";
	exit();
?>