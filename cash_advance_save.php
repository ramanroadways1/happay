<?php
require_once("./connect.php");

$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

$tno = escapeString($conn,$_POST['tno']);
$company = escapeString($conn,$_POST['company']);
$branch = escapeString($conn,$_POST['branch']);
$amount = escapeString($conn,$_POST['amount']);
$trans_date = escapeString($conn,$_POST['trans_date']);
$narration = escapeString($conn,$_POST['narration']);

if(!isset($_POST['otp']))
{
	echo "<script>
		alert('OTP not found !');
		$('#loadicon').hide();
		$('#button1').attr('disabled',false);
	</script>";
	exit();
}

if(!isset($_POST['trip_id']))
{
	echo "<script>
		alert('Trip not found !');
		$('#loadicon').hide();
		$('#button1').attr('disabled',false);
	</script>";
	exit();
}

$otp = escapeString($conn,$_POST['otp']);
$trip_id = escapeString($conn,$_POST['trip_id']);

if($trip_id=="")
{
	echo "<script>
		alert('Trip not found !');
		$('#loadicon').hide();
		$('#button1').attr('disabled',false);
	</script>";
	exit();
}

if($tno=="" || $company=="" || $branch=="" || $amount=="" || $trans_date=="" || $narration=="" || $otp=="")
{
	echo "<script>
		alert('Enter Vehicle No, Trans Date, Branch, Amount, Transaction Date, Narration and OTP First !');
		$('#loadicon').hide();
		$('#button1').attr('disabled',false);
	</script>";
	exit();
}

if($amount<=0)
{
	echo "<script>
		alert('Invalid Amount !');
		$('#loadicon').hide();
		$('#button1').attr('disabled',false);
	</script>";
	exit();
}

$get_trip_details = Qry($conn,"SELECT tno,driver_code,trip_no FROM dairy.trip WHERE id='$trip_id'");

if(!$get_trip_details){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request.","./");
	exit();
}

if(numRows($get_trip_details)==0)
{
	Redirect("Trip not found.","./");
	exit();
}

$row_trip = fetchArray($get_trip_details);

if($row_trip['tno']!=$tno)
{
	echo "<script>
		alert('Invalid vehicle Number. Entered: $tno and trip vehicle no is : $row_trip[tno] !');
		$('#loadicon').hide();
		$('#button1').attr('disabled',false);
	</script>";
	exit();
}

$driver_code = $row_trip['driver_code'];
$trip_no = $row_trip['trip_no'];

if($driver_code==0 || $driver_code=="")
{
	echo "<script>
		alert('Driver not found !');
		$('#loadicon').hide();
		$('#button1').attr('disabled',false);
	</script>";
	exit();
}

$get_DriverName = Qry($conn,"SELECT name,mobile,mobile2 FROM dairy.driver WHERE code='$driver_code'");

if(!$get_DriverName){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request.","./");
	exit();
}

if(numRows($get_DriverName)==0)
{
	Redirect("Driver not found.","./");
	exit();
}

$row_driver = fetchArray($get_DriverName);

$driver_name = $row_driver['name'];
$driver_mobile = $row_driver['mobile'];
$driver_mobile2 = $row_driver['mobile2'];

$nrr = $tno."/".$driver_name." : ".$narration;

// $url="https://www.smsschool.in/api/verifyRequestOTP.php";
// $postData = array(
    // 'authkey' => "242484ARRP024r65bc082c9",
    // 'mobile' => 9024281599,
    // 'otp' => $otp
// );

// $ch = curl_init();
// curl_setopt_array($ch, array(
// CURLOPT_URL => $url,
// CURLOPT_RETURNTRANSFER => true,
// CURLOPT_POST => true,
// CURLOPT_POSTFIELDS => $postData
// ));

// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

// $output = curl_exec($ch);

// if(curl_errno($ch))
// {
	// echo "<script>
		// alert('API Failure !');
		// $('#loadicon').hide();
		// $('#button1').attr('disabled',true);
	// </script>";
	// exit();	
// }

// $result = json_decode($output, true);
// curl_close($ch);

// if($result['type']=='error')
// {
	// echo "<script>
			// alert('Error : $result[message].');
			// $('#loadicon').hide();
			// $('#button1').attr('disabled',false);
		// </script>";
	// exit();	
// }
// else
// {
	// if($result['message']!='otp_verified')
	// {
		// echo "<script>
			// alert('Error : $result[message].');
			// $('#loadicon').hide();
			// $('#button1').attr('disabled',false);
		// </script>";
		// exit();	
	// }
// }

VerifyOTP($otp,"button1");

// if($otp!=$_SESSION['session_otp'])
	// {
		// echo "<script>
				// alert('Error : Invalid OTP entered.');
				// $('#button1').attr('disabled',false);
				// $('#loadicon').hide();
		// </script>";
		// exit();	
	// }

	$check_bal=Qry($conn,"SELECT balance,balance2 FROM user WHERE username='$branch'");

	if(!$check_bal)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while Processing Request.","./");
		exit();
	}

	if(numRows($check_bal)==0)
	{
		Redirect("Branch Cash not found. You can not pay Cash.","./");
		exit();
	}

	$row_bal=fetchArray($check_bal);

	if($company=='RRPL' && $row_bal['balance']>=$amount)
	{
		$newbal=$row_bal['balance'] - $amount;
		$debit='debit';			
		$balance='balance';
	}
	else if($company=='RAMAN_ROADWAYS' && $row_bal['balance2']>=$amount)
	{
		$newbal=$row_bal['balance2'] - $amount;
		$debit='debit2';			
		$balance='balance2';
	}
	else
	{
		Redirect("INSUFFICIENT Balance in Company : $company.","./");
		exit();
	}
	
	$trans_type="CASH";
	
	$get_vou_id=GetVouId("T",$conn,"mk_tdv","tdvid",$branch);
	if(!$get_vou_id || $get_vou_id=="0" || $get_vou_id=="")
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error While Fetching Vou Id.","./");
		exit();
	}
	
	$tids=$get_vou_id;
	
	$trans_id_Qry=GetTransId($trans_type,$conn,"cash");
	if(!$trans_id_Qry || $trans_id_Qry=="" || $trans_id_Qry=="0")
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error While Fetching Transaction Id.","./");
		exit();
	}
	
	$trans_id=$trans_id_Qry;
	
StartCommit($conn);
$flag = true;

$insert_cash_diary=Qry($conn,"INSERT INTO dairy.cash (trip_id,trans_id,trans_type,tno,vou_no,amount,date,narration,branch,timestamp) 
VALUES ('$trip_id','$trans_id','$trans_type','$tno','$tids','$amount','$trans_date','$narration','$branch','$timestamp')");

if(!$insert_cash_diary)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}		

$query_update = Qry($conn,"update user set `$balance`='$newbal' where username='$branch'");

if(!$query_update)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$update_cashbook=Qry($conn,"INSERT INTO cashbook(user,date,vou_date,comp,vou_no,vou_type,desct,`$debit`,`$balance`,
timestamp) VALUES ('$branch','".date("Y-m-d")."','$trans_date','$company','$tids','Truck_Voucher','$nrr','$amount',
'$newbal','$timestamp')");

if(!$update_cashbook)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
		
		
$insert_vou=Qry($conn,"INSERT INTO mk_tdv(user,company,tdvid,date,newdate,truckno,dname,amt,mode,dest,timestamp) 
VALUES ('$branch','$company','$tids','".date("Y-m-d")."','$trans_date','$tno','$driver_name','$amount','$trans_type',
'$narration','$timestamp')");	
		
if(!$insert_vou)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$update_trip=Qry($conn,"UPDATE dairy.trip SET cash=cash+$amount WHERE id='$trip_id'");
if(!$update_trip)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$select_amount=Qry($conn,"SELECT id,amount_hold FROM dairy.driver_up WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");
if(!$select_amount)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$row_amount=fetchArray($select_amount);
$hold_amt=$row_amount['amount_hold']+$amount;
$driver_balance_id=$row_amount['id'];

$insert_book=Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_id,trip_no,trans_id,desct,credit,balance,date,branch,
timestamp) VALUES ('$driver_code','$tno','$trip_id','$trip_no','$trans_id','CASH_ADV','$amount','$hold_amt','$date','$branch',
'$timestamp')");

if(!$insert_book)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_hold_amount=Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold+$amount WHERE id='$driver_balance_id'");

if(!$update_hold_amount)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	if($trans_type=='HAPPAY'){
		$msg_text = "आपके एटीएम कार्ड में $amount रुपए का रिचार्ज $branch ब्रांच द्वारा  दिनांक  $trans_date को किया गया हैं";
	}
	else{
		$msg_text = "आपको $branch  ब्रांच ने दिनांक  $trans_date  को  $amount  रुपए नकद दिए हैं";
	}
	
	if($driver_mobile2!='')
	{
		SendTransSms($driver_mobile2,$tno.": ".$msg_text);
	}	
	
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Voucher created successfully !!');
		window.location.href='./cash_advance.php';
		$('#loadicon').hide();
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	errorLog("COMMIT NOT COMPLETED. Timestamp : $timestamp. Truck No: $tno, TripNo: $trip_no.",$conn,$page_name,__LINE__);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./cash_advance.php");
	exit();
}		
?>