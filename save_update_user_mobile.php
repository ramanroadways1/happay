<?php
require_once("connect.php");

$requestId=date("Y:m:d_H:i:s");

$timestamp = date("Y-m-d H:i:s");
$u_id = escapeString($conn,strtoupper($_POST['id']));
$company = escapeString($conn,strtoupper($_POST['company']));
$veh_no = escapeString($conn,strtoupper($_POST['veh_no']));
$mobile_ext = escapeString($conn,strtoupper($_POST['mobile_ext']));
$mobile_update = escapeString($conn,strtoupper($_POST['mobile_update']));

if($mobile_ext==$mobile_update)
{
	echo "<script>
		alert('Nothing to update !');
		$('#submit_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if(strlen($mobile_update)!=10)
{
	echo "<script>
		alert('Invalid mobile number !');
		$('#submit_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}


$verify_user = Qry($conn,"SELECT veh_no,company FROM dairy.happay_users WHERE id='$u_id'");
if(!$verify_user){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error !","./update_user_details.php");
	exit();
}

if(numRows($verify_user)==0)
{
	echo "<script>
		alert('Error : user not found.');
		window.location.href='./update_user_details.php';
	</script>";
	exit();
}

$row_verify = fetchArray($verify_user);

if($row_verify['veh_no']!=$veh_no)
{
	echo "<script>
		alert('Error : vehicle number not verified.');
		window.location.href='./update_user_details.php';
	</script>";
	exit();
}

$requestId = "UPDATE1234";

$json_update = array(
"requestId"=>$requestId,
"userId"=>$veh_no,
"mobileNo"=>$mobile_update
);

$json_update = json_encode($json_update);

$result_update = HappayAPI("auth/v1/updateuser/",$json_update,$row_verify['company']);

$result_decode = json_decode($result_update, true);

	if(strpos($result_update,"Unauthorized") !== false)
	{ 
		echo "<font color='red'><b>Error :</b> ".$result_update."</font>";
		echo "<script>
			$('#loadicon').hide();
		</script>";
		exit();
	} 
	
	if(!empty($result_decode['error']['message']))
	{
		echo "<font color='red'><b>Error :</b> ".$result_decode['error']['message']."</font><br><br>";
		echo "<script>
			$('#loadicon').hide();
			$('#submit_button').attr('disabled',false);
		</script>";
		exit();
	}
	
	echo "<font color='green'><b>SUCCESS :</b> ".$result_decode['res_str']."</font><br><br>";
		
	$update_user = Qry($conn,"UPDATE dairy.happay_users SET mobile='$mobile_update' WHERE id='$u_id'");	
	
	if(!$update_user){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error !","./update_user_details.php");
		exit();
	}
	
	$log_insert=Qry($conn,"INSERT INTO dairy.happay_log(req_id,tno,result,api_response,status,timestamp) VALUES 
		('$requestId','$u_id','$result_decode[res_str]','$result_update','1','$timestamp')");
	
	if(!$log_insert){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error !","./update_user_details.php");
		exit();
	}
	
	$update_log=Qry($conn,"INSERT INTO dairy.happay_edit_log(card_no,log_type,response,timestamp) VALUES 
		('$veh_no','UPDATE_USER','Mobile Updated $mobile_ext to $mobile_update.','$timestamp')");
	
	if(!$update_log){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error !","./update_user_details.php");
		exit();
	}
	
		echo "<script>
			alert('SUCCESS : $result_decode[res_str] !');
			document.getElementById('UserForm').reset();
			document.getElementById('modal_hide').click(); 
			$('#loadicon').hide();
		</script>";
	exit();

?>