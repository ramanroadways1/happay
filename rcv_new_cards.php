<?php
include "header.php";

$JsonString_RRPL = array("requestId"=>mt_rand(),"start"=>"0","count"=>"100");
$JsonString_RR = array("requestId"=>mt_rand(),"start"=>"0","count"=>"100");

$JsonString_RRPL = json_encode($JsonString_RRPL);
$JsonString_RR = json_encode($JsonString_RR);

$result_RRPL = HappayAPI("auth/v1/cards/get_available_cards/",$JsonString_RRPL,"RRPL");
$result_RR = HappayAPI("auth/v1/cards/get_available_cards/",$JsonString_RR,"RAMAN_ROADWAYS");
	
$result_RRPL = json_decode($result_RRPL, true);
$result_RR = json_decode($result_RR, true);
?>

<script type="text/javascript">
	$(function() {
		$("#tno").autocomplete({
		source: 'autofill/get_tno.php',
		select: function (event, ui) { 
              $('#tno').val(ui.item.value);   
              $('#company').val(ui.item.company);   
             $("#button1").attr('disabled',false);				   
             return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#tno").val('');
			$("#company").val('');
			$("#tno").focus();
			$("#button1").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
		});});
</script> 

<script type="text/javascript">
$(document).ready(function (e) {
$("#NewCardForm").on('submit',(function(e) {
$("#loadicon").show();
$("#button1").attr("disabled", true); 
e.preventDefault();
	$.ajax({
	url: "./rcv_new_cards_save.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_cards").html(data);
	},
	error: function() 
	{} });}));});
</script>

<div class="content-wrapper">
    <section class="content-header">
      <h4 style="font-size:15px">
		Rcv new Cards :
      </h4>
	  
	  <style>
		.form-control{text-transform:uppercase;}
	  </style>
	
	
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div class="row">
 
 <div class="form-group col-md-12">
    
	<div class="form-group col-md-12" id="result_cards"></div>
	<div class="form-group col-md-12">
	
	<form autocomplete="off" id="NewCardForm">
		<div class="row">
		
		<script>
		function KitId(id)
		{
			var res = id.split("_");
			$('#tno').val(res[0]);
			$('#kit_id2').val(res[1]);
			$('#company').val(res[2]);
		}
		</script>
		
		<div class="form-group col-md-3">
            <label>Kit Id : <font color="red">*</font></label>
            <select onchange="KitId(this.value)" name="kit_id" data-live-search="true" data-live-search-style=""  data-size="5"  class="form-control selectpicker">
			<option value="">--Select Vehicle--</option>
			<?php 
			foreach($result_RRPL['res_data']['card_list'] as $rrpl_kits)
			{
				$veh_no1 = str_replace(" RRPL","",$rrpl_kits["embossing_name"]);
				?>
				<option value="<?php echo $veh_no1."_$rrpl_kits[card_kit_id]_RRPL"; ?>"><?php echo $rrpl_kits["card_kit_id"]." ($veh_no1)";?></option>
				<?php
			}
			
			foreach($result_RR['res_data']['card_list'] as $rr_kits)
			{
				$veh_no1 = str_replace(" RAMAN_ROADWAYS","",$rr_kits["embossing_name"]);
				?>
				<option value="<?php echo $veh_no1."_$rr_kits[card_kit_id]_RAMAN_ROADWAYS"; ?>"><?php echo $rr_kits["card_kit_id"]." ($veh_no1)";?></option>
				<?php
				// echo "<option value='$veh_no1_$rr_kits[card_kit_id]_RAMAN_ROADWAYS'>$rr_kits[card_kit_id] ($rr_kits[embossing_name])</option>";
			}
			?>
			</select>
        </div>
				 
		<div class="form-group col-md-3">
			<label>Vehicle Number <font color="red"><sup>*</sup></font></label>
			<input readonly name="tno" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" class="form-control" required="required" id="tno">
		</div>
		
		<div class="form-group col-md-3">
			<label>Card type <font color="red"><sup>*</sup></font></label>
			<select name="card_type" class="form-control" required="required">
				<option value="">--select option--</option>
				<option value="REPLACEMENT">REPLACEMENT</option>
				<option value="NEW">NEW</option>
			</select>
		</div>
		
		<input type="hidden" id="company" name="company">
		<input type="hidden" id="kit_id2" name="kit_id">
		
		<div class="form-group col-md-3">
			<label>&nbsp;</label>
			<br />
			<button id="button1" type="submit" class="btn btn-sm btn-primary">Rcv Card</button>
		</div>
	</div>
	
	</form>
		
    </div>
	
	<div class="form-group col-md-12 table-responsive">
	<div id="result"></div>
	  <table id="example" class="table table-bordered table-striped" style="font-size:12px;">
		<thead>		
		 <tr>
				<th>#</th>
				<th>Vehicle Number</th>
				<th>Kit Id</th>
				<th>Company</th>
				<th>New/Replacement</th>
				<th>Rcvd on</th>
				<th>##</th>
			</tr>
          </thead>
		 <tbody> 
            <?php
              $sql = Qry($conn,"SELECT id,veh_no,kit_id,company,card_type,added_timestamp FROM dairy.happay_new_cards WHERE 
			  added!='1' ORDER BY id ASC");
              
			  if(!$sql){
				echo getMySQLError($conn);
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
				exit();
			}

			  
			  if(numRows($sql)==0)
			  {
				echo "<tr><td colspan='7'><b>NO RESULT FOUND..</b></td></tr>";  
			  }
			 else
			 {
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
				echo "<tr>
					<td>$sn</td>
					<td>$row[veh_no]</td>
					<td>$row[kit_id]</td>
					<td>$row[company]</td>
					<td>$row[card_type]</td>
					<td>$row[added_timestamp]</td>
					<td><button type='button' class='btn btn-success btn-xs' id='RcvCardBtn$row[id]' onclick=RcvCard('$row[id]')>Add to inventory</button></td>
				</tr>"; 
				$sn++;		
              }
			}
            ?>
		</tbody>	
        </table>
      </div>
	  <div id="pin_result"></div>
    </div>
  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>

<script>	
function RcvCard(id)
{
	$('#RcvCardBtn'+id).attr('disabled',true);
	$("#loadicon").show();
	jQuery.ajax({
		url: "rcv_new_card_move_inventory.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
		$("#result_cards").html(data);
		},
		error: function() {}
	});
}

$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

<?php
include "footer.php";
?>