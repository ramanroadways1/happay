<?php
require_once("./connect.php");

$requestId2 = mt_rand();

$JsonString = array(
"requestId"=>$requestId2,
"start"=>"0",
"count"=>"100"
);

$JsonString = json_encode($JsonString);

$result1 = HappayAPI("auth/v1/cards/get_available_cards/",$JsonString,"RRPL");
	
$result2 = json_decode($result1, true);

	if(strpos($result1,"Unauthorized") !== false)
	{ 
		closeConnection($conn);
		echo "<font color='red'><b>Error :</b> ".$result1."</font>
		<script>
			$('#loadicon').hide();
		</script>";
		exit();
	} 
	
if(@$result2['error'])
{
	closeConnection($conn);
	
	echo "<font color='red'><b>Error :</b> ".$result2['error']['message']."</font><br><br>";	
	echo "<script>
		$('#loadicon').hide();
	</script>";
	exit();
}

foreach($result2['res_data']['card_list'] as $user_card_result)
	{
		$tno1 = $user_card_result['embossing_name'];
		$kit_id1 = $user_card_result['card_kit_id'];
		
		echo $tno1." - ".$kit_id1."<br>";
	}
