<?php
include "header.php";
exit();
?>

<div class="content-wrapper">
    <section class="content-header">
      <h4 style="font-family:Verdana">
		Assignable Cards :
      </h4>
	  
	  <style>
	  label{font-family:Verdana;font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div id="tab_result"></div>
	
<?php
$data = array(
"requestId"=>"12345ABC",
"start"=>0,
"count"=>500
);
$JsonString = json_encode($data);
echo HappayAPI("auth/v1/cards/get_assignable_cards/",$JsonString,"RRPL");
?>	

<div class="row">
 
 <div class="form-group col-md-12">
    
	<div class="form-group col-md-12 table-responsive">
	 <br />
        <table id="myTable" class="table table-bordered table-striped" style="font-family:Verdana;font-size:12px;">
          <tr>
				<th>#id</th>
				<th>Card<br>Name</th>
				<th>Last<br>Name</th>
				<th>Mobile</th>
				<th>DOB</th>
				<th>Password</th>
				<th>#</th>
			</tr>
          
            <?php
              $sql = Qry($conn,"SELECT id,f_name,l_name,company,mobile,dob,userId,password FROM dairy.happay_users WHERE card_id='0'");
              
			 if(!$sql){
				echo getMySQLError($conn);
				errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
				exit();
			}
			  
			  if(numRows($sql)==0)
			  {
				echo "<tr>
						<td colspan='12'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			  }
			 else
			 {
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
				echo 
                "<tr>
					<td>$sn</td>
					<td>$row[f_name]</td>
					<td>$row[l_name]</td>
					<td>$row[mobile]</td>
					<td>".date("d/m/y",strtotime($row['dob']))."</td>				 
					<td>$row[password]</td>
					<td>
						
					</td>
				</tr>
				";
				$sn++;		
              }
			}
            ?>
        </table>
      </div>
    </div>
  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>

<button type="button" id="ModalButton" style="display:none" class="btn btn-primary" data-toggle="modal" data-target="#myModal"></button>

<div class="modal" id="myModal" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title">Assign Card to : <span style="color:blue" id="customer_name"></span></h4>
      </div>
	<form id="AssignForm" autocomplete="off">
      <div class="modal-body">
		<div class="row">
			<div class="form-group col-md-12" id="card_kit_data">
				<label>Available Cards : <font color="red">*</font></label>
				<select name="kit_id" class="form-control" required>
					<option value="">--Select Card--</option>
				</select>
			</div>
		</div>
			<input type="hidden" id="id_box" name="id">	
			<input type="hidden" id="userid_box" name="userid">	
			<input type="hidden" id="company_modal" name="company">	
      </div>

      <div class="modal-footer">
        <button type="submit" id="submit_button" class="btn btn-primary">Assign Card</button>
        <button type="button" onclick="MyFunc1($('#id_box').val())" id="close_modal_button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="button" id="modal_hide" data-dismiss="modal" style="display:none">das</button>
      </div>
	</form>
    </div>
  </div>
</div>	

<script>	
function AssignCard(id,userid)
{
	$('#Assign'+id).attr('disabled',true);
	var customer_name = $('#customer_name'+id).val();
	var company = $('#company'+id).val();
	
	$("#loadicon").show();
	jQuery.ajax({
		url: "fetch_free_cards_for_assign.php",
		data: 'customer_name=' + customer_name + '&id=' + id + '&userid=' + userid + '&company=' + company,
		type: "POST",
		success: function(data) {
		$("#card_kit_data").html(data);
		},
		error: function() {}
	});
}

function MyFunc1(id){
	$('#Assign'+id).attr('disabled',false);
}
</script>

<?php
include "footer.php";
?>