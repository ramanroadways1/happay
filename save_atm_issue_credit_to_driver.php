<?php
require_once("connect.php");

$requestId=date("Y:m:d_H:i:s");
$date=date("Y-m-d");
$timestamp=date("Y-m-d H:i:s");

$tno = escapeString($conn,$_POST['tno']);
$company = escapeString($conn,$_POST['company']);
$amount = escapeString($conn,$_POST['amount']);
$narration = escapeString($conn,$_POST['narration']);
$otp = escapeString($conn,$_POST['otp']);
$trip_id = escapeString($conn,$_POST['trip_id']);

if($trip_id=="")
{
	echo "<script>
		alert('Trip not found !');
		$('#loadicon').hide();
		$('#button1').attr('disabled',false);
	</scrit>";
	exit();
}

if($tno=="" || $company=="" || $amount=="" || $narration=="" || $otp=="")
{
	echo "<script>
		alert('Enter Vehicle No, Amount, Narration and OTP First !');
		$('#loadicon').hide();
		$('#button1').attr('disabled',false);
	</scrit>";
	exit();
}

if($amount<=0)
{
	echo "<script>
		alert('Invalid Amount !');
		$('#loadicon').hide();
		$('#button1').attr('disabled',false);
	</scrit>";
	exit();
}

$get_trip_details = Qry($conn,"SELECT tno,driver_code,trip_no FROM dairy.trip WHERE id='$trip_id'");

if(!$get_trip_details){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request.","./");
	exit();
}

if(numRows($get_trip_details)==0)
{
	Redirect("Trip not found.","./credit_atm_issue_driver.php");
	exit();
}

$row_trip = fetchArray($get_trip_details);

if($row_trip['tno']!=$tno)
{
	Redirect("Invalid vehicle Number.","./credit_atm_issue_driver.php");
	exit();
}

$driver_code = $row_trip['driver_code'];
$trip_no = $row_trip['trip_no'];

if($driver_code==0 || $driver_code=="")
{
	echo "<script>
		alert('Driver not found !');
		$('#loadicon').hide();
		$('#button1').attr('disabled',false);
	</scrit>";
	exit();
}

// $url="https://www.smsschool.in/api/verifyRequestOTP.php";
// $postData = array(
    // 'authkey' => "242484ARRP024r65bc082c9",
    // 'mobile' => 9024281599,
    // 'otp' => $otp
// );

// $ch = curl_init();
// curl_setopt_array($ch, array(
// CURLOPT_URL => $url,
// CURLOPT_RETURNTRANSFER => true,
// CURLOPT_POST => true,
// CURLOPT_POSTFIELDS => $postData
// ));

// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

// $output = curl_exec($ch);

// if(curl_errno($ch))
// {
	// echo "<script>
		// alert('API Failure !');
		// $('#loadicon').hide();
		// $('#button1').attr('disabled',true);
	// </script>";
	// exit();	
// }

// $result = json_decode($output, true);
// curl_close($ch);

// if($result['type']=='error')
// {
	// echo "<script>
			// alert('Error : $result[message].');
			// $('#loadicon').hide();
			// $('#button1').attr('disabled',false);
		// </script>";
	// exit();	
// }
// else
// {
	// if($result['message']!='otp_verified')
	// {
		// echo "<script>
			// alert('Error : $result[message].');
			// $('#loadicon').hide();
			// $('#button1').attr('disabled',false);
		// </script>";
		// exit();	
	// }
// }

VerifyOTP($otp,"button1");

	// if($otp=='')
	// {
		// echo "<script>
				// alert('Error : Please Enter OTP.');
				// $('#button1').attr('disabled',false);
				// $('#loadicon').hide();
			// </script>";
		// exit();	
	// }
	// else
	// {
		// if($otp!=$_SESSION['session_otp'])
		// {
			// echo "<script>
				// alert('Error : Invalid OTP entered.');
				// $('#button1').attr('disabled',false);
				// $('#loadicon').hide();
			// </script>";
			// exit();	
		// }
	// }

	$trans_id_Qry=GetTransIdAtmIssueCredit("HPATM_CR",$conn);
	
	if(!$trans_id_Qry || $trans_id_Qry=="" || $trans_id_Qry=="0")
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error While Fetching Transaction Id.","./credit_atm_issue_driver.php");
		exit();
	}
	
	$trans_id = $trans_id_Qry;

StartCommit($conn);
$flag = true;

$insert_Trans = Qry($conn,"INSERT INTO dairy.happay_atm_issue_credit (tno,driver_code,trip_no,trans_id,amount,narration,timestamp) VALUES 
('$tno','$driver_code','$trip_no','$trans_id','$amount','$narration','$timestamp')");

if(!$insert_Trans){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

// Driver Balance Update starts 

$select_amount=Qry($conn,"SELECT id,tno,amount_hold FROM dairy.driver_up WHERE down=0 AND code='$driver_code' ORDER BY id 
DESC LIMIT 1");

if(!$select_amount){	
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($select_amount)==0){
	$flag = false;
	errorLog("Error While Fetching Driver Balance. Code: $driver_code.",$conn,$page_name,__LINE__);
}

$row_amount=fetchArray($select_amount);
$hold_amt=$row_amount['amount_hold']-round($amount);
$driver_id=$row_amount['id'];

if($row_amount['tno']!=$tno)
{
	$flag = false;
	errorLog("Vehicle not verified. DbVeh: $row_amount[tno]. System : $tno.",$conn,$page_name,__LINE__);
}

$insert_book = Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_no,trans_id,desct,debit,balance,date,branch,
timestamp) VALUES ('$driver_code','$tno','$trip_no','$trans_id','HAPPAY_ATM_REVERSAL','".round($amount)."','$hold_amt','$date','HAPPAY_ADMIN',
'$timestamp')");

if(!$insert_book){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_hold_amount=Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold-'".round($amount)."' WHERE id='$driver_id'");

if(!$update_hold_amount){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

// Driver Balance Update ends 

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<font color='green'><b>SUCCESS : Amount Credit.</b></font>";	
	echo "<script>
		alert('Amount Credit Successfully.');
		$('#loadicon').hide();
	</script>"; 
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./credit_atm_issue_driver.php");
	exit();
}	
	
?>