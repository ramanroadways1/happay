<?php
include "header.php";
?>
<script type="text/javascript">
	$(function() {
		$("#tno").autocomplete({
		source: 'autofill/get_tno.php',
		select: function (event, ui) { 
              $('#tno').val(ui.item.value);   
              $('#company').val(ui.item.company);   
             return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#tno").val('');
			$("#company").val('');
			$("#tno").focus();
		}}, 
		focus: function (event, ui){
			return false;}
		});});
</script> 

<script type="text/javascript">
$(document).ready(function (e) {
	$("#Form1").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#button1").attr("disabled", true);
		$.ajax({
        	url: "./cash_advance_save.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#func_result_form1").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>

<div class="content-wrapper">
    <section class="content-header">
      <h4>
		Cash Advance over happay :
      </h4>
	  
	  <style>
		.form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
		</section>
		
   <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<form id="Form1" autocomplete="off" method="POST">

<div class="row">
 
 <div class="form-group col-md-12">

 <div id="func_result_form1" class="form-group col-md-12"></div>
    
	<div class="form-group col-md-12">
		<div class="row">
		<div class="form-group col-md-3">
			<label>Vehicle Number </label>
			<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" class="form-control" required="required" id="tno" name="tno">
		</div>
		
		<input id="company" name="company" type="hidden" required="required">
		
		<div class="form-group col-md-3">
			<label>Transaction Date <font color="red">*</font></label>
			<input name="trans_date" id="trans_date" type="date" max="<?php echo date("Y-m-d"); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" class="form-control" required="required">
		</div>
		
		<div class="form-group col-md-3">
			<label>Branch <font color="red">*</font></label>
			<select name="branch" id="branch_name" data-live-search="true" data-live-search-style="" class="form-control selectpicker" required>
					<option value="">--Select Card--</option>
					<?php
$get_branches = Qry($conn,"SELECT username FROM user WHERE role='2' AND username NOT in('HEAD','DUMMY')");
if(!$get_branches){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($get_branches)>0)
{
	while($row_branch=fetchArray($get_branches))
	{
		echo "<option value='$row_branch[username]'>$row_branch[username]</option>";
	}
}
			?>
			</select>
			</select>
		</div>
		
		<div class="form-group col-md-3">
			<label>Amount <font color="red">*</font></label>
			<input type="number" name="amount" id="amount" class="form-control" required="required">
		</div>
		
		<div class="form-group col-md-3">
			<label>Narration <font color="red">*</font></label>
			<textarea name="narration" id="narration" oninput="this.value=this.value.replace(/[^a-z A-Z0-9,-/]/,'')" class="form-control" required="required"></textarea>
		</div>
		
		<div class="form-group col-md-3">
			<label>Enter OTP <a href="#" onclick="SendOTP()">Send otp</a></label>
			<input readonly name="otp" oninput="this.value=this.value.replace(/[^0-9]/,'')" maxlength="6" type="text" class="form-control" required="required" id="otp">
		</div>
		
		<div class="form-group col-md-3" id="func_result">
		</div>
		
		<div class="form-group col-md-3">
			<label>&nbsp;</label>
			<br />
			<button disabled id="button1" type="submit" class="btn btn-primary">Submit</button>
		</div>

	</div>
		
    </div>
	
	</div>
  </div>
</div>

</form>

</body>
</html>

          </div>
          </div>
		  </div>
       </div>         
    </section>
	
<script>
function SendOTP()
{
	$('#button1').attr('disabled',true);
	$('#otp').attr('readonly',true);
	$('#otp').val('');
	
	var trans_date = $('#trans_date').val();
	var tno = $('#tno').val();
	var branch_name = $('#branch_name').val();
	var amount = $('#amount').val();
	
	if(trans_date!='' && tno!='' && branch_name!='' && amount!='')
	{
		if(amount>0)
		{
			$('#trans_date').attr('readonly',true);
			$('#tno').attr('readonly',true);
			$('#branch_name').attr('readonly',true);
			$('#amount').attr('readonly',true);
			
			$("#loadicon").show();
				jQuery.ajax({
					url: "_send_otp.php",
					data: 'tno=' + tno + '&trans_date=' + trans_date + '&branch_name=' + branch_name + '&amount=' + amount,
					type: "POST",
					success: function(data) {
						$("#func_result").html(data);
					},
				error: function() {}
			});
		}
		else
		{
			alert('invalid amount !');
		}
	}
	else
	{
		alert('Please enter vehicle number, transaction date, amount and branch !');
	}
}


// $(document).ready(function() {
    // $('#example').DataTable();
// } );
</script>	
<?php
include "footer.php";
?>