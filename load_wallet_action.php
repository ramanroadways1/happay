<?php
require_once("connect.php");
exit();
$requestId=date("Y:m:d_H:i:s");

$card_id = escapeString($conn,strtoupper($_POST['id']));
$card_no = escapeString($conn,strtoupper($_POST['card_no']));
$userid = escapeString($conn,strtoupper($_POST['mobile']));
$tno = escapeString($conn,strtoupper($_POST['tno']));
$balance = escapeString($conn,strtoupper($_POST['balance']));
$company = escapeString($conn,strtoupper($_POST['company']));
$amount = escapeString($conn,($_POST['amount']));

if($amount==0)
{
	echo "<script>
			alert('Error : amount is invalid.');
			window.location.href='./load_wallet.php';
		</script>";
	exit();
}

$chk_card_bal = Qry($conn,"SELECT id FROM happay_card WHERE id='$card_id' AND status='1' AND card_assigned='1'");
if(!$chk_card_bal)
{
	echo getMySQLError($conn);
}

if(numRows($chk_card_bal)==0)
{
		echo "<script>
			alert('Error : Something went wrong. Unable to validate data.');
			window.location.href='./load_wallet.php';
		</script>";
		exit();
}

$data = array(
"requestId"=>$requestId,
"card_kit_id"=>$card_no,
"load_amount"=>$amount,
"wallet_name"=>"Imprest"
);

$JsonString = json_encode($data);

$result = HappayAPI("auth/v1/cards/load_card_by_kit_id/",$JsonString,$company);

$result2 = json_decode($result, true);

	if(strpos($result,"Unauthorized") !== false)
	{ 
		echo "<font color='red'><b>Error :</b> ".$result."</font>";
		exit();
	} 
	
if(@$result2['error'])
{
	echo "<font color='red'><b>Error :</b> ".$result2['error']['message']."</font><br><br>";	
	
	echo "<script>
		$('#Button$card_id').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}
else
{
	$result_res=$result2['res_str'];			
	echo $result;
	echo "<script>
		alert('Card Successfully Assigned.');
		// document.getElementById('AssignForm').reset();
		// document.getElementById('modal_hide').click();
		// $('#Assign$u_id').attr('disabled',true);
		// $('#Assign$u_id').html('Card Assigned');
		// $('#Button$card_id').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
	
	$update_user_tab = Qry($conn,"UPDATE happay_users SET card_id='$kit_id',time_of_assign='$timestamp' WHERE id='$u_id'");
	$update_card_tab = Qry($conn,"UPDATE happay_card SET card_assigned='1' WHERE id='$kit_id'");
	
	$log_insert=Qry($conn,"INSERT INTO happay_log(req_id,card_no,card_holder,result,api_response,status,timestamp) VALUES 
		('$requestId','$card_no','$u_id','$result_res','$result','1','$timestamp')");
	
	if(!$log_insert)
	{
		echo getMySQLError($conn);
	}
	
	echo "<script>
		alert('Card Successfully Assigned.');
		document.getElementById('AssignForm').reset();
		document.getElementById('modal_hide').click();
		$('#Assign$u_id').attr('disabled',true);
		$('#Assign$u_id').html('Card Assigned');
		$('#Button$card_id').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}
?>