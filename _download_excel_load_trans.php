<?php
date_default_timezone_set('Asia/Kolkata');

include_once('../mail_lib/config.php');
include('../mail_lib/Classes/PHPExcel.php');

$from_date = mysqli_real_escape_string($conn,$_REQUEST['from_date']);
$to_date = mysqli_real_escape_string($conn,$_REQUEST['to_date']);
$tno = mysqli_real_escape_string($conn,$_REQUEST['tno']);

if($tno!="")
{
	$qry_tno=" AND t.card_using='$tno'";
}
else
{
	$qry_tno="";
}

$objPHPExcel = new PHPExcel();

$query = "SELECT t.card_no,o.comp as company,t.card_using,t.trans_id,t.trans_type,t.credit,t.branch,t.date,t.superv_entry,t.done,t.done_time,
t.timestamp,d.name as driver_name
FROM dairy.happay_card_transactions AS t 
LEFT OUTER JOIN dairy.driver AS d ON d.code=t.driver_code 
LEFT OUTER JOIN dairy.own_truck AS o ON o.tno=t.card_using 
WHERE t.date BETWEEN '$from_date' AND '$to_date'$qry_tno ORDER BY t.id ASC";

$result = mysqli_query($conn,$query);

if(!$result){
	echo mysqli_error($conn);
	exit();
}
// while($row=mysqli_fetch_array($result))
// {
	$sn=1;
		
// }
// exit();

$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Company');
$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Card_No');
$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Active_On');
$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Driver_Name');
$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Trans_id');
$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Date');
$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Amount');
$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Branch');
$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Entry_By');
$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Recharge_At');

$objPHPExcel->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);

$rowCount	=	2;

if(mysqli_num_rows($result) > 0)
{
while($row = mysqli_fetch_array($result))
{
	if($row['superv_entry']=="1")
		{
			$entry_by = "Supervisor";
		}
		else
		{
			$entry_by = "Branch";
		}
		
		if($row['done_time']==0)
		{
			$done_time = "Pending";
		}
		else
		{
			$done_time = $row['done_time'];
		}
	
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, mb_strtoupper($row['company'],'UTF-8'));
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, mb_strtoupper($row['card_using'],'UTF-8'));
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, mb_strtoupper($row['card_no'],'UTF-8'));
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, mb_strtoupper($row['driver_name'],'UTF-8'));
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, mb_strtoupper($row['trans_id'],'UTF-8'));
	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, mb_strtoupper($row['date'],'UTF-8'));
	$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, mb_strtoupper($row['credit'],'UTF-8'));
	$objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, mb_strtoupper($row['branch'],'UTF-8'));
	$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, mb_strtoupper($entry_by,'UTF-8'));
	$objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, mb_strtoupper($done_time,'UTF-8'));
	$rowCount++;
}

// START - Auto size columns for each worksheet
foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {

    $objPHPExcel->setActiveSheetIndex($objPHPExcel->getIndex($worksheet));

    $sheet = $objPHPExcel->getActiveSheet();
    $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
    $cellIterator->setIterateOnlyExistingCells(true);

    foreach ($cellIterator as $cell) {
        $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
    }
}
// END - Auto size columns for each worksheet

$objWriter	=	new PHPExcel_Writer_Excel2007($objPHPExcel);

	$filename = 'Happay_Load_Transactions.xlsx';
	 
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="'. $filename);
	header('Cache-Control: max-age=0');
	$objWriter->save('php://output');
	 // $objWriter->save('./'.$filename);
}
?>