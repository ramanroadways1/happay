<?php
require_once("./connect.php");

$menu_page_name = basename($_SERVER['PHP_SELF']);

$getReqPending = Qry($conn,"SELECT id FROM dairy.happay_card_transactions WHERE done!='1' AND trans_type='Wallet Credit'");

if(!$getReqPending){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$pendingReq = numRows($getReqPending);

$getCurrent_Bal = Qry($conn,"SELECT company,api_bal FROM dairy.happay_main_balance");
if(!$getCurrent_Bal){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

while($row_bal = fetchArray($getCurrent_Bal))
{
	if($row_bal['company']=='RRPL')
	{
		$rrpl_balance = $row_bal['api_bal'];
	}
	else
	{
		$rr_balance = $row_bal['api_bal'];	
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta name="robots" content="noindex,nofollow"/>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Happay-Card : Control Panel</title>
	<link rel="shortcut icon" type="image/png" href="happay_small.jpg"/>
	<link rel="shortcut icon" type="image/png" href="happay_small.png"/>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
  
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&display=swap" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css" />
<link href="google_font.css" rel="stylesheet">

<style>
@media screen and (min-width: 769px) {

    #logo_mobile { display: none; }
    #logo_desktop { display: block; }

}

@media screen and (max-width: 768px) {

    #logo_mobile { display: block; }
    #logo_desktop { display: none; }

}
</style>

</head>

<style type="text/css">
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
  position: fixed;
  left: 0px;
  top: 0px;
  width: 100%;
  height: 100%;
  z-index: 9999;
  background: url(load_icon.gif) center no-repeat #fff;
}

label{
	font-size:12px !important;
}

:-internal-autofill-previewed {
  font-size: 12px !important;
}

.ui-autocomplete { z-index:2147483647; font-size:13px !important;}

.ui-autocomplete { z-index:2147483647;font-family: Verdana,Arial,sans-serif; font-size:11px !important;}

.filter-option {
    font-size: 12px !important;
}

h4{
	font-size:15px !important;
}

select{
	font-size:12px !important;
}

select>option{
	font-size:12px !important;
}

.selectpicker{
	font-size:12px !important;
}

input[type="text"]{
    font-size:12px !important;
}

input[type="date"]{
   font-size:12px !important;
}
</style>

<script type="text/javascript">
  $(window).load(function() {
    $(".se-pre-con").fadeOut("slow");;
  });
</script>

<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);" class="hold-transition skin-blue sidebar-mini">
 
 <div class="se-pre-con"></div>
<div class="wrapper">

  <header class="main-header" style="font-size:12px;">

	<a href="./" class="logo" style="background:#FFF">
      <span class="logo-mini"><img src="happay_small.jpg" style="width:100%;height:50px" class="" /></span>
      <span class="logo-lg" id="logo_desktop"><img src="happay1.jpg" style="margin-top:5px;width:100%;height:40px" class="img-responsive" /></span>
      <span class="logo-lg" id="logo_mobile"><center><img src="happay1.jpg" style="margin-top:5px;width:50%;height:40px" class="img-responsive" /></center></span>
    </a>

    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
		
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="avtar2.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo  "HAPPAY ADMIN"; ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
                <img src="avtar2.png" class="img-circle" alt="User Image">
				<p>
                   <?php echo "HAPPAY ADMIN"; ?>
                </p>
              </li>
              <!-- Menu Body -->
            
              <!-- Menu Footer-->
              <li class="user-footer">
          
                <div class="pull-right">
                  <a href="logout.php" class="btn btn-default btn-flat">Log out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!--
		  <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
        </ul>
      </div>

    </nav>
  </header>
  
  <aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
       
      </div>
      
	  <ul style="font-size:12px !important;" class="sidebar-menu" data-widget="tree">          
             
 	<li class="<?php if($menu_page_name=="index.php") {echo "active";} ?>">
        <a href="./"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
     </li>
	 
	
	 <li class="<?php if($menu_page_name=="create_user.php") {echo "active";} ?>">
        <a href="./create_user.php"><i class="fa fa-user-plus"></i> <span>Create User</span></a>
     </li>
	 
	 <li class="<?php if($menu_page_name=="assign_card.php") {echo "active";} ?>">
        <a href="./assign_card.php"><i class="fa fa-address-card"></i> <span>Assign Card</span></a>
     </li>
	 
	 <li class="<?php if($menu_page_name=="get_all_cards.php") {echo "active";} ?>">
        <a href="./get_all_cards.php"><i class="fa fa-credit-card"></i> <span>Active Cards</span></a>
     </li>
	 
	 <li class="<?php if($menu_page_name=="rcv_new_cards.php") {echo "active";} ?>">
        <a href="./rcv_new_cards.php"><i class="fa fa-credit-card"></i> <span>Rcv New Cards</span></a>
     </li>
	 
	 <li class="<?php if($menu_page_name=="detached_cards.php") {echo "active";} ?>">
        <a href="./detached_cards.php"><i class="fa fa-address-card"></i> <span>Detached Cards</span></a>
     </li>
	 
	<!-- get_all_users
	 -->
	 
	 <li class="<?php if($menu_page_name=="recharge_req.php") {echo "active";} ?>">
        <a href="./recharge_req.php"><i class="fa fa-credit-card"></i> <span>Load Request <?php echo "<font color='yellow'> (".$pendingReq.")</font>"; ?></span></a>
     </li>
	 
	 
	 <li class="<?php if($menu_page_name=="load_requests.php") {echo "active";} ?>">
		<a href="./load_requests.php"> <i class="fa fa-download"></i> <span>Report : Load Requests</span> </a>
     </li>
	 
	 <!--
	 <li class="<?php if($menu_page_name=="free_cards.php") {echo "active";} ?>">
        <a href="./free_cards.php"><i class="fa fa-battery-empty"></i> <span>Free Cards</span></a>
     </li>
	 -->
	 
	 <li class="<?php if($menu_page_name=="card_transactions.php") {echo "active";} ?>">
		<a href="./card_transactions.php"> <i class="fa fa-list"></i> <span>Card Wise Transactions</span> </a>
     </li>	
	 
	 <li class="<?php if($menu_page_name=="org_load_wdl_txn.php") {echo "active";} ?>">
		<a href="./org_load_wdl_txn.php"> <i class="fa fa-list-alt"></i> <span>Org. Load/Wdl Txn</span> </a>
     </li>
	 
	 <li class="<?php if($menu_page_name=="all_transactions.php") {echo "active";} ?>">
		<a href="./all_transactions.php"> <i class="fa fa-list-alt"></i> <span>ALL Transactions</span> </a>
     </li>
	
	<!--	
	<li class="<?php if($menu_page_name=="user_info.php") {echo "active";} ?>">
		<a href="./user_info.php"> <i class="fa fa-info"></i> <span>Get User info</span> </a>
    </li>
	<li class="<?php if($menu_page_name=="search_card.php") {echo "active";} ?>">
		<a href="./search_card.php"> <i class="fa fa-search"></i> <span>Search Card</span> </a>
     </li>	
	<li class="<?php if($menu_page_name=="___load_wallet___.php") {echo "active";} ?>">
		<a href="./load_wallet.php"> <i class="fa fa-download"></i> <span>Load Wallet</span> </a>
     </li>
	 -->
	 <li class="<?php if($menu_page_name=="ediary_docs.php") {echo "active";} ?>">
		<a href="./ediary_docs.php"> <i class="fa fa-id-card-o"></i> <span>e-Diary Docs.</span> </a>
     </li>
	 
	 
	 <li class="<?php if($menu_page_name=="update_kyc.php") {echo "active";} ?>">
		<a href="./update_kyc.php"> <i class="fa fa-bank"></i> <span>Update KYC</span> </a>
     </li>
	
	 <li class="<?php if($menu_page_name=="update_user_details.php") {echo "active";} ?>">
		<a href="./update_user_details.php"> <i class="fa fa-user"></i> <span>Update User details</span> </a>
     </li>
	 
	<li class="<?php if($menu_page_name=="withdraw_wallet.php") {echo "active";} ?>">
		<a href="./withdraw_wallet.php"> <i class="fa fa-upload"></i> <span>Wdl. Wallet (driver)</span> </a>
    </li>
	
	<li class="<?php if($menu_page_name=="withdraw_wallet_direct_cr.php") {echo "active";} ?>">
		<a href="./withdraw_wallet_direct_cr.php"> <i class="fa fa-upload"></i> <span>Wdl. Wallet (direct)</span> </a>
    </li>
	
	<li class="<?php if($menu_page_name=="credit_atm_issue_driver.php") {echo "active";} ?>">
		<a href="./credit_atm_issue_driver.php"> <i class="fa fa-upload"></i> <span>ATM Issue Credit</span> </a>
    </li>
	
	<li class="<?php if($menu_page_name=="load_wallet_direct.php") {echo "active";} ?>">
		<a href="./load_wallet_direct.php"> <i class="fa fa-download"></i> <span>Load Wallet (direct)</span> </a>
     </li>
	
	<li class="<?php if($menu_page_name=="cash_advance.php") {echo "active";} ?>">
		<a href="./cash_advance.php"> <i class="fa fa-inr"></i> <span>Cash Advance Entry</span> </a>
    </li>
	
	<!--
	<li class="<?php if($menu_page_name=="withdraw_wallet2.php") {echo "active";} ?>">
		<a href="./withdraw_wallet2.php"> <i class="fa fa-upload"></i> <span>Withdraw Wallet - 2</span> </a>
    </li>
	
	
	<li class="<?php if($menu_page_name=="_fetch_api_transactions.php") {echo "active";} ?>">
		<a href="./_fetch_api_transactions.php"> <i class="fa fa-download"></i> <span>Load Transaction (API)</span> </a>
     </li>
	-->
	 <li class="<?php if($menu_page_name=="move_to_cash.php") {echo "active";} ?>">
		<a href="./move_to_cash.php"> <i class="fa fa-share"></i> <span>Move to Cash Entry</span> </a>
     </li>

	 
     <li>
		<a href="logout.php"><i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a>
     </li>
		
      </ul>
    </section>
  </aside>
  
  