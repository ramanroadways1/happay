<?php
include "header.php";
?>
<script type="text/javascript">
	$(function() {
		$("#tno").autocomplete({
		source: 'autofill/get_tno.php',
		select: function (event, ui) { 
              $('#tno').val(ui.item.value);   
             $("#button1").attr('disabled',false);				   
             return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#tno").val('');
			$("#tno").focus();
			$("#button1").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
		});});
</script> 

<div class="content-wrapper">
    <section class="content-header">
      <h4>
		Card Transactions :
      </h4>
	  
	  <style>
		.form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
		</section>
		
   <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div class="row">
 
 <div class="form-group col-md-12">
    <br />
	<div class="form-group col-md-12">
		<div class="row">
		<div class="form-group col-md-3">
			<label>Vehicle Number <font color="red">*</font></label>
			<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" class="form-control" required="required" id="tno">
		</div>
		
		<div class="form-group col-md-3">
			<label>From Date <font color="red">*</font></label>
			<input type="date" max="<?php echo date("Y-m-d"); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" class="form-control" required="required" id="from_date">
		</div>
		
		<div class="form-group col-md-3">
			<label>To Date <font color="red">*</font></label>
			<input type="date" max="<?php echo date("Y-m-d"); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" class="form-control" required="required" id="to_date">
		</div>
		
		<div class="form-group col-md-3">
			<label>&nbsp;</label>
			<br />
			<button id="button1" type="button" onclick="LoadTrans();" class="btn btn-sm btn-primary">Get Txns</button>
		</div>
	</div>
		
    </div>
	
	<div class="form-group col-md-12 table-responsive" id="func_result">
			
    </div>
    </div>
  </div>
</div>

</body>
</html>

          </div>
          </div>
		  </div>
       </div>         
    </section>
	
<script>
function LoadTrans()
{
	var tno = $('#tno').val();
	var from_date = $('#from_date').val();
	var to_date = $('#to_date').val();
	
	if(tno=="" || from_date=="" || to_date=="")
	{
		alert('Vehicle number or dates not found !');
	}
	else
	{
		$("#loadicon").show();
		jQuery.ajax({
			url: "_fetch_card_transactions.php",
			data: 'tno=' + tno + '&from_date=' + from_date + '&to_date=' + to_date,
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			   $('#example').DataTable({ 
                 "destroy": true, //use for reinitialize datatable
              });
			},
			error: function() {}
		});
	}
}

// $(document).ready(function() {
    // $('#example').DataTable();
// } );
</script>	
<?php
include "footer.php";
?>