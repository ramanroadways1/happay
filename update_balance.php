<?php
require_once("../_connect.php");

$get_first= Qry($conn,"SELECT id,card_no,credit,debit,balance FROM dairy.happay_webhook_live WHERE card_no IN(
SELECT card_no from dairy.first_trans) AND id NOT IN(SELECT id from dairy.first_trans) ORDER BY id ASC");

if(!$get_first){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "Error";
	exit();
}

while($row = fetchArray($get_first))
{
	$get_bal= Qry($conn,"SELECT balance FROM dairy.happay_live_balance WHERE veh_no='$row[card_no]'");

	if(!$get_bal){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "Error ".getMySQLError($conn);
		exit();
	}
	
	$row_bal = fetchArray($get_bal);
	
	if($row['credit']>0){
		$new_bal = $row['credit']+$row_bal['balance'];
	}
	else{
		$new_bal = $row_bal['balance']-$row['debit'];
	}
	
	$update_bal_record = Qry($conn,"UPDATE dairy.happay_webhook_live SET balance='$new_bal' WHERE id='$row[id]'");

	if(!$update_bal_record){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "Error ".getMySQLError($conn);
		exit();
	}
	
	$update_new_bal= Qry($conn,"UPDATE dairy.happay_live_balance SET balance='$new_bal' WHERE veh_no='$row[card_no]'");

	if(!$update_new_bal){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "Error ".getMySQLError($conn);
		exit();
	}
}
	
echo "ALL OK";	
?>