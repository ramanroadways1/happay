<?php
require_once("./connect.php");

// echo "<script>
		// alert('on hold till : 07:55 !');
		// $('#loadicon').hide();
	// </script>";
	// exit();
	
$id = escapeString($conn,$_POST['id']);
$tno = escapeString($conn,$_POST['tno']);
$amount = escapeString($conn,$_POST['amount']);
$trans_id = escapeString($conn,$_POST['trans_id']);

$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

$get_driver_info = Qry($conn,"SELECT c.card_using,c.branch,d.mobile,d.mobile2 
FROM dairy.happay_card_transactions as c 
LEFT OUTER JOIN dairy.driver as d ON d.code=c.driver_code
WHERE c.id='$id'");

if(!$get_driver_info){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	echo "<script>alert('Error !');$('#loadicon').hide();</script>";
	exit();
}

$row_driver = fetchArray($get_driver_info);

$driver_mobile1 = $row_driver['mobile'];
$driver_mobile2 = $row_driver['mobile2'];
$card_using = $row_driver['card_using'];
$adv_branch = $row_driver['branch'];

$chkStatus = Qry($conn,"SELECT company,card_status as status FROM dairy.happay_card_inventory WHERE veh_no='$tno'");
if(!$chkStatus){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($chkStatus)==0)
{
	echo "<script>
		alert('Card not found !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$rowCard = fetchArray($chkStatus);

if($rowCard['status']==0)
{
	echo "<script>
		alert('Card is disabled !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$req_id = date("Y-m-d_H:i:s");

$json_pyld = array(
"requestId"=>$req_id,
"userId"=>$tno,
"start_date"=>date("d-m-Y"),
"end_date"=>date("d-m-Y")
);

$json_for_get_txns = json_encode($json_pyld);

$result_get_Txn_Api = HappayAPI("auth/v1/cards/get_user_txns/",$json_for_get_txns,$rowCard['company']);
$result_decoded_txn_api = json_decode($result_get_Txn_Api, true);

if(strpos($result_get_Txn_Api,"Unauthorized") !== false)
{ 
	closeConnection($conn);
	echo "<font color='red'><b>Error :</b> ".$result_get_Txn_Api."</font>
	<script>$('#loadicon').hide();</script>";
	exit();
} 

$duplicate_recharge = false;
	
if(@$result_decoded_txn_api['error'])
{
	$error_msg = $result_decoded_txn_api['error']['message'];
	echo "<font color='red'><b>Error :</b> ".$error_msg."</font>";	
	
	$insertApiLog = Qry($conn,"INSERT INTO dairy.happay_api_log(req_id,tno,result,api_name,api_response,status,timestamp) VALUES 
	('$req_id','$tno','$error_msg','WALLET_LOAD','$result_get_Txn_Api','0','$timestamp')");
	
	if(!$insertApiLog){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	echo "<script>
		alert('API Error : $error_msg !');
		$('#loadicon').hide();
	</script>";
	exit();
}
else
{
	if($result_decoded_txn_api['res_data']['total_count']>0)
	{
		foreach($result_decoded_txn_api['res_data']['txn_list'] as $data)
		{
			$timeZone = substr($data['txn_date'],-5);
				
			if($timeZone=="00:00"){
				$txn_date1 = date('Y-m-d H:i:s',strtotime('+5 hour +30 minutes',strtotime(substr($data['txn_date'],0,19))));
				$txn_date2 = date("Y-m-d h:i A",strtotime(substr($txn_date1,0,19)));
			}
			else{
				$txn_date2 = date("Y-m-d h:i A",strtotime(substr($data['txn_date'],0,19)));		
			}
								
			$amount_recharge = $data['currency_amount'];
			
			if($amount==$amount_recharge AND $data['debit_or_credit_indicator']!='D')
			{
				$duplicate_recharge = true;
				$txn_date2 = $txn_date2;
			}
		}
	}
}

if($duplicate_recharge)
{
	echo "<font color='red'><b>Duplicate recharge.</b> Last Txn's Timestamp is : $txn_date2</font>
	<script>$('#loadicon').hide();</script>";
	exit();
}
			
// $chk_running_file = Qry($conn,"SELECT id FROM dairy.running_scripts");
// if(!$chk_running_file){
	// echo getMySQLError($conn);
	// errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	// exit();
// }

// if(numRows($chk_running_file)>0)
// {
	// echo "<script>
		// alert('Please wait while api transaction are loading !');
		// $('#loadicon').hide();
	// </script>";
	// exit();
// }

$Get_balance = Qry($conn,"SELECT api_bal FROM dairy.happay_main_balance WHERE company='$rowCard[company]'");
if(!$Get_balance){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($Get_balance)==0)
{
	echo "<script>
		alert('Balance not found !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$rowMain_Bal = fetchArray($Get_balance);

if($rowMain_Bal['api_bal']<$amount)
{
	echo "<script>
		alert('Insufficient balance in $rowCard[company]. Available balance is : $rowMain_Bal[api_bal] !');
		$('#loadicon').hide();
	</script>";
	exit();
}

function url_test( $url ) {
	$timeout = 10;
	$ch = curl_init();
	curl_setopt ( $ch, CURLOPT_URL, $url );
	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
	curl_setopt ( $ch, CURLOPT_TIMEOUT, $timeout );
	$http_respond = curl_exec($ch);
	$http_respond = trim( strip_tags( $http_respond ) );
	$http_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
	if ( ( $http_code == "200" ) || ( $http_code == "302" ) ) {
		return true;
	} else {
		// you can return $http_code here if necessary or wanted
		return false;
	}
	curl_close( $ch );
}

$website = "https://api-v2.happay.in";

// if(!url_test( $website ) )
// {
	// echo "<script>
		// alert('Happay\s server is down. Please try after some time !');
		// $('#loadicon').hide();
	// </script>";
	// exit();
// }

$req_id = date("Y-m-d_H:i:s");

$data_Json_Payload = array(
"requestId"=>$req_id,
"userId"=>$tno,
"load_amount"=>$amount,
"wallet_name"=>"Imprest"
);

$JsonString = json_encode($data_Json_Payload);

$result_Api = HappayAPI("auth/v1/cards/load_user_wallet/",$JsonString,$rowCard['company']);
$result_decoded = json_decode($result_Api, true);

if(strpos($result_Api,"Unauthorized") !== false)
{ 
	closeConnection($conn);
	echo "<font color='red'><b>Error :</b> ".$result_Api."</font>
	<script>$('#loadicon').hide();</script>";
	exit();
} 
	
if(@$result_decoded['error'])
{
	$error_msg = $result_decoded['error']['message'];
	echo "<font color='red'><b>Error :</b> ".$error_msg."</font>";	
	
	$insertApiLog = Qry($conn,"INSERT INTO dairy.happay_api_log(req_id,tno,result,api_name,api_response,status,timestamp) VALUES 
	('$req_id','$tno','$error_msg','WALLET_LOAD','$result_Api','0','$timestamp')");
	
	if(!$insertApiLog){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	echo "<script>
		alert('API Error : $error_msg !');
		$('#loadicon').hide();
	</script>";
	exit();
}
else
{
	if(empty($result_decoded['res_data']['txn_id']))
	{
		// echo $result_Api;
		$insertApiLog = Qry($conn,"INSERT INTO dairy.happay_api_log(req_id,tno,result,api_name,api_response,status,timestamp) VALUES 
		('$req_id','$tno','Txn_Id_Not_Rcvd','WALLET_LOAD','$result_Api','0','$timestamp')");
		
		if(!$insertApiLog){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	
		echo "<script>
			alert('Happay\'s server not responding. Please try after some time !');
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$Api_Response = $result_decoded['res_str'];	
	$txn_id = $result_decoded['res_data']['txn_id'];			
	
	// $txn_type = $result_decoded['res_data']['txn_type']['name'];
	// $txn_type_id = $result_decoded['res_data']['txn_type']['id'];
	// $txn_amount = $result_decoded['res_data']['currency_amount'];
	// $txn_date = $result_decoded['res_data']['txn_date'];
	// $diff = (strtotime($timestamp)-strtotime($txn_date)/3600);
	// if($diff>5){
		// $txn_date = date('Y-m-d H:i:s',strtotime('+5 hour +30 minutes',strtotime(substr($txn_date,0,19))));
	// }else{
		// $txn_date = $txn_date;
	// }
	// $merchant = $result_decoded['res_data']['payee_merchant'];
	// $txn_user = $result_decoded['res_data']['user']['name'];
	
	$insertApiLog = Qry($conn,"INSERT INTO dairy.happay_api_log(req_id,tno,result,api_name,api_response,status,timestamp) VALUES 
	('$req_id','$tno','$Api_Response','WALLET_LOAD','$result_Api','1','$timestamp')");
	
	if(!$insertApiLog){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
	
StartCommit($conn);
$flag = true;

$markReq = Qry($conn,"UPDATE dairy.happay_card_transactions SET txn_id='$txn_id',api_call='1',done='1',done_time='$timestamp' WHERE id='$id'");

if(!$markReq){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$updateMain_bal = Qry($conn,"UPDATE dairy.happay_main_balance SET api_bal=api_bal-'$amount' WHERE company='$rowCard[company]'");

if(!$updateMain_bal){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
} 

$updateTransDone = Qry($conn,"UPDATE dairy.happay_api_log SET completed='1' WHERE req_id='$req_id'");

if(!$updateTransDone){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	
	if(strlen($driver_mobile2)==10)
	{ 
		$adv_msg_mobile = $driver_mobile2;
	}
	else if(strlen($driver_mobile1)==10)
	{
		$adv_msg_mobile = $driver_mobile1;
	}
	
	$adv_msg_date = date("d/m/y",strtotime($date));
	
	if(strlen($adv_msg_mobile)==10)
	{
		$_SESSION['happay_adv_tno_for_msg'] = $card_using;
		MsgAdvDiary($adv_msg_mobile,$card_using,"HAPPAY",$amount,$adv_branch,$adv_msg_date);
	}
	
	echo "<font color='green'><b>SUCCESS :</b> ".$Api_Response."</font>";	
	echo "<script>
		alert('OK: Wallet Credited Successfully.');
		$('#Button1$id').html('Approved');
		$('#loadicon').hide();
	</script>"; 
	closeConnection($conn);
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./recharge_req.php");
	exit();
}
?>