<?php
require_once("./connect.php");

$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

$id = escapeString($conn,$_POST['id']);
$tno = trim(escapeString($conn,$_POST['veh_no']));

if(empty($id))
{
	echo "<script>
		alert('User-Id not found !');
		$('#loadicon').fadeOut('slow');
		$('#kyc_button$id').attr('disabled',false);
	</script>";
	exit();
}

$get_user = Qry($conn,"SELECT company,veh_no FROM dairy.happay_users WHERE id='$id'");

if(!$get_user){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error !');$('#loadicon').fadeOut('slow');$('#kyc_button$id').attr('disabled',false);</script>";
	exit();
}	

if(numRows($get_user)==0)
{
	echo "<script>
		alert('Use not found !');
		$('#loadicon').fadeOut('slow');
		$('#kyc_button$id').attr('disabled',false);
	</script>";
	exit();
}

$row_user = fetchArray($get_user);

$company = $row_user['company'];

if($row_user['veh_no']!=$tno)
{
	echo "<script>
		alert('Use not verified !');
		$('#loadicon').fadeOut('slow');
		$('#kyc_button$id').attr('disabled',false);
	</script>";
	exit();
}

$ReqID_Verify = date("Y-m-d_H:i:s");
	
$verify_Payload = array(
	"requestId"=>$ReqID_Verify,
	"userId"=>$tno
);

$verify_Payload = json_encode($verify_Payload);
$result_Api_Verify = HappayAPI("auth/v1/cards/init_kyc/",$verify_Payload,$company);
$result_decoded_Verify = json_decode($result_Api_Verify, true);

// echo $result_Api_Verify;

	if(strpos($result_Api_Verify,"Unauthorized") !== false)
	{ 
		// closeConnection($conn);
		echo "<font color='red'><b>Error :</b> ".$result."</font>
		<script>
			$('#loadicon').fadeOut('slow');
			$('#kyc_button$id').attr('disabled',false);
		</script>";
		exit();
	} 
	
	if(@$result_decoded_Verify['error'])
	{
		$error_msg_Verify = $result_decoded_Verify['error']['message'];
		
		echo "<script>
				alert('Error : API Error : $error_msg_Verify.');
				$('#loadicon').fadeOut('slow');
				$('#kyc_button$id').attr('disabled',false);
		</script>";
		exit();
	}
	else
	{
		$link = $result_decoded_Verify['res_data']['redirect_url'];
	}
	
StartCommit($conn);
$flag = true;

$update_kyc_url = Qry($conn,"UPDATE dairy.happay_users SET kyc_url='$link' WHERE id='$id'");

if(!$update_kyc_url){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}		

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		$('#kyc_button$id').attr('disabled',true);
		$('#kyc_button$id').attr('onclick','');
		$('#kyc_button$id').html('Link Generated');
		// $('#link_kyc_$id').attr('href','$link');
		$('#link_kyc_2_$id').val('$link');
		$('#kyc_link_btn_$id').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
		alert('Error While Processing Request.');
		$('#kyc_button$id').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}	
?>