<?php
include "header.php";
?>

<div class="content-wrapper">
    <section class="content-header">
      <h4 style="font-size:15px">
		Assign Cards to User :
      </h4>
	  
	  <style>
	  .form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<script type="text/javascript">
$(document).ready(function (e) {
$("#AssignForm").on('submit',(function(e) {
$("#loadicon").show();
$("#submit_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_assign_card.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#tab_result").html(data);
	},
	error: function() 
	{} });}));});
</script>	
	
<div id="tab_result"></div>
	
<?php
// $data = array(
// "requestId"=>"12345ABC",
// "userId"=>"GJ18AZ6763"
// );
// $JsonString = json_encode($data);
// echo HappayAPI("auth/v1/cards/get_user_info/",$JsonString,"RRPL");
?>	

<div class="row">
 
 <div class="form-group col-md-12">
    
	<div class="form-group col-md-12 table-responsive">
	 <br />
        <table id="example" class="table table-bordered table-striped" style="font-size:12px;">
          <thead>		
		  <tr>
				<th>#id</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Email-Id</th>
				<th>Company</th>
				<th>Vehicle_No</th>
				<th>Mobile</th>
				<th>DOB</th>
				<th>Password</th>
				<th>#</th>
			</tr>
          </thead>	
			<tbody> 		  
            <?php
              $sql = Qry($conn,"SELECT id,f_name,l_name,company,veh_no,mobile,email,dob,userId,password 
			  FROM dairy.happay_users WHERE card_id='0' AND kyc_status_happay='Approved'");
              
			 if(!$sql){
				echo getMySQLError($conn);
				errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
				exit();
			}
			  
			  if(numRows($sql)==0)
			  {
				echo "<tr>
						<td colspan='12'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			  }
			 else
			 {
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
				echo 
                "<tr>
					<td>$sn</td>
					<td>$row[f_name]</td>
					<td>$row[l_name]</td>
					<td>$row[email]</td>
					<td>$row[company]</td>
					<td>$row[veh_no]</td>
					<td>$row[mobile]</td>
					<td>";
					if($row['dob']==0)
					{	
						echo "<font color='red'>NA</font>"; 
					}
					else
					{ 
						echo date("d/m/y",strtotime($row['dob']));
					}
					
					echo "</td>				 
					<td>$row[password]</td>
					<td>
						<input type='hidden' id='customer_name$row[id]' value='$row[f_name] $row[l_name]'>
						<input type='hidden' id='company$row[id]' value='$row[company]'>
						<input type='hidden' id='veh_no$row[id]' value='$row[veh_no]'>
						<button type='button' id='Assign$row[id]' onclick=AssignCard('$row[id]','$row[mobile]') 
						class='btn btn-xs btn-success'><li class='fa fa-credit-card'></li> Assign Card</button>
					</td>
				</tr>
				";
				$sn++;		
              }
			}
            ?>
		</tbody> 	
        </table>
      </div>
    </div>
  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>

<button type="button" id="ModalButton" style="display:none" class="btn btn-primary" data-toggle="modal" data-target="#myModal"></button>

<div class="modal" id="myModal" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" style="font-size:15px">Assign Card to : <span style="color:blue" id="customer_name"></span></h4>
      </div>
	<form id="AssignForm" autocomplete="off">
      <div class="modal-body">
		<div class="row">
			<div class="form-group col-md-12">
				<label>Available Cards : <font color="red">*</font></label>
				<select data-size="8" name="kit_id" data-live-search="true" data-live-search-style="" class="form-control selectpicker" required>
					<option value="">--Select Card--</option>
					<?php
$get_cards = Qry($conn,"SELECT id,card_kit_id,company,veh_no FROM dairy.happay_card_inventory WHERE card_assigned='0'");
if(!$get_cards){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($get_cards)>0)
{
	while($row=fetchArray($get_cards))
	{
		echo "<option id='card_list_$row[card_kit_id]' value='$row[id]_$row[card_kit_id]'>$row[card_kit_id] - $row[veh_no]</option>";
	}
}
			?>
			</select>
			</div>
		</div>
			<input type="hidden" id="id_box" name="id">	
			<input type="hidden" id="userid_box" name="userid">	
			<input type="hidden" id="company_modal" name="company">	
			<input type="hidden" id="veh_no_user" name="veh_no_user">	
      </div>

      <div class="modal-footer">
        <button type="submit" id="submit_button" class="btn btn-sm btn-primary">Assign Card</button>
        <button type="button" onclick="MyFunc1($('#id_box').val())" id="close_modal_button" class="btn btn-sm btn-danger" data-dismiss="modal">Close</button>
        <button type="button" id="modal_hide" data-dismiss="modal" style="display:none">das</button>
      </div>
	</form>
    </div>
  </div>
</div>	

<div id="card_kit_data"></div>

<script>	
function AssignCard(id,userid)
{
	$('#Assign'+id).attr('disabled',true);
	var customer_name = $('#customer_name'+id).val();
	var company = $('#company'+id).val();
	var veh_no = $('#veh_no'+id).val();
	
	$("#loadicon").show();
	jQuery.ajax({
		url: "fetch_free_cards_for_assign.php",
		data: 'customer_name=' + customer_name + '&id=' + id + '&userid=' + userid + '&company=' + company + '&veh_no=' + veh_no,
		type: "POST",
		success: function(data) {
		$("#card_kit_data").html(data);
		},
		error: function() {}
	});
}

function MyFunc1(id){
	$('#Assign'+id).attr('disabled',false);
}

$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

<?php
include "footer.php";
?>