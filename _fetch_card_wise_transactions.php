<?php
require_once '../_connect.php';

$timestamp = date("Y-m-d H:i:s");

$from_date = "2020-08-17";
$to_date = date("Y-m-d");

$sql = Qry($conn,"SELECT veh_no,company FROM dairy.happay_card_inventory WHERE veh_no in('GJ18AX5823',
'GJ18AX5823',
'GJ18AZ7053',
'GJ18AZ7053',
'GJ18AX7380',
'GJ18AZ6742',
'GJ18AZ3901',
'GJ18AX9954',
'GJ18AX3339',
'GJ18AZ3993',
'GJ18AZ7053',
'GJ18AZ3901',
'RJ38GA0654',
'RJ38GA0654',
'GJ18AZ7053',
'GJ18AZ7053',
'GJ18X8995',
'GJ18AZ7412',
'GJ18AX9900',
'GJ18AX7209',
'GJ18AZ1149',
'GJ18AZ1149') OR veh_no IN(select card_no from dairy.happay_webhook_live WHERE date BETWEEN '$from_date' AND '$to_date')");

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($sql)==0)
{
	echo "No trans. found.";
	exit();
}

StartCommit($conn);
$flag = true;	

$insert_file_run = Qry($conn,"INSERT INTO dairy.running_scripts(file_name,timestamp) VALUES ('LOAD_CARD_WISE_TRANS','$timestamp')");

if(!$insert_file_run){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
}

while($row_card = fetchArray($sql))
{
	if($row_card['company']=='RRPL'){
		$token="lmfIAJzy1RZPsSZ50hwWcLpEa";
	}else{
		$token="icaxFOPnZGn9zxQHFslAQxEt2";
	}
		
	$JsonString1 = json_encode(array(
	"requestId"=>"REQ12345",
	"userId"=>"$row_card[veh_no]",
	"start_date"=>date(("d-m-Y"),strtotime($from_date)),
	"end_date"=>date(("d-m-Y"),strtotime($to_date))
	));
	$ch = curl_init('https://api-v2.happay.in/auth/v1/cards/get_user_txns/');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, 900);
	curl_setopt($ch, CURLINFO_HEADER_OUT, true);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $JsonString1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Authorization: Bearer '.$token.'',
		'Content-Type: application/json',
		'Content-Length: ' . strlen($JsonString1))
	);
	$result = curl_exec($ch);
	curl_close($ch);
	
	$result1 = json_decode($result, true);
	
	if(@$result1['error']=="")
	{
		$vehicle_no = $row_card['veh_no'];
		if($result1['res_data']['total_count']>0)
		{
			foreach($result1['res_data']['txn_list'] as $data)
			{
				$txn_date_Zone = substr($data['txn_date'],-5);
				if($txn_date_Zone=="00:00")
				{
					$txn_date1 = date('Y-m-d H:i:s',strtotime('+5 hour +30 minutes',strtotime(substr($data['txn_date'],0,19))));
					$txn_date = substr($txn_date1,0,19);
				}
				else
				{
					$txn_date = substr($data['txn_date'],0,19);		
				}
				
			$insert = Qry($conn,"INSERT INTO dairy.happay_webhook_db(veh_no,merchant,dr_cr,txn_id,user,is_fee,txn_type,
			currency_amount,txn_type_id,card_details,txn_wallet,mcc_code,txn_date,channel,timestamp) VALUES ('$row_card[veh_no]',
				'$data[merchant]','$data[debit_or_credit_indicator]','$data[txn_id]','$data[user]','$data[is_fee]',
				'$data[txn_type]','$data[currency_amount]','$data[txn_type_id]','','$data[txn_wallet]','$data[mcc_code]',
				'$txn_date','$data[channel]','$timestamp')");
				
				if(!$insert){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				// $insert_trans = Qry($conn,"INSERT INTO dairy._temp_happay_trans(card_no,txn_id) VALUES ('$vehicle_no','$data[txn_id]')");
				// if(!$insert_trans){
					// $flag = false;
					// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				// }
			}
		}
	}
	else
	{
		$flag = false;
		errorLog("Fetch Api Trans Error: $result",$conn,$page_name,__LINE__);
		echo "<font color='red'>$result<br></font>";	
	}
}

$lastId =  getInsertID($conn);

$insert2 = Qry($conn,"INSERT INTO dairy.happay_webhook_db(veh_no,merchant,dr_cr,txn_id,user,is_fee,txn_type,currency_amount,
txn_type_id,card_details,txn_wallet,mcc_code,txn_date,channel,timestamp) SELECT veh_no,merchant,dr_cr,txn_id,user,is_fee,
txn_type,currency_amount,txn_type_id,card_details,txn_wallet,mcc_code,txn_date,channel,timestamp 
FROM dairy.happay_webhook_db ORDER BY txn_date ASC");
				
if(!$insert2){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$delete1 = Qry($conn,"DELETE FROM dairy.happay_webhook_db WHERE id<='$lastId'");
if(!$delete1){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$update_trans_id_erp = Qry($conn,"UPDATE dairy.happay_webhook_db t1 
        INNER JOIN dairy.happay_webhook_live t2 
             ON t1.txn_id = t2.txn_id
SET t1.trans_id = t2.trans_id 
WHERE t2.date BETWEEN '$from_date' AND '$to_date' AND t1.txn_id = t2.txn_id");

if(!$update_trans_id_erp){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$delete_existing_trans = Qry($conn,"DELETE FROM dairy.happay_webhook_live WHERE date BETWEEN '$from_date' AND '$to_date'");
	
if(!$delete_existing_trans){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$update_balance_first = Qry($conn,"UPDATE dairy.happay_live_balance t1 
        INNER JOIN dairy.happay_webhook_live t2 
             ON t1.veh_no = t2.card_no
SET t1.balance_webhook = t2.balance 
WHERE t2.id IN(SELECT max(id) 
FROM dairy.happay_webhook_live WHERE card_no IN(SELECT DISTINCT veh_no FROM dairy.happay_webhook_db) GROUP BY card_no) AND 
t1.veh_no = t2.card_no");
	
if(!$update_balance_first){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$get_transactions = Qry($conn,"SELECT id,veh_no,dr_cr,currency_amount as amount FROM dairy.happay_webhook_db ORDER BY txn_date ASC");
if(!$get_transactions){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
	while($row_trans = fetchArray($get_transactions))
	{
		$get_live_balance = Qry($conn,"SELECT balance_webhook FROM dairy.happay_live_balance WHERE veh_no='$row_trans[veh_no]'");
	
		if(!$get_live_balance){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$row_live_balance = fetchArray($get_live_balance);
		
		if($row_trans['dr_cr']=='C')
		{
			$column_name = "credit";
			$new_balance1 = $row_trans['amount']+$row_live_balance['balance_webhook'];
		}
		else
		{
			$column_name = "debit";
			$new_balance1 = $row_live_balance['balance_webhook']-$row_trans['amount'];
		}
		
		$update_balance = Qry($conn,"UPDATE dairy.happay_live_balance SET balance_webhook='$new_balance1' WHERE 
		veh_no='$row_trans[veh_no]'");
	
		if(!$update_balance){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$update_this_trans = Qry($conn,"UPDATE dairy.happay_webhook_db SET `$column_name`='$row_trans[amount]',
		balance='$new_balance1' WHERE id='$row_trans[id]'");
		
		if(!$update_this_trans){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	
$insert_new_trans = Qry($conn,"INSERT INTO dairy.happay_webhook_live(card_no,company,card_kit_id,first_name,last_name,user_id,
email_id,mobile_no,merchant,txn_id,trans_id,trans_date,trans_type,credit,debit,balance,date,narration,timestamp) SELECT 
veh_no,'','',user,'','','','',merchant,txn_id,trans_id,txn_date,txn_type,credit,debit,balance,date(txn_date),'',timestamp 
FROM dairy.happay_webhook_db");	

if(!$insert_new_trans){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$update_new_balance = Qry($conn,"UPDATE dairy.happay_live_balance SET balance=balance_webhook WHERE veh_no IN(SELECT 
DISTINCT veh_no FROM dairy.happay_webhook_db)");
	
if(!$update_new_balance){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$Delete_Temp_Trans = Qry($conn,"DELETE FROM dairy.happay_webhook_db");
if(!$Delete_Temp_Trans){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$Delete_running_file = Qry($conn,"DELETE FROM dairy.running_scripts WHERE file_name='LOAD_CARD_WISE_TRANS'");
if(!$Delete_running_file){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
	if($flag)
	{
		MySQLCommit($conn);
		closeConnection($conn);
		echo "<script>
			alert('SUCCESS : Card Transactions fetched successfully !');
			$('#loadicon').hide();
		</script>";
		exit();
	}
	else
	{
		echo "<script>
			alert('Error while Fetching Transactions !');
			$('#loadicon').hide();
		</script>"; 
		
		MySQLRollBack($conn);
		closeConnection($conn);
		errorLog("Error While Processing Request.",$conn,$page_name,__LINE__);
		exit();
	}
	
echo "ALL OK";	
?>