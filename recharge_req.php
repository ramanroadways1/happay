<?php
include "header.php";
?>
 
<script type="text/javascript">
$(document).ready(function (e) {
	$("#UpdateTransaction").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#BtnSubmit").attr("disabled", true);
		$.ajax({
        	url: "./recharge_req_update.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#formResult").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>

 <div class="content-wrapper">
    <section class="content-header">
     
<h4 style="font-size:15px">
	Wallet Load Request : <font size="3" color="maroon">Available Balance => <?php echo "RRPL: $rrpl_balance, RR: $rr_balance"; ?></font>
</h4>
	  
	  <style>
		.form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div class="row">
 
 <div class="form-group col-md-12">
    
	<div class="form-group col-md-12 table-responsive">
	<div id="result"></div>
	 <br />
        <table id="example" class="table table-bordered table-striped" style="font-size:12px;">
		<thead>		
		 <tr>
				<th>#</th>
				<th>Vehicle<br>Number</th>
				<th>Trans.Id</th>
				<th>Trans.Type</th>
				<th>Amount</th>
				<th>Branch</th>
				<th>Date</th>
				<th>Req.By</th>
				<th>#Delete</th>
				<th>#Approve</th>
			</tr>
          </thead>	
		  <tbody>  
 <?php
$sql = Qry($conn,"SELECT id,card_no,card_using,trans_id,trans_type,credit,branch,date,superv_entry FROM dairy.happay_card_transactions 
WHERE done!='1'");
              
			  if(!$sql){
				echo getMySQLError($conn);
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
				exit();
			  }
			  
			  if(numRows($sql)==0)
			  {
				echo "<tr>
						<td colspan='12'>NO RESULT FOUND !!</td>
					</tr>";  
			  }
			 else
			 {
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
					if($row['superv_entry']==1)
					{
						$req_by = "<font color='red'>Supervisor</font>";
					}	
					else
					{
						$req_by = "<font color='green'>Branch</font>";
					}
//<td><button type='button' id='ButtonEdit$row[id]' onclick=Edit('$row[id]') class='class_btn_edit btn btn-danger btn-sm'>Edit</button></td>
			echo 
                "<tr>
					<td>$sn</td>
					<td>$row[card_using]</td>
					<td>$row[trans_id]</td>
					<td>$row[trans_type]</td>
					<td>$row[credit]</td>
					<td>$row[branch]</td>
					<td>".date("d-m-y",strtotime($row["date"]))."</td>
					<td>$req_by</td>
					<input type='hidden' id='amountDb$row[id]' value='$row[credit]'>
					<input type='hidden' id='trans_idDb$row[id]' value='$row[trans_id]'>
					<td><button type='button' id='ButtonDelete$row[id]' onclick=Delete('$row[id]') class='class_btn_delete btn btn-danger btn-xs'><li class='fa fa-trash-o'></li> Delete</button></td>
					<td><button type='button' id='Button1$row[id]' onclick=Approve('$row[id]','$row[card_using]','$row[credit]') class='class_btn_approve btn btn-success btn-xs'><li class='fa fa-thumbs-o-up'></li> Approve</button></td>
				</tr>
				";
				$sn++;		
              }
			}
            ?>
		 </tbody> 	
        </table>
      </div>
    </div>
  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>

<script>	
function Approve(id,tno,amount)
{
	$('#ButtonEdit'+id).attr('disabled',true);
	$('#Button1'+id).attr('disabled',true);
	$('#ButtonDelete'+id).attr('disabled',true);
	
	var trans_id = $('#trans_idDb'+id).val();
	
	$("#loadicon").show();
	jQuery.ajax({
		url: "recharge_req_save.php",
		data: 'id=' + id + '&tno=' + tno + '&amount=' + amount + '&trans_id=' + trans_id,
		type: "POST",
		success: function(data) {
		$("#result").html(data);
		},
		error: function() {}
	});
}

function Edit(id){
	$('#ButtonEdit'+id).attr('disabled',true);
	$('#amount_modal').val($('#amountDb'+id).val());
	$('#ModalId').val(id);
	$('#BtnModal').click();
}

function Delete(id){
	$('#ButtonDelete'+id).attr('disabled',true);
	$("#loadicon").show();
	jQuery.ajax({
		url: "recharge_req_delete.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
		$("#result").html(data);
		},
		error: function() {}
	});
}

$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

<?php
include "footer.php";
?>

<button type="button" id="BtnModal" class="btn btn-info btn-lg" data-toggle="modal" data-target="#EditModal" style="display:none"></button>

<form id="UpdateTransaction" autocomplete="off" method="POST">
<div class="modal fade" id="EditModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <h4 class="modal-title">Update Transaction :</h4>
        </div>
        <div class="modal-body">
          <div class="row">
			<div class="form-group col-md-6">
				<label>Amount <font color="red"><sup>*</font></font></label>
				<input type="number" class="form-control" min="1" required="required" id="amount_modal" name="amount" />
			</div>
		  </div>
        </div>
		<div id="formResult"></div>
		<input type="hidden" id="ModalId" name="id">
        <div class="modal-footer">
          <button type="submit" id="BtnSubmit" class="btn btn-success">Update</button>
          <button type="button" class="btn btn-default" onclick="$('#ButtonEdit'+$('#ModalId').val()).attr('disabled',false);" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
 </form> 
