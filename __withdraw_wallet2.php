<?php
include "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
      <h4 style="font-family:Verdana">
		Withdraw Wallet Amount - 2 :
      </h4>
	  
	  <style>
		label{font-family:Verdana;font-size:13px;}
		.form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div class="row">
 
 <div class="form-group col-md-12">
    
	<div class="form-group col-md-12 table-responsive">
	<div id="result"></div>
	 <br />
        <table id="myTable" class="table table-bordered table-striped" style="font-family:Verdana;font-size:12px;">
          <tr>
				<th>#</th>
				<th>Card/Vehicle<br>Number</th>
				<th>Company</th>
				<th>Withdraw Amount</th>
			</tr>
          
            <?php
              $sql = Qry($conn,"SELECT id,tno,company,card_no2,balance FROM happay_card");
              
			  if(!$sql){
				  echo getMySQLError($conn);exit();
			  }
			  
			  if(numRows($sql)==0)
			  {
				echo "<tr>
						<td colspan='12'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			  }
			 else
			 {
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
				echo 
                "<tr>
					<td>$sn</td>
					<td>$row[card_no2]<br>($row[tno])</td>
					<td>$row[company]</td>
					<td>
		<div class='form-inline'>
			<input type='number' class='form-control' style='font-size:12px;height:30px;' id='amount$row[id]'>
			<input type='hidden' value='$row[tno]' id='tno_1$row[id]'>
<button type='button' id='button1$row[id]' onclick=WithdrawAmount('$row[id]')
class='btn btn-primary btn-sm'><span style='font-size:px;' class='glyphicon glyphicon-share-alt'></span></a></div>
					</td>
				</tr>
				";
				$sn++;		
              }
			}
            ?>
        </table>
      </div>
    </div>
  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>

<script>	
function WithdrawAmount(id)
{
	var amount = $('#amount'+id).val();
	var tno = $('#tno_1'+id).val();
	
	if(amount!=0 && amount!='' && amount>0)
	{
		$('#button1'+id).attr('disabled',true);
			$("#loadicon").show();
			jQuery.ajax({
				url: "withdraw_wallet_action2.php",
				data: 'id=' + id + '&amount=' + amount + '&tno=' + tno,
				type: "POST",
				success: function(data) {
				$("#result").html(data);
				},
				error: function() {}
		});
	}
	else
	{
		alert('Please enter valid amount value !');
	}
}
</script>

<?php
include "footer.php";
?>