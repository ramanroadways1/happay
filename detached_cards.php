<?php
include "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
      <h4 style="">
		Detached Cards :
      </h4>
	  
	  <style>
		.form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div class="row">
 
 <div class="form-group col-md-12">
    
	<div class="form-group col-md-12 table-responsive">
	<div id="result"></div>
	 <br />
       <table id="example" class="table table-bordered table-striped" style="font-size:12px;">
		<thead>		
		 <tr>
				<th>#</th>
				<th>Vehicle Number</th>
				<th>Kit Id</th>
				<th>Timestamp</th>
			</tr>
          </thead>
		 <tbody> 
            <?php
              $sql = Qry($conn,"SELECT * FROM dairy.happay_detached_cards ORDER by id ASC");
              
			  if(!$sql){
				echo getMySQLError($conn);
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
				exit();
			}

			  
			  if(numRows($sql)==0)
			  {
				echo "<tr>
						<td colspan='12'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			  }
			 else
			 {
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
				echo 
                "<tr>
					<td>$sn</td>
					<td>$row[card_no]</td>
					<td>$row[kit_id]</td>
					<td>$row[timestamp]</td>
				</tr>";
				
				$sn++;		
              }
			}
            ?>
		</tbody>	
        </table>
      </div>
    </div>
  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>

<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<?php
include "footer.php";
?>