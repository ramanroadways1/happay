<?php
include "header.php";
exit();
?>

<div class="content-wrapper">
    <section class="content-header">
      <h4 style="font-family:Verdana">
		Load API Transactions :
      </h4>
	  
	  <style>
		label{font-family:Verdana;font-size:13px;}
		.form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
		</section>
		
   <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div class="row">
 
 <div class="form-group col-md-12">
    <br />
	<div class="form-group col-md-12">
	<div class="row">
		<div class="form-group col-md-4">
			<label>Select Transaction Date <font color="red">*</font></label>
			<input type="date" max="<?php echo date("Y-m-d"); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" class="form-control" required="required" id="from_date">
		</div>
		
		<div class="form-group col-md-3">
			<label>&nbsp;</label>
			<br />
			<button id="button1" type="button" onclick="LoadTrans();" class="btn btn-primary">Get Transactions</button>
		</div>
	</div>
		
    </div>
	
	<div class="form-group col-md-12 table-responsive" id="func_result">
		 
        	
    </div>
    </div>
  </div>
</div>

</body>
</html>

          </div>
          </div>
		  </div>
       </div>         
    </section>
	
<script>
function LoadTrans()
{
	var from_date = $('#from_date').val();
	
	if(from_date=="")
	{
		alert('Date not found !');
	}
	else
	{
		$("#loadicon").show();
		jQuery.ajax({
			url: "_fetch_api_transactions.php",
			data: 'from_date=' + from_date,
			type: "POST",
			success: function(data) {
				$("#func_result").html(data);
			},
			error: function() {}
		});
	}
}

// $(document).ready(function() {
    // $('#example').DataTable();
// } );
</script>	
<?php
include "footer.php";
?>