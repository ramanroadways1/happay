<?php
require_once 'connect.php';

$from_date = escapeString($conn,strtoupper($_POST['from_date']));
$to_date = escapeString($conn,strtoupper($_POST['to_date']));

$sql = Qry($conn,"SELECT * FROM dairy.happay_webhook_load_wallet WHERE date(txn_date) BETWEEN '$from_date' AND '$to_date' 
ORDER BY txn_date ASC");

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

?>
<a target="_blank" href="_download_excel_org_trans.php?from_date=<?php echo $from_date; ?>&to_date=<?php echo $to_date; ?>"><button type="button" class="pull-right btn btn-danger">Download Excel</button></a>
<br />
<br />
	<table id="example" class="table table-bordered table-striped" style="font-size:12.5px;">
        <thead>
		<tr>
			<th>#</th>
			<th>Company</th>
			<th>Merchant</th>
			<th>Txn_Id</th>
			<th>Trans_Type</th>
			<th>Credit</th>
			<th>Debit</th>
			<th>Txn_Date</th>
		</tr>
		</thead>
       <tbody id=""> 
		
<?php
if(numRows($sql)==0)
{
	echo "<tr>
		<td colspan='7'>NO RESULT FOUND !!</td>
	</tr>";  
}
else
{
	$sn=1;
	while($row = fetchArray($sql))
	{
		if($row['dr_cr']=="C"){
			$cr_amt = $row['currency_amount'];
			$dr_amt = 0;
		}
		else{
			$dr_amt = $row['currency_amount'];
			$cr_amt = 0;
		}
				echo 
                "<tr>
					<td>$sn</td>
					<td>$row[company]</td>
					<td>$row[merchant]</td>
					<td>$row[txn_id]</td>
					<td>$row[txn_type]</td>
					<td>$cr_amt</td>
					<td>$dr_amt</td>
					<td>".date("d-M-y H:i A",strtotime($row["txn_date"]))."</td>
				</tr>";
		$sn++;		
     }
}
?>
	</tbody>
		</table>
<script> 
$("#loadicon").hide();
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script> 