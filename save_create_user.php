<?php
require_once("connect.php");

$requestId=date("Y:m:d_H:i:s");

$title = escapeString($conn,($_POST['title']));
$f_name = escapeString($conn,strtoupper($_POST['f_name']));
$l_name = escapeString($conn,strtoupper($_POST['l_name']));
$gender = escapeString($conn,($_POST['gender']));
$password = escapeString($conn,($_POST['password']));
$mobile = escapeString($conn,($_POST['mobile']));
$dob = escapeString($conn,($_POST['dob']));
$email = escapeString($conn,(strtolower($_POST['email'])));
$company = escapeString($conn,($_POST['company']));
$veh_no = escapeString($conn,strtoupper($_POST['veh_no']));
$email2 = escapeString($conn,strtoupper($_POST['email_unq_id']));
// $doc_type = escapeString($conn,($_POST['doc_type']));
// $doc_no = escapeString($conn,strtoupper($_POST['doc_no']));

if(strlen($mobile)!=10){
	echo "<script>
		alert('Error : Check Mobile Number.');
		$('#create_button').attr('disabled', false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$chk_mobile = Qry($conn,"SELECT id FROM dairy.happay_users WHERE mobile='$mobile'");
if(!$chk_mobile){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($chk_mobile)>0){
	echo "<script>
		alert('Error : Duplicate Mobile Number Found.');
		$('#create_button').attr('disabled', false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$chk_email = Qry($conn,"SELECT id FROM dairy.happay_users WHERE email='$email'");
if(!$chk_email){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($chk_email)>0){
	echo "<script>
		alert('Error : Duplicate Email ! ');
		$('#create_button').attr('disabled', false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$data = array(
"requestId"=>$requestId,
"title"=>$title,
"firstName"=>$f_name,
"lastName"=>$l_name,
"emailId"=>$email,
"metaFields"=>array(
"Vehicle Number"=>$veh_no,
// "Card Kit ID"=>".",
),
"dob"=>$dob,
"userId"=>$mobile,
"gender"=>$gender,
"password"=>$password,
"mobileNo"=>$mobile
);

$JsonString = json_encode($data);

$result = HappayAPI("auth/v1/cards/add_user/",$JsonString,$company);

// echo $result."<br><br>";

$result2 = json_decode($result, true);

	if(strpos($result,"Unauthorized") !== false)
	{ 
		closeConnection($conn);
		echo "<font color='red'><b>Error :</b> ".$result."</font>
		<script>
			$('#create_button').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
		exit();
	} 
	
if(@$result2['error'])
{
	closeConnection($conn);
	echo "<font color='red'><b>Error :</b> ".$result2['error']['message']."</font>
	<script>
			$('#create_button').attr('disabled', false);
			$('#loadicon').hide();
	</script>";
	exit();	
}
else
{
	$result_res=$result2['res_str'];			
	$user_id_happay = $result2['res_data']['userId'];			
	
	foreach($result2['res_data']['wallets'] as $doc)
	{
		$wl_id = $doc['wallet_id'];
	}

StartCommit($conn);
$flag = true;
	
	$insert = Qry($conn,"INSERT INTO dairy.happay_users(title,f_name,l_name,company,veh_no,gender,email,email2,mobile,dob,userId,
	wallet_id,org_user_id,password,timestamp) VALUES ('$title','$f_name','$l_name','$company','$veh_no','$gender','$email',
	'$email2','$mobile','$dob','$user_id_happay','$wl_id','$mobile','$password','$timestamp')");
	
	if(!$insert){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
	$log_insert=Qry($conn,"INSERT INTO dairy.happay_log(req_id,result,api_name,api_response,status,timestamp) VALUES 
		('$requestId','$result_res','Add_User','$result','1','$timestamp')");
	
	if(!$log_insert){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('User Successfully created.');
		window.location.href='./create_user.php';
		// $('#create_button').attr('disabled', false);
		// $('#loadicon').hide();
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./create_user.php");
	exit();
}
	
}
?>