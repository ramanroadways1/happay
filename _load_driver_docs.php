<?php
require_once 'connect.php';

$mobile = escapeString($conn,strtoupper($_POST['mobile']));

$sql = Qry($conn,"SELECT name,lic,lic_front,lic_rear,driver_photo FROM dairy.driver WHERE mobile='$mobile'");
if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

?>
<table id="example" class="table table-bordered table-striped" style="font-size:12.5px;">
        <tr>
			<th>Driver Name</th>
			<th>Lic.No</th>
			<th>Lic Copy(front)</th>
			<th>Lic Copy(rear)</th>
			<th>Driver Photo</th>
		</tr>
		
<?php
if(numRows($sql)==0)
{
	echo "<tr>
		<td colspan='12'>NO RESULT FOUND !!</td>
	</tr>";  
}
else
{
	$sn=1;
	while($row = fetchArray($sql))
	{
				echo 
                "<tr>
					<td>$row[name]</td>
					<td>$row[lic]</td>
					<td><a target='_blank' href='../diary_admin/$row[lic_front]'>View</a></td>
					<td><a target='_blank' href='../diary_admin/$row[lic_rear]'>View</a></td>
					<td><a target='_blank' href='../diary_admin/$row[driver_photo]'>View</a></td>
				</tr>
				";
		$sn++;		
     }
}
?>
	</table>
<script> 
$("#loadicon").hide();
</script> 