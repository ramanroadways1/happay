<?php
require_once("connect.php");

$requestId=date("Y:m:d_H:i:s");
$date=date("Y-m-d");
$timestamp=date("Y-m-d H:i:s");

$id = escapeString($conn,strtoupper($_POST['id']));
$amount = escapeString($conn,strtoupper($_POST['amount']));

if($amount=='' || $amount==0 || $amount<=0){
	echo "<script>
		alert('Invalid amount entered !!');
		$('#loadicon').hide();
		$('#button1$id').attr('disabled',false);
	</script>";
	exit();	
}

$chk_running_file = Qry($conn,"SELECT id FROM dairy.running_scripts");
if(!$chk_running_file){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($chk_running_file)>0)
{
	echo "<script>
		alert('Please wait while api transaction are loading !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$getCardBal = Qry($conn,"SELECT veh_no,card_kit_id,company,card_status as status FROM dairy.happay_card_inventory WHERE id='$id'");
if(!$getCardBal){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing request.","withdraw_wallet.php");
	exit();
}

if(numRows($getCardBal)==0){
	echo "<script>
			alert('Error : card not found.');
			$('#loadicon').hide();
		</script>";
	exit();
}

$row_card_info = fetchArray($getCardBal);

if($row_card_info['status']!=1)
{
	echo "<script>
			alert('Error : card is in-active.');
			$('#loadicon').hide();
		</script>";
	exit();
}

$tno = $row_card_info['veh_no'];

$chk_for_other_card = Qry($conn,"SELECT tno FROM dairy.happay_card WHERE card_using='$tno' AND other_card='1'");

if(!$chk_for_other_card){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing request.","withdraw_wallet.php");
	exit();
}

if(numRows($chk_for_other_card)>0)
{
	$row_other_card = fetchArray($chk_for_other_card);
	
	$ChkTrip = Qry($conn,"SELECT trip_no,driver_code FROM dairy.trip WHERE tno='$row_other_card[tno]'");
	if(!$ChkTrip){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Trip not Found.","./");
		exit();
	}
	$tno2 = $row_other_card['tno'];
}
else
{
	$ChkTrip = Qry($conn,"SELECT trip_no,driver_code FROM dairy.trip WHERE tno='$tno'");
	if(!$ChkTrip){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Trip not Found.","./");
		exit();
	}
	$tno2 = $tno;
}

if(numRows($ChkTrip)==0)
{
	echo "<script>
		alert('Error : Running Trip not found. Vehicle Number : $tno.');
		$('#loadicon').hide();
	</script>"; 
	exit();
}

$rowTrip = fetchArray($ChkTrip);

if($rowTrip['driver_code']==0 || $rowTrip['driver_code']=="")
{
	echo "<script>
		alert('Error : Driver not found. Vehicle Number : $tno($rowTrip[trip_no]).');
		$('#loadicon').hide();
	</script>"; 
	exit();
}

$trip_no = $rowTrip['trip_no'];
$driver_code = $rowTrip['driver_code'];

$card_bal = Qry($conn,"SELECT balance FROM dairy.happay_live_balance WHERE veh_no='$row_card_info[veh_no]'");
if(!$card_bal){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing request.","withdraw_wallet.php");
	exit();
}

if(numRows($card_bal)==0){
	echo "<script>
			alert('Error : card balance not found.');
			$('#loadicon').hide();
		</script>";
	exit();
}

$rowBal = fetchArray($card_bal);

if($amount>$rowBal['balance'])
{
	echo "<script>
			alert('Error : Requested Withdrawn amount is greater than balance amount.');
			$('#loadicon').hide();
		</script>";
	exit();
}

if($amount==0 || $amount=='')
{
	echo "<script>
			alert('Error : Invalid amount entered.');
			$('#loadicon').hide();
		</script>";
	exit();
}

$newBalance = $rowBal['balance']-$amount;
$company = $row_card_info['company'];

/// Check Current Bal of Card //////

	$ReqID_Verify = date("Y-m-d_H:i:s");
	$verify_Payload = array(
	"requestId"=>$ReqID_Verify,
	"userId"=>$tno
	);
	$verify_Payload = json_encode($verify_Payload);
	$result_Api_Verify = HappayAPI("auth/v1/cards/get_wallet_balance/",$verify_Payload,$company);
	$result_decoded_Verify = json_decode($result_Api_Verify, true);

	if(strpos($result_Api_Verify,"Unauthorized") !== false)
	{ 
		closeConnection($conn);
		echo "<font color='red'><b>Error :</b> ".$result."</font>
		<script>
			$('#loadicon').hide();
		</script>";
		exit();
	} 
	
	if(@$result_decoded_Verify['error'])
	{
		$error_msg_Verify = $result_decoded_Verify['error']['message'];
		$insertApiLog_Verify = Qry($conn,"INSERT INTO dairy.happay_api_log(req_id,tno,result,api_name,api_response,status,timestamp) 
		VALUES ('$ReqID_Verify','$tno','$error_msg_Verify','GET_WALLET_BALANCE','$result_Api_Verify','0','$timestamp')");
		
		if(!$insertApiLog_Verify){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		echo "<script>
				alert('Error : API Error : $error_msg_Verify.');
				$('#loadicon').hide();
		</script>";
		exit();
	}
	else
	{
		foreach($result_decoded_Verify['res_data'] as $data_Bal)
		{
			$verify_Bal = $data_Bal['current_balance'];			
		}			
	
		if($rowBal['balance']!=$verify_Bal)
		{
			// SendMsgCustom(9024281599,"$tno: Balance not matching. API and System Balance is : $verify_Bal and $rowBal[balance].");
			
			echo "<script>
				alert('Error : Card balance not verified. Please try agian after 5 minutes.');
				$('#loadicon').hide();
			</script>";
			exit();
		}
	}
	
/// Check Current Bal of Card ends //////

	$trans_id_Qry=GetTransIdHappayWdl("HPWD",$conn);
	
	if(!$trans_id_Qry || $trans_id_Qry=="" || $trans_id_Qry=="0")
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error While Fetching Transaction Id.","./");
		exit();
	}
	
	$trans_id = $trans_id_Qry;

$data = array(
"requestId"=>$requestId,
"userId"=>$row_card_info['veh_no'],
"withdraw_amount"=>$amount,
"wallet_name"=>"Imprest"
);

$JsonString = json_encode($data);

$result = HappayAPI("auth/v1/cards/withdraw_user_wallet/",$JsonString,$company);
$result_decoded = json_decode($result, true);
// echo $result;
if(@$result_decoded['error'])
{
	$error_msg = $result_decoded['error']['message'];
	echo "<font color='red'><b>Error :</b> ".$error_msg."</font>";	
	
	$insertApiLog = Qry($conn,"INSERT INTO dairy.happay_api_log(req_id,tno,result,api_name,api_response,status,timestamp) VALUES 
	('$requestId','$row_card_info[veh_no]','$error_msg','WALLET_WITHDRAW','$result','0','$timestamp')");
	
	if(!$insertApiLog){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	echo "<script>
		alert('Error : $error_msg !');
		$('#Button$id').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}
else
{
	if(empty($result_decoded['res_data']['txn_id']))
	{
		echo "$result
		<script>
			alert('Happay server not responding. Please try after some time !');
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$Api_Response = $result_decoded['res_str'];	
	$txn_id = $result_decoded['res_data']['txn_id'];			
	
	$txn_type = $result_decoded['res_data']['txn_type']['name'];
	$txn_type_id = $result_decoded['res_data']['txn_type']['id'];
	
	$txn_amount = $result_decoded['res_data']['currency_amount'];
	$txn_date = $result_decoded['res_data']['txn_date'];
		
	$diff = (strtotime($timestamp)-strtotime($txn_date)/3600);
	
	if($diff>5)
	{
		$txn_date = date('Y-m-d H:i:s',strtotime('+5 hour +30 minutes',strtotime(substr($txn_date,0,19))));
	}
	else
	{
		$txn_date = $txn_date;
	}
	
	$merchant = $result_decoded['res_data']['payee_merchant'];
	
	$txn_user = $result_decoded['res_data']['user']['name'];
	
	$insertApiLog = Qry($conn,"INSERT INTO dairy.happay_api_log(req_id,tno,result,api_name,api_response,status,timestamp) VALUES 
	('$requestId','$row_card_info[veh_no]','$Api_Response','WALLET_WITHDRAW','$result','1','$timestamp')");
	
	if(!$insertApiLog){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing request.","withdraw_wallet.php");
		exit();
	}
}

StartCommit($conn);
$flag = true;

if($tno2!=$tno)
{
	$wdl_narration = "Card active on : $tno2";
}
else
{
	$wdl_narration = "";
}

$insert_Trans = Qry($conn,"INSERT INTO dairy.happay_card_withdrawal (tno,driver_code,trip_no,trans_id,txn_id,amount,narration,timestamp) VALUES 
('$tno','$driver_code','$trip_no','$trans_id','$txn_id','$amount','$wdl_narration','$timestamp')");

if(!$insert_Trans){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

// Driver Balance Update starts 

$select_amount=Qry($conn,"SELECT id,tno,amount_hold FROM dairy.driver_up WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");

if(!$select_amount){	
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($select_amount)==0){
	$flag = false;
	errorLog("Error While Fetching Driver Balance. Code: $driver_code.",$conn,$page_name,__LINE__);
}

$row_amount=fetchArray($select_amount);
$hold_amt=$row_amount['amount_hold']-round($amount);
$driver_id=$row_amount['id'];

if($row_amount['tno']!=$tno2)
{
	$flag = false;
	errorLog("Vehicle not verified. DbVeh: $row_amount[tno]. System : $tno.",$conn,$page_name,__LINE__);
}

$insert_book = Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_no,trans_id,desct,debit,balance,date,branch,
timestamp) VALUES ('$driver_code','$row_amount[tno]','$trip_no','$trans_id','HAPPAY_WITHDRAWAL','".round($amount)."','$hold_amt',
'$date','HAPPAY_ADMIN','$timestamp')");

if(!$insert_book){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_hold_amount=Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold-'".round($amount)."' WHERE id='$driver_id'");

if(!$update_hold_amount){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

// Driver Balance Update ends 

$updateMain_bal = Qry($conn,"UPDATE dairy.happay_main_balance SET api_bal=api_bal+'$amount' WHERE company='$company'");
if(!$updateMain_bal){
	$flag = false; 
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$updateTransDone = Qry($conn,"UPDATE dairy.happay_api_log SET completed='1' WHERE req_id='$requestId'");
if(!$updateTransDone){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<font color='green'><b>SUCCESS :</b> ".$Api_Response."</font>";	
	echo "<script>
		alert('Wallet Withdrawn Successfully.');
		$('#loadicon').hide();
	</script>"; 
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn); 
	Redirect("Error While Processing Request.","./withdraw_wallet.php");
	exit();
}	
	
?>