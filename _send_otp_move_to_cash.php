<?php
require_once("./connect.php");

$tno = escapeString($conn,$_POST['tno']);

if($tno=="")
{
	echo "<script>
		alert('Enter Vehicle Number First !');
		$('#button1').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

$check_assign_card = Qry($conn,"SELECT card_assigned FROM dairy.happay_card WHERE tno='$tno'");

if(!$check_assign_card){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request.","./");
	exit();
}

if(numRows($check_assign_card)==0){
	echo "<script>
		alert('Card not found !');
		$('#button1').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

$row_card = fetchArray($check_assign_card);

if($row_card['card_assigned']==0)
{
	echo "<script>
		alert('Card detached already !');
		$('#button1').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

$check_balance = Qry($conn,"SELECT balance FROM dairy.happay_live_balance WHERE veh_no='$tno'");

if(!$check_balance){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request.","./");
	exit();
}

if(numRows($check_balance)==0){
	echo "<script>
		alert('Vehicle not found !');
		$('#button1').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

$row_balance = fetchArray($check_balance);

if($row_balance['balance']>0){
	echo "<script>
		alert('Withdraw wallet amount first ! Available balance is $row_balance[balance].');
		$('#button1').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

$otp = '1234567890';
$otp = substr(str_shuffle($otp),0,6);

// SendMsgCustom(8600997434,"Deactivate happay services on vehicle $tno and switch to cash by sharing this OTP: $otp.");
// MsgHappayMoveToCash($tno,$otp);

$msg_template="Deactivate happay services on vehicle $tno and switch to cash by sharing this OTP: $otp.\nRamanRoadways.";

SendWAMsg($conn,9024281599,$msg_template);

$_SESSION['session_otp'] = $otp;
// $_SESSION['session_otp_try'] = $otp;

echo "<script>
		alert('OTP Sent !');
		$('#button1').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
?>