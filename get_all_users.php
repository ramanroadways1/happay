<?php
include "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
      <h4 style="font-family:Verdana">
		All Users :
      </h4>
	  
	  <style>
	  label{font-family:Verdana;font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

	<div class="row">
 
 <div class="form-group col-md-12">
    
	<div class="form-group col-md-12 table-responsive">
	 <br />
        <table id="myTable" class="table table-bordered table-striped" style="font-family:Verdana;font-size:12px;">
			<tr>
				<th>#id</th>
				<th>First<br>Name</th>
				<th>Last<br>Name</th>
				<th>Mobile</th>
				<th>DOB</th>
				<th>Password</th>
				<th>KYC<br>Status</th>
				<th>Card<br>Assigned</th>
			</tr>
          
            <?php
              $sql = Qry($conn,"SELECT u.f_name,u.l_name,u.kyc_status,u.mobile,u.dob,u.password,c.tno,c.card_no FROM 
			  happay_users AS u 
			  LEFT OUTER JOIN happay_card AS c ON c.id=u.card_id");
              
			  if(!$sql){
				  echo getMySQLError($conn);exit();
			  }
			  
			  if(numRows($sql)==0)
			  {
				echo "<tr>
						<td colspan='12'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			  }
			 else
			 {
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
				 if($row['kyc_status']==0){	
					 $kyc_status="<b><font color='red'>Pending</font></b>";
				 }else if($row['kyc_status']==1){
					 $kyc_status="<b><font color='blue'>Addr. Proof<br>Pending</font></b>";
				 }else if($row['kyc_status']==2){
					 $kyc_status="<b><font color='blue'>Photo<br>Pending</font></b>";
				 }else{
					$kyc_status="<b><font color='green'>Done</font></b>";
				 }
				if($row['card_no']==NULL){
					$card_assigned="NULL";
				}else{
					$card_assigned=$row['card_no']."<br>(".$row['tno'].")";
				}
					 
				echo 
                "<tr>
					<td>$sn</td>
					<td>$row[f_name]</td>
					<td>$row[l_name]</td>
					<td>$row[mobile]</td>
					<td>".date("d/m/y",strtotime($row['dob']))."</td>	
					<td>$row[password]</td>
					<td>$kyc_status</td>
					<td>$card_assigned</td>
				</tr>
				";
				$sn++;		
              }
			}
            ?>
        </table>
      </div>
    </div>
  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>
<?php
include "footer.php";
?>