<?php
require_once("./connect.php");

$tno = escapeString($conn,strtoupper($_POST['tno']));
$kit_id = escapeString($conn,strtoupper($_POST['kit_id']));
$card_type = escapeString($conn,strtoupper($_POST['card_type']));
$company = escapeString($conn,strtoupper($_POST['company']));

if($company!='RRPL')
{
	$company="RAMAN_ROADWAYS";
}

$timestamp = date("Y-m-d H:i:s");

if($kit_id=='')
{	
	echo "
	<font color='red'>Kit id not found..</font>
	<script>
		$('#button1').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if(count(explode(' ', $kit_id)) > 1)
{
	echo "
	<font color='red'>Kit id contains whitespaces..</font>
	<script>
		$('#button1').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($tno=='')
{	
	echo "
	<font color='red'>Vehicle number not found..</font>
	<script>
		$('#button1').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if(count(explode(' ', $tno)) > 1)
{
	echo "
	<font color='red'>Vehicle number contains whitespaces..</font>
	<script>
		$('#button1').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($company=='')
{	
	echo "
	<font color='red'>Company not found..</font>
	<script>
		$('#button1').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if(count(explode(' ', $company)) > 1)
{
	echo "
	<font color='red'>Company contains whitespaces..</font>
	<script>
		$('#button1').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$chk_inventory = Qry($conn,"SELECT id FROM dairy.happay_card_inventory WHERE veh_no='$tno'");

if(!$chk_inventory){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($chk_inventory)>0)
{	
	echo "
	<font color='red'>Card exists in inventory..</font>
	<script>
		$('#button1').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$chk_new_cards = Qry($conn,"SELECT id FROM dairy.happay_new_cards WHERE veh_no='$tno' AND added!='1'");

if(!$chk_new_cards){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($chk_new_cards)>0)
{	
	echo "
	<font color='red'>Duplicate record found..</font>
	<script>
		$('#button1').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

// $insert_new_card = Qry($conn,"INSERT INTO dairy.happay_card_inventory(card_kit_id,company,veh_no,card_status,timestamp) 
// VALUES ('$kit_id','$company','$tno','1','$timestamp')");

// if(!$insert_new_card){
	// $flag = false;
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
// }

$insert_new_card2 = Qry($conn,"INSERT INTO dairy.happay_new_cards(veh_no,kit_id,company,card_type,timestamp) 
VALUES ('$tno','$kit_id','$company','$card_type','$timestamp')");

if(!$insert_new_card2){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

// $insert_new_card3 = Qry($conn,"INSERT INTO dairy.happay_card(tno,company,status,timestamp) 
// VALUES ('$tno','$company','1','$timestamp')");

// if(!$insert_new_card3){
	// $flag = false;
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
// }

if($flag)
{ 
	MySQLCommit($conn);
	closeConnection($conn);	
	echo "<script type='text/javascript'>
		alert('OK : Card rcvd !!');
		window.location.href='./rcv_new_cards.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script type='text/javascript'>
		alert('Error !!');
		window.location.href='./rcv_new_cards.php';
	</script>";
	exit();
}
?>