<?php
require_once 'connect.php';

if(isset($_POST['tno']))
{
	$tno = escapeString($conn,strtoupper($_POST['tno']));
	$qry_tno=" AND card_no='$tno'";
}
else
{
	$qry_tno="";
	$tno="";
}

$from_date = escapeString($conn,strtoupper($_POST['from_date']));
$to_date = escapeString($conn,strtoupper($_POST['to_date']));

$sql = Qry($conn,"SELECT * FROM dairy.happay_webhook_live WHERE date(trans_date) BETWEEN '$from_date' AND '$to_date'$qry_tno 
ORDER BY trans_date ASC,card_no ASC");
if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

?>
<a target="_blank" href="_download_excel.php?tno=<?php echo $tno; ?>&from_date=<?php echo $from_date; ?>&to_date=<?php echo $to_date; ?>"><button type="button" class="pull-right btn btn-danger">Download Excel</button></a>
<br />
<br />
	<table id="example" class="table table-bordered table-striped" style="font-size:12.5px;">
        <thead>
		<tr>
			<th>#</th>
			<?php
			if(!isset($_POST['tno']))
				{
					echo "<th>Vehicle_No</th>";
				}
			?>
			<th>Merchant</th>
			<th>Txn_Id</th>
			<th>Trans_id</th>
			<th>Trans_Date</th>
			<th>Trans_Type</th>
			<th>Credit</th>
			<th>Debit</th>
			<th>Balance</th>
		</tr>
		</thead>
       <tbody id=""> 
		
<?php
if(numRows($sql)==0)
{
	echo "<tr>
		<td colspan='12'>NO RESULT FOUND !!</td>
	</tr>";  
}
else
{
	$sn=1;
	while($row = fetchArray($sql))
	{
				echo 
                "<tr>
					<td>$sn</td>
				";
				if(!isset($_POST['tno']))
				{
					echo "<td>$row[card_no]</td>";
				}
				echo "
					<td>$row[merchant]</td>
					<td>$row[txn_id]</td>
					<td>$row[trans_id]</td>
					<td>".date("d-m-y",strtotime($row["trans_date"]))."</td>
					<td>$row[trans_type]</td>
					<td>$row[credit]</td>
					<td>$row[debit]</td>
					<td>$row[balance]</td>
				</tr>
				";
		$sn++;		
     }
}
?>
	</tbody>
		</table>
<script> 
$("#loadicon").hide();
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script> 