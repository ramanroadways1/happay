<?php
include "header.php";
?>

<div class="content-wrapper">
    <section class="content-header">
      <h4>
		E-diary driver's documents :
      </h4>
	  
	  <style>
		.form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
		</section>
		
   <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div class="row">
 
 <div class="form-group col-md-12">
    <br />
	<div class="form-group col-md-12">
		<div class="row">
		<div class="form-group col-md-3">
			<label>Mobile Number <font color="red">*</font></label>
			<input oninput="this.value=this.value.replace(/[^0-9]/,'')" type="text" class="form-control" required="required" id="mobile">
		</div>
		
		<div class="form-group col-md-3">
			<label>&nbsp;</label>
			<br />
			<button id="button1" type="button" onclick="LoadDocs();" class="btn btn-sm btn-primary">Search Driver</button>
		</div>
	</div>
		
    </div>
	
	<div class="form-group col-md-12 table-responsive" id="func_result">
			
    </div>
    </div>
  </div>
</div>

</body>
</html>

          </div>
          </div>
		  </div>
       </div>         
    </section>
	
<script>
function LoadDocs()
{
	var mobile = $('#mobile').val();
	
	if(mobile=="")
	{
		alert('enter mobile number first !');
	}
	else
	{
		$("#loadicon").show();
		jQuery.ajax({
			url: "_load_driver_docs.php",
			data: 'mobile=' + mobile,
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
	}
}
</script>	
<?php
include "footer_enable_copy.php";
?>