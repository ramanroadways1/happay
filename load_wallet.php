<?php
include "header.php";
exit();
?>
<div class="content-wrapper">
    <section class="content-header">
      <h4 style="font-family:Verdana">
		Load/Credit Wallet :
      </h4>
	  
	  <style>
		label{font-family:Verdana;font-size:13px;}
		.form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
	
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div class="row">
 
 <div class="form-group col-md-12">
    
	<div class="form-group col-md-12 table-responsive">
	<div id="result"></div>
	 <br />
        <table id="myTable" class="table table-bordered table-striped" style="font-family:Verdana;font-size:12px;">
        
		<tr>
			<th>#id</th>
			<th>Card<br>Number</th>
			<th>Driver Name</th>
			<th>Mobile No.</th>
			<th>Company</th>
			<th>Balance</th>
			<th>Enter Amount</th>
			<th>#</th>
		</tr>
          
            <?php
             $sql = Qry($conn,"SELECT happay_card.id,happay_card.tno,happay_card.company,happay_card.card_no,happay_card.balance,
			 happay_users.f_name,happay_users.l_name,happay_users.mobile FROM happay_card,happay_users WHERE happay_card.status='1' 
			 AND happay_card.id=happay_users.card_id ORDER BY happay_card.tno ASC");
              
			  if(!$sql){
				  echo getMySQLError($conn);exit();
			  }
			  
			  if(numRows($sql)==0)
			  {
				echo "<tr>
						<td colspan='12'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			  }
			 else
			 {
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
				echo 
                "<tr>
					<td>$sn</td>
					<td>$row[card_no]<br>($row[tno])</td>
					<td>$row[f_name] $row[l_name]</td>
					<td>$row[mobile]</td>
					<td>$row[company]</td>
					<td>$row[balance]</td>
					<td><input type='number' oninput=ChkAmount('$row[id]',this.value) min='1' class='form-control' id='amount$row[id]'></td>
					
					<input type='hidden' id='driver_name$row[id]' value='$row[f_name] $row[l_name]'>
					<td><button disabled type='button' id='Button$row[id]' class='btn btn-primary btn-sm' 
					onclick=Withdraw('$row[id]','$row[tno]','$row[balance]','$row[card_no]','$row[mobile]','$row[company]')>Load Wallet</button></td>
				</tr>
				";
				$sn++;		
              }
			} 
            ?>
        </table>
      </div>
    </div>
  </div>
</div>

</body>
</html>

<script>
function ChkAmount(id,amount)
{
		if(amount>0)
		{
			$('#Button'+id).attr('disabled',false);
		}
		else
		{
			$('#Button'+id).attr('disabled',true);
		}
}
</script>

<script>
function Withdraw(id,tno,balance,card_no,mobile,company)
{
	$('#Button'+id).attr('disabled',true);
	
	var amount = $('#amount'+id).val();
	
	if(amount>0)
	{
		if(confirm("Are you want to Load amount : "+amount+" For Vehicle No : "+tno+" ?"))
		{
			$("#loadicon").show();
			jQuery.ajax({
			url: "load_wallet_action.php",
			data: 'id=' + id + '&card_no=' + card_no + '&mobile=' + mobile + '&tno=' + tno + '&balance=' + balance + '&company=' + company + '&amount=' + amount,
			type: "POST",
			success: function(data) {
			$("#result").html(data);
			},
			error: function() {}
			});
		}
		else
		{
			$('#Button'+id).attr('disabled',true);
			$('#amount'+id).val('');
		}
	}
	else
	{
		alert('Warning : Please enter valid amount !');
		$('#Button'+id).attr('disabled',false);
	}
}
</script>
            </div>
          </div>
		  </div>
       </div>         
    </section>
<?php
include "footer.php";
?>