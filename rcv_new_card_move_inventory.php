<?php
require_once("./connect.php");

$id = escapeString($conn,strtoupper($_POST['id']));
$timestamp = date("Y-m-d H:i:s");

if($id=='')
{	
	echo "
	<font color='red'>Card not found..</font>
	<script>
		$('#loadicon').hide();
	</script>";
	exit();
}

$chk_card= Qry($conn,"SELECT * FROM dairy.happay_new_cards WHERE id='$id'");

if(!$chk_card){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($chk_card)==0)
{	
	echo "
	<font color='red'>Card not found..</font>
	<script>
		$('#loadicon').hide();
	</script>";
	exit();
}

$row = fetchArray($chk_card);

if($row['added']=="1")
{
	echo "
	<font color='red'>Card already exists in inventory..</font>
	<script>
		$('#loadicon').hide();
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$insert_new_card = Qry($conn,"INSERT INTO dairy.happay_card_inventory(card_kit_id,company,veh_no,card_status,timestamp) 
VALUES ('$row[kit_id]','$row[company]','$row[veh_no]','1','$timestamp')");

if(!$insert_new_card){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_new_card2 = Qry($conn,"INSERT INTO dairy.happay_card(tno,company,status,timestamp) 
VALUES ('$row[veh_no]','$row[company]','1','$timestamp')");

if(!$insert_new_card2){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$updated_added = Qry($conn,"UPDATE dairy.happay_new_cards SET added='1',added_timestamp='$timestamp' WHERE id='$id'");

if(!$updated_added){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$chk_in_balance = Qry($conn,"SELECT id FROM dairy.happay_live_balance WHERE veh_no='$row[veh_no]'");

if(!$chk_in_balance){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($chk_in_balance)==0)
{
	$insert_new_veh = Qry($conn,"INSERT INTO dairy.happay_live_balance(veh_no) VALUES ('$row[veh_no]')");

	if(!$insert_new_veh){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($flag)
{ 
	MySQLCommit($conn);
	closeConnection($conn);	
	echo "<script type='text/javascript'>
		alert('Success : Card moved to inventory !!');
		$('#loadicon').hide();
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script type='text/javascript'>
		alert('Error !!');
		$('#loadicon').hide();
	</script>";
	exit();
}
?>